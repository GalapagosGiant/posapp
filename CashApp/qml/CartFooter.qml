import QtQuick 2.0

Item {

    Rectangle {
        id: totalView
        width:  parent.width
        height: parent.height - chargeButton.height * 1.7
        color:  "transparent"

        Rectangle {
            id: item1
            width:  parent.width
            height: parent.height / 2
            color: "transparent"

            Rectangle {
                width: parent.width
                height: 1
                color: Style_.colorBack
            }

            Text {
                text: "Sub Total"
                color: Style_.colorText
                font.family: textFont
                font.pointSize: 10// parent.height / 4
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text:  Provider_.total + "$"
                color: Style_.colorText
                font.family: textFont
                font.pointSize: 10// parent.height / 4
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
            }
        } // item1

        Rectangle {
            id: item2
            width:  item1.width
            height: item1.height
            color: "transparent"

            anchors.top: item1.bottom

            Rectangle {
                width: parent.width
                height: 1
                color: Style_.colorBack
            }

            Text {
                text: "Tax included"
                color: Style_.colorText
                font.family: textFont
                font.pointSize: 10// parent.height / 4
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text:  Provider_.tax + "$"
                color: Style_.colorText
                font.family: textFont
                font.pointSize: 10// parent.height / 4
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
            }
        } // item2
    } // totalView

    Rectangle {
        id: chargeButton
        width:  parent.width
        height: parent.height/4
        color:  "#3674d9"
        radius: Style_.radius
        anchors.bottom: parent.bottom
        anchors.bottomMargin: height / 2

        Text {
            text: "Charge"
            color: Style_.colorText
            font.family: textFont
            font.pointSize: 10//parent.height / 4
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: parent.height / 3
        }

        Text {
            text: "<b>$" + Provider_.tax + "</b>"
            color: Style_.colorText
            font.family: textFont
            font.pointSize: 10//parent.height / 4
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: parent.height / 3
        }

        MouseArea {
            anchors.fill: parent
            onPressed:  parent.opacity = 0.9
            onReleased: {
                parent.opacity = 1;
                Provider_.onCheckClicked();
            }

        }
    }
} // Item
