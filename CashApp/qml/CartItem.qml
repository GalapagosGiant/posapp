import QtQuick 2.0

Item {

    property int i_price  : 0
    property int i_amount : 1
    property string i_name: ""

    property int tab: height * 1.5
    property string textFontGlobal;

    Rectangle {
        anchors.fill: parent
        color: "transparent"

        Text {
            text: i_amount
            color: Style_.colorText
            font.family: textFontGlobal
            font.pointSize: parent.height / 3

            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
        } // textAmount

        Text {
            text: i_name
            color: Style_.colorText
            font.family: textFontGlobal
            font.pointSize: parent.height / 3

            anchors.left: parent.left
            anchors.leftMargin: tab
            anchors.verticalCenter:   parent.verticalCenter
        } // textName

        Text {
            text: "$" + i_price
            color: Style_.colorText
            font.family: textFontGlobal
            font.pointSize: parent.height / 3

            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
        } // textPrice
    }
}
