import QtQuick 2.0

Item {

    Text {
        id: textCart
        text: "Cart"
        color: Style_.colorText
        font.family: textFont
        font.pointSize: 20// parent.height / 3

        anchors.verticalCenter:   parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Rectangle {
        width:  textCart.font.pointSize * 1.5
        height: width
        radius: width / 2
        color: "#72b4b2"

        anchors.left: textCart.right
        anchors.leftMargin: width
        anchors.verticalCenter:   parent.verticalCenter

        Text {
            text:  Provider_.amount
            color: Style_.colorMain
            font.family: textFont
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter:   parent.verticalCenter
        }
    } // amount circle

    Image {
        width:  height
        height: parent.height / 3
        source: "qrc:/img/trash-512.png"
        anchors.verticalCenter:   parent.verticalCenter

        MouseArea {
            anchors.fill: parent
            onClicked: Provider_.clearCart()
        }
    }
} // Item
