import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.12
import ToDo 1.0

Item {
    property real gridSize: 0.8
    property real tileSizeMax: parent.width/3
    property real tileSizeMin: parent.width/10
    property int tileSize:    parent.width/5

    property string textFont;

    Rectangle {
        anchors.fill: parent
        color: Style_.colorBack

        Rectangle {
            id: gridRect
            width:  parent.width
            height: parent.height * gridSize
            color: Style_.colorBack

            anchors.top: headerRect.bottom

            GridView {
                id: gridWiew
                anchors.fill: parent
                boundsBehavior: Flickable.DragOverBounds
                clip: true

                cellWidth:  tileSize * gridSlider.value / 100
                cellHeight: tileSize * gridSlider.value / 100

                highlight: Rectangle { color: "lightsteelblue";
                    radius: Style_.radius }
//                highlightFollowsCurrentItem: false

                model: model_
//                ItemListModel {
//                    list: listProducts_
//                }
                delegate:
                    GoodsGridItem {
    //                Rectangle {
                        id: item

                        width:  tileSize
                        height: width
                        textFontGlobal:  textFont

                        Layout.preferredWidth:  tileSize * gridSlider.value / 100
                        Layout.preferredHeight: tileSize * gridSlider.value / 100

                        str: model.name
                        img: model.img
    //                    img:  portrait

                        MouseArea {
                            anchors.fill: parent
                            onClicked:
                            {
                              gridWiew.currentIndex = index;

                              Provider_.addToCart(model.id);
                            }
                        }
                    }

            } // GridView
        } // gridRect

        Rectangle {
            id: headerRect
            width:  parent.width
            height: parent.height - gridRect.height - footerRect.height
            color: Style_.colorBack

            GridView {
                id: groupView
                anchors.fill: parent
                boundsBehavior: Flickable.DragOverBounds

                cellWidth:  tileSize * gridSlider.value / 100
                cellHeight: headerRect.height

                highlight: Rectangle { color: "lightsteelblue";
                    radius: Style_.radius }
                flow: GridView.FlowTopToBottom

                model: ItemListModel {
                    list: listGroups_
                }
                delegate:
                    GoodsGroupItem {
//                    Rectangle {
                        width:  groupView.cellWidth
                        height: groupView.cellHeight
                        textFontGlobal:  textFont

//                        Layout.preferredWidth:  tileSize * gridSlider.value / 100
//                        Layout.preferredHeight: tileSize / 4 * gridSlider.value / 100

                        groupName:  model.name
                        groupColor: model.color

                        MouseArea {
                            anchors.fill: parent
                            onClicked:
                            {
                                groupView.currentIndex = index;

                                Provider_.setCategory(model.id);
                            }
                        }
                    }

            } // GridView
        }

        Rectangle {
            id: footerRect
            width:  parent.width
            height: gridRect.height / 12
            color: Style_.colorBack

            anchors.top: gridRect.bottom
            anchors.left: parent.left

            Rectangle {
                width:  gridSlider.width * 1.5
                height: gridSlider.height * .8
                color: "#3a3f44"
                radius: Style_.radius
                anchors.left:   parent.left
                anchors.leftMargin: height/4
                anchors.bottom: parent.bottom

                TextInput {
                    id: hostInput
                    color: "white"
                    font.pointSize: parent*0.6
                    anchors.fill: parent
                    onTextChanged: {
                        Provider_.filterChanged(text)
                    }
                }

                Image {
                    width:  height
                    height: parent.height * 0.6
                    source: "qrc:/img/goods/search.png";
                    anchors.right: parent.right
                    anchors.rightMargin: height/2
                    anchors.verticalCenter: parent.verticalCenter
                }
            } // searchRect

            Slider {
                id: gridSlider
                from: 1
                value: 100
                to: 100

                anchors.right:  parent.right
                anchors.rightMargin: height/2
                anchors.bottom: parent.bottom
            } // Slider
        }




    }// ParentRect
} // Item
