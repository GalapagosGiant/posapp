import QtQuick 2.0
import QtQuick.Controls 2.12

Page {
    property real leftScreenSize: 0.7

    property string textFontGlobal:  "Corbel"

    Rectangle {
        id: mainRect
        color: Style_.colorBack
        anchors.fill: parent

        Rectangle {
            id: leftScreen
            width: parent.width * leftScreenSize
            height: parent.height
            color: "transparent"

            GoodsGrid {
                anchors.fill: parent

                textFont:  textFontGlobal
            }
        } // leftScreen

        Rectangle {
            id: rightScreen
            width: parent.width - leftScreen.width
            height: parent.height
            color: Style_.colorMain

            anchors.left: leftScreen.right

            Cart {
                anchors.fill: parent

                textFont:  textFontGlobal
            }
        } // rightScreen

    } // mainRect

}
