import QtQuick 2.0

Item {

    property string groupName;
    property string groupColor;

    property string textFontGlobal;

    Rectangle {
        anchors.fill: parent
        color: "transparent"

        Rectangle {
            id: rect

            width:  parent.width * 0.9
            height: parent.height - (parent.width - width)
            radius: Style_.radius
            color:  groupColor

            anchors.verticalCenter:   parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text  {
            text: groupName.toUpperCase();
            color: Style_.colorText
//                    font.family: textFontGlobal
            font.pointSize: 15
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
