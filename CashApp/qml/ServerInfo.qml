import QtQuick 2.0

Item {

    property string defaultText: "Not logged in"

    property bool   flag       : false

    Rectangle {
        id: mainRect
        anchors.fill: parent
        color: "transparent"
        radius: Style_.radius

        border.width: Style_.borderWidth
        border.color: "transparent"

        Text {
            text: qsTr("Your online session lasts")
            color: Style_.colorText
            visible: timerText.visible

            anchors.left: parent.left
            anchors.leftMargin: button.width
            anchors.verticalCenter: button.verticalCenter
        }

        Text {
            id: timerText
//            text: qsTr("00 hours 01 min 20 secs")
            text: defaultText
            color: Style_.colorText
            font.pointSize: button.width * 0.6
            visible: false

            anchors.top: button.bottom
            anchors.topMargin: button.width
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Rectangle {
            width: button.width + 10
            height: width
            radius: width / 2
            color:  Style_.colorBack
            anchors.verticalCenter:   button.verticalCenter
            anchors.horizontalCenter: button.horizontalCenter
        } // button border

        Rectangle {
            id: button
            width:  30
            height: width
            radius: width / 2
            color:  Style_.colorBack

            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin:   width / 2
            anchors.rightMargin: width / 2

            Image {
                anchors.fill: parent
                source: "qrc:/img/gear30x30.png"
            }

            MouseArea {
                anchors.fill: parent
                onClicked:    buttonClicked()
            }

        } // button

        Rectangle {
            width: status.width + 10
            height: width
            radius: width / 2
            color:  Style_.colorBack
            anchors.verticalCenter:   status.verticalCenter
            anchors.horizontalCenter: status.horizontalCenter
        } // status border

        Rectangle {
            id: status
            width:  10
            height: width
            radius: width / 2
            color: "red"

            anchors.left:   button.left
            anchors.bottom: button.bottom
        } // status

        Rectangle {
            width:  height * 3
            height: 40
            color:  "transparent"
            radius: Style_.radius
            visible: timerText.visible

            border.width:  Style_.borderWidth
            border.color:  Style_.borderColor

            anchors.top: timerText.top
            anchors.right: parent.right
            anchors.topMargin:   button.width * 2
            anchors.rightMargin: button.width / 2

            Text {
                text: qsTr("Log off")
                color: Style_.colorText
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onPressed:  parent.opacity = 0.8
                onReleased: parent.opacity = 1
            }
        }
    } // mainRect

    Timer {
        id: timer
        interval: 100
        running: false
        repeat: true
        onTriggered: timeChanged()
    }

    function timeChanged() {
        var date = new Date;

        var hours   = date.getHours()   - Provider_.hour;
        var minutes = date.getMinutes() - Provider_.min;
        var seconds = date.getUTCSeconds();

        timerText.text = hours + " h " + minutes + " m " + seconds + " s";
    }

    function startTime() {
        timer.start()
    }

    function endTime() {
        timer.stop()
        timerText = defaultText
    }

    function setStatus(val) {
        status.color = val ? "green" : "red";
    }

    function buttonClicked() {
        console.log("ServerInfo: Button clicked")

        flag = !flag

        showInfo(flag)
    }

    function showInfo() {
        mainRect.color = flag ? Style_.colorMain : "transparent";
        mainRect.border.color = flag ? Style_.borderColor : "transparent";

        timerText.visible = flag ? true : false;
    }

    function hideInfo() {

    }
}
