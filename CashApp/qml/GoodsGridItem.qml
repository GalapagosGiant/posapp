import QtQuick 2.0
import QtGraphicalEffects 1.12

Item {

    property string str: ""
    property string img:  ""

    property string defaultImg:  "qrc:/img/goods/tshirt1.png"
    property string textFontGlobal;

    Rectangle {
        anchors.fill: parent
        color: "transparent"

//        RectangularGlow {
//            id: effect
//            anchors.fill: rect
//            glowRadius: 10
//            spread: 0.2
//            opacity: 0.05
//            color: "black"
//            cornerRadius: rect.radius + glowRadius
//        }

        Rectangle {
            id: rect
            color: "#3a3f44"

            width:  parent.width * 0.9
            height: width
            radius: Style_.radius

            anchors.verticalCenter:   parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            Column {
                anchors.fill: parent

                Image {
                    id: image
                    width:  rect.width * 0.8
                    height: width
                    source: img ? img : defaultImg;
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Text  {
                    text: str;
                    color: Style_.colorText
//                    font.family: textFontGlobal
                    font.pointSize: 10//(rect.height - image.height) / 3
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }
        }
    }
}
