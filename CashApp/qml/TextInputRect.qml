import QtQuick 2.0

Item {

    property string defColor: "#3a3f44"

    property string defaultText;

    Rectangle {
        id: mainRect
        anchors.fill: parent
        color: defColor

        border.width: Style_.borderWidth
        border.color: Style_.borderColor
        radius: Style_.radius

        TextInput {
            id: textInput

            text:  defaultText
            color: Style_.colorText
            font.family: "Open Sans"
            font.pointSize: parent.height/4
            maximumLength: 34

            anchors.left: parent.left
            anchors.leftMargin: parent.height / 2
            anchors.verticalCenter: parent.verticalCenter
        } // hostInput

        Keys.onPressed: {
            if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter || event.key == Qt.Key_Tab) {
                deactivateLogin()
                activatePass()

                console.log("host Tab")
            }
        }
    } // MainRect

    function activate() {
        console.log("TextInputRect: Activate")

        mainRect.border.color = Style_.borderColorActive

        textInput.color = "white"
        textInput.forceActiveFocus()

        if (textInput.text == defaultText)
            textInput.text = ""
    }

    function deactivate() {
        console.log("TextInputRect: Deactivate")

        mainRect.border.color = Style_.borderColor

        textInput.focus = false

        if (textInput.text == "")
            textInput.text = defaultText
    }

    function selectAll() {
        textInput.selectAll()
    }

    function getText() {
        return textInput.text;
    }
}
