import QtQuick 2.0
import ToDo 1.0

Item {
    property real listSize: 0.5
    property real footerSize: 0.23

    property string textFont;

    Rectangle {
        anchors.fill: parent
        color: Style_.colorMain

        Rectangle {
            width:  parent.width * 0.9
            height: parent.height
            color: "transparent"
            anchors.horizontalCenter: parent.horizontalCenter

            CartHeader {
                id: header
                width:  parent.width
                height: footer.height / 2
            } // header

            ListView {
                id: listView
                width:  parent.width
                height: parent.height - header.height - footer.height

                anchors.top: header.bottom

                spacing: 5
                model: modelCheck_
                clip: true

                delegate: CartItem {
                    width:  listView.width
                    height: listView.height / 12
                    textFontGlobal:  textFont

                    i_name:   model.name
                    i_amount: model.amount
                    i_price:  model.price

                    MouseArea {
                        anchors.fill: parent
                        onClicked:
                        {
                            Provider_.removeFromCart(model.id);
                        }
                    }
                }
            } // listView

            CartFooter {
                id: footer
                width:  parent.width
                height: parent.height * footerSize
                anchors.bottom: parent.bottom
            } // footer
        }

    } // ParentRect
} // Item
