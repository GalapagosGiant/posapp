import QtQuick 2.0

Item {

    Rectangle {
        anchors.fill: parent
        opacity: 0.5
        color: "#38ad6b"
    }

    Text {
        id: textMessage
        anchors.fill: parent
        text: qsTr("Check accepted")
        color: "white"
        font.pointSize: height / 4
        verticalAlignment:   TextInput.AlignVCenter
        horizontalAlignment: TextInput.AlignHCenter
    }

    function setMessage(msg) {
        textMessage.text = msg;
    }
}
