import QtQuick 2.0

Item {

    signal btnClicked(string text)

    property string buttonColor: "#36b7eb"
    property string buttonBorderColor: borderActiveColor

    Rectangle {
        anchors.fill: parent

        border.width: Style_.borderWidth
        border.color: buttonBorderColor
        radius: Style_.radius
        color: buttonColor

        Text {
            id:   btnText
            text: "Connect"
            color: "white"
            font.family: "Open Sans"
            font.pointSize: parent.height/4
            font.bold: true
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            z: 5

            onClicked: {
                console.log("ButtonRect: Clicked");
                btnClicked(btnText.text);
            }
        }
    } // button

}
