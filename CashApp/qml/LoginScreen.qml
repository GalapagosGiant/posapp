import QtQuick 2.0
import QtQuick.Controls 2.12

import "qrc:/qml/scripts/functions.js" as Functions

Page {
//    anchors.fill: parent

    property int blockWidth:  parent.width/12

    property string pasiveColorTmp:  "#eaeaea"
    property string textDefaultColor:  "black"

    property string messageDefaultValue:  "Please fill out the form below to get started"

    Connections {
        target: Client_
        onQmlGotError: showError(error)
    }

    Rectangle {
        anchors.fill: parent
        color: Style_.colorBack

        Rectangle {
            width:  parent.width  / 4
            height: parent.height / 4

            color: "transparent"

            anchors.verticalCenter:   parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            Rectangle {
                id: messageArea
                color: "transparent"

                width:  parent.width
                height: (parent.height - login.height - pass.height) /1.5

                Text {
                    id: signInText

                    text: "Sign in"
                    color: pasiveColorTmp
                    font.family: "Open Sans"
                    font.pointSize: parent.height/3
                    font.bold: true

                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Text {
                    id: messageText

                    text:  messageDefaultValue
                    color: Style_.colorText
                    font.family: "Open Sans"
                    font.pointSize: parent.height/7
                    font.bold: true

                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }

            TextInputRect {
                id: login
                width:  parent.width
                height: parent.height / 5

                defaultText: "Login"

                anchors.top:  messageArea.bottom
                anchors.topMargin: height / 2

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        login.activate();
                        pass.deactivate()
                    }
                    onDoubleClicked: {
                        login.selectAll()
                    }
                }
            }

            TextInputRect {
                id: pass
                width:  login.width
                height: login.height

                defaultText: "Password"

                anchors.top:  login.bottom
                anchors.topMargin: height / 2

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        pass.activate()
                        login.deactivate()
                    }
                    onDoubleClicked: {
                        pass.selectAll()
                    }
                }
            }

            ButtonRect {
                id: button
                width:  parent.width
                height: login.height

                anchors.top:   pass.bottom
                anchors.topMargin: height / 2

                onBtnClicked: connect()
            }

            MouseArea {
                anchors.fill: parent
                z: -10

                onClicked: {
                    login.deactivate();
                    pass.deactivate();

                    console.log("background")
                }
            }
        }
    } // background

    function connect() {
//        if (!validatePort(portInput.text)) {
//            showError("Port is not valid")
//            portTextHint.text = portDefaultValue
//            portInput.text = ""
//            deactivatePort()

//            return
//        }

        login.deactivate()
        pass.deactivate()

        var result = Client_.login(login.getText(), pass.getText())

        if (result === 1)
            swipe.popExit()
    }

    function showError(error) {
        messageText.text = error
        messageText.color = "#FF5632"
    }

    function cleanError() {
        messageText.text  = messageDefaultValue
        messageText.color = Style_.colorText
    }

    function validatePort(port) {
        if (isNaN(port))
            return false

        return port >= 1 && port <= 65535
    }
} // Item
