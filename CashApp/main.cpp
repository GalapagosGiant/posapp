#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QStandardPaths>
#include <QDir>

#include "clientmanager.h"
#include "models/itemlistmodel.h"
#include "lists/itemlist.h"
#include "options.h"
#include "style.h"

#include "cpp/cashprovider.h"

void setOptions(Options *options);

int main(int argc, char *argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QGuiApplication app(argc, argv);

  qmlRegisterType<ItemListModel>("ToDo", 1, 0, "ItemListModel");
  qmlRegisterUncreatableType<ItemList>("ToDo", 1, 0, "ItemList",
      QStringLiteral("ItemList should not be created in QML"));

  Options options;

  setOptions(&options);

  Logger logger(QCoreApplication::applicationPid(), "client");

  logger.startMessage();

  // Data provider

  CashProvider provider(&logger, &options);

  // Create client

  ClientManager client("cash", &logger, &options, &provider);

  provider.setClient(&client.client_);

  // Style

  Style style("dark");

  // Start gui application

  QQmlApplicationEngine engine;

  // Set context properties

  QQmlContext *context = engine.rootContext();

  context -> setContextProperty("Provider_", &provider);
  context -> setContextProperty("Client_", &client.client_);
  context -> setContextProperty("Style_", &style);
  context -> setContextProperty("listGroups_", &provider.listGroups_);
//  context -> setContextProperty("listProducts_", &provider.listProducts_);
  context -> setContextProperty("model_", &provider.proxyProducts_);
  context -> setContextProperty("modelCheck_", &provider.modelCheck_);

  // Load qml files

  const QUrl url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                   &app, [url](QObject *obj, const QUrl &objUrl) {
    if (!obj && url == objUrl)
      QCoreApplication::exit(-1);
  }, Qt::QueuedConnection);
  engine.load(url);


  client.client_.run();

  return app.exec();
}


void setOptions(Options *options)
{
  QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);

  options -> HomePath = path + "/PosApp";
  options -> DocsPath = options -> HomePath + "/docs";
  options -> ImgPath  = options -> HomePath + "/img";

  bool res = false;

  if (!QDir(options -> HomePath).exists())
  {
    res = QDir(path).mkdir("PosApp");

    qDebug() << "Created home directory " << options -> HomePath << res;
  }

  if (!QDir(options -> ImgPath).exists())
  {
    res = QDir(options -> HomePath).mkdir("img");

    qDebug() << "Created image directory " << options -> ImgPath << res;
  }

  if (!QDir(options -> DocsPath).exists())
  {
    res = QDir(options -> HomePath).mkdir("docs");

    qDebug() << "Created docs directory " << options -> DocsPath << res;
  }
}
