#ifndef OBJECTLISTMODEL_H
#define OBJECTLISTMODEL_H

#include <QAbstractListModel>

class ObjectList;

class ObjectListModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(ObjectList *list READ list WRITE setList)

public:
  explicit ObjectListModel(QObject *parent = nullptr);

  enum {
    NameRole = Qt::UserRole,
    ImageRole,
    PriceRole,
    AmountRole
  };

  // Basic functionality:
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  // Editable:
  bool setData(const QModelIndex &index, const QVariant &value,
               int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

  virtual QHash<int, QByteArray> roleNames() const override;

  ObjectList *list() const;
  void setList(ObjectList *list);

private:
  ObjectList *list_;
};

#endif // OBJECTLISTMODEL_H
