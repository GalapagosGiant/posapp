#ifndef CASHPROVIDER_H
#define CASHPROVIDER_H

#include <QObject>

#include <provider.h>

class Client;

class CashProvider : public Provider
{
  Q_OBJECT
  Q_PROPERTY(double tax READ tax WRITE setTax NOTIFY taxChanged)
  Q_PROPERTY(double total READ total WRITE setTotal NOTIFY totalChanged)
  Q_PROPERTY(int amount READ amount WRITE setAmount NOTIFY amountChanged)

public:
  CashProvider(Logger *logger, Options *options, QObject *parent = nullptr);

  QString getName() override
  {
    return "CashProvider";
  }

  void setTax(const double &value);
  void setTotal(const double &value);
  void setAmount(const int &value);

  double tax() const;
  double total() const;
  int amount() const;

  ItemListModel modelCheck_;

signals:
  void taxChanged();
  void totalChanged();
  void amountChanged();

public slots:
  void addToCart(int id);
  void removeFromCart(int id);
  void onCheckClicked();
  void clearCart();

private:
  void updateCart();

  double tax_;
  double total_;
  int amount_;

  ItemList listCheck_;
};

#endif // CASHPROVIDER_H
