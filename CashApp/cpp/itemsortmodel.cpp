#include "itemsortmodel.h"
#include "models/itemlistmodel.h"

#include <QDebug>

ItemSortModel::ItemSortModel(QObject *parent) :
  QSortFilterProxyModel(parent)
{

}

void ItemSortModel::setCategory(int parent)
{
  category = parent;

  invalidateFilter();
}

bool ItemSortModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
  QModelIndex index0 = sourceModel() -> index(sourceRow, 0, sourceParent);

//  qDebug() << "TEST: category" << category;
//  qDebug() << "TEST: cat" << sourceModel() -> data(index0, ItemListModel::ParentRole)
//                                       .toInt();

  return (sourceModel() -> data(index0, ItemListModel::NameRole)
            .toString().contains(filterRegExp()) &&
          itemInCat(sourceModel() -> data(index0, ItemListModel::ParentRole)
                    .toInt()));
}

bool ItemSortModel::itemInCat(int item) const
{
  return category == item;
}
