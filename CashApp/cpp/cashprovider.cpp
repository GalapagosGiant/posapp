#include "cashprovider.h"
#include "client.h"

CashProvider::CashProvider(Logger *logger, Options *options, QObject *parent)
  : Provider(logger, options, parent)
{
  tax_    = 0;
  total_  = 0;
  amount_ = 0;

  modelCheck_.setList(&listCheck_);
}

void CashProvider::addToCart(int id)
{
  log("Add to cart index '" + QString::number(id) + "'");

  QVector<Item> vec = listProducts_.items();

  for (int i = 0; i < vec.size(); i++)
  {
    if (vec.at(i).id() == id)
    {
      Item item = vec.at(i);

      item.setAmount(1);

      listCheck_.appendItem(item);
    }
  }

  updateCart();
}

void CashProvider::removeFromCart(int id)
{
  log("Remove from cart index '" + QString::number(id) + "'");

  listCheck_.removeOrReduce(id);

  updateCart();
}

void CashProvider::onCheckClicked()
{
  if (client_)
  {
    client_ -> sendCheck("check", listCheck_.items(), QString::number(total(), 'f', 2), QString::number(tax(), 'f', 2));

    clearCart();
  }
}

void CashProvider::clearCart()
{
  listCheck_.clear();

  setAmount(0);
  setTotal(0);
  setTax(0);
}

void CashProvider::updateCart()
{
  // Set cart values

  QVector<Item> list = listCheck_.items();

  int amount   = 0;
  double total = 0;

  for (int i = 0; i < list.size(); i++)
  {
    amount += list.at(i).amount();
    total  += list.at(i).price() * list.at(i).amount();
  }

  setAmount(amount);
  setTotal(total);
  setTax(total * (1 + 0.23));
}

void CashProvider::setTax(const double &value)
{
  if (value != tax_) {
    tax_ = value;
    emit taxChanged();
  }
}

void CashProvider::setTotal(const double &value)
{
  if (value != total_) {
    total_ = value;
    emit totalChanged();
  }
}

void CashProvider::setAmount(const int &value)
{
  if (value != amount_) {
    amount_ = value;
    emit amountChanged();
  }
}

double CashProvider::tax() const
{
  return tax_;
}

double CashProvider::total() const
{
  return total_;
}

int CashProvider::amount() const
{
  return amount_;
}
