import QtQuick 2.12
import QtQuick.Controls 2.12

import "qml" as MyComponents

ApplicationWindow {
    visible: true
    visibility: "Maximized"
    title: qsTr("Cash Register")

    // Define properties

    property bool isLogged: false

    // StackView with 2 screens
    color: Style_.colorBack

    Rectangle {
        anchors.fill: parent
        color: Style_.colorBack

        SwipeView {
            id: swipe
            anchors.fill: parent

            interactive: false
            currentIndex: 0

            MyComponents.LoginScreen {
                id: connectScreen
            }

            MyComponents.MainScreen {
                id: mainScreen
            }
        } // swipe

        MyComponents.ServerInfo {
            id: info
            width:  parent.width  / 4
            height: parent.height / 3.7

            anchors.top:   parent.top
            anchors.right: parent.right
            anchors.topMargin:   5
            anchors.rightMargin: 5
        } // info
    } // mainRect

    Connections {
        target: Client_

        onStatusChanged: {
            if (Client_.status === "connected") {
                console.log("Got connected status.")
//                stack.push(paneMain)

                info.setStatus(1);

                connectScreen.cleanError();
            }
            if (Client_.status === "disconnected")
            {
//                stack.pop()
                swipe.setCurrentIndex(0);

                info.setStatus(0);
                info.stopTime();
            }
        }

        onClientLogged: {
            console.log("Client logged in")
            swipe.setCurrentIndex(1);
            info.startTime();
        }
    }
}
