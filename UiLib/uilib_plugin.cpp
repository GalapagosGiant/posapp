#include "uilib_plugin.h"

#include "textinputrect.h"

#include <qqml.h>

void UiLibPlugin::registerTypes(const char *uri)
{
  // @uri com.test.qmlcomponents
  qmlRegisterType<TextInputRect>(uri, 1, 0, "TextInputRect");
}

