#ifndef TEXTINPUTRECT_H
#define TEXTINPUTRECT_H

#include <QQuickItem>

class TextInputRect : public QQuickItem
{
  Q_OBJECT
  Q_DISABLE_COPY(TextInputRect)

public:
  explicit TextInputRect(QQuickItem *parent = nullptr);
  ~TextInputRect() override;
};

#endif // TEXTINPUTRECT_H
