#ifndef UILIB_PLUGIN_H
#define UILIB_PLUGIN_H

#include <QQmlExtensionPlugin>

class UiLibPlugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
  void registerTypes(const char *uri) override;
};

#endif // UILIB_PLUGIN_H
