#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QStandardPaths>
#include <QDir>

#include "cpp/database.h"
#include "cpp/printer.h"
#include "cpp/server.h"
#include "options.h"

void setOptions(Options *options);

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    // Options

    Options options;

    setOptions(&options);

    // Logger

    Logger logger(QCoreApplication::applicationPid(), "server");

    logger.startMessage();

    // Printer

    Printer printer(&logger, &options);

    // Database

    Database db(&logger, &options, &printer);

    // Server

    Server server(&logger, &options, &db);

    server.start();

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}

void setOptions(Options *options)
{
  QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);

  options -> HomePath = path + "/PosApp";
  options -> DocsPath = options -> HomePath + "/docs";
  options -> ImgPath  = options -> HomePath + "/img";

  bool res = false;

  if (!QDir(options -> HomePath).exists())
  {
    res = QDir(path).mkdir("PosApp");

    qDebug() << "Created home directory " << options -> HomePath << res;
  }

  if (!QDir(options -> ImgPath).exists())
  {
    res = QDir(options -> HomePath).mkdir("img");

    qDebug() << "Created image directory " << options -> ImgPath << res;
  }

  if (!QDir(options -> DocsPath).exists())
  {
    res = QDir(options -> HomePath).mkdir("docs");

    qDebug() << "Created docs directory " << options -> DocsPath << res;
  }
}
