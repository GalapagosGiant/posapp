#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QSqlQuery>

#include "object.h"
#include "items/item.h"
#include "items/documentitem.h"
#include "items/customeritem.h"
#include "items/employeeitem.h"
#include "items/sessionitem.h"

class Printer;

class Database : public Object
{
public:
  Database(Logger *logger, Options *options, Printer *printer);
  ~Database();

  QString getName() override
  {
      return "Database";
  }

  void connect();

  void getDocuments(QVector<DocumentItem> &vec, const QString &id = "");
  void getSuppliers(QVector<CustomerItem> &vec, const QString &id = "");
  void getEmployees(QVector<EmployeeItem> &vec, const QString &id = "");
  void getSessions(QVector<SessionItem> &vec, const QString &id = "");
  void getCategories(QVector<Item> &vec, const QString &id = "");
  void getProducts(QVector<Item> &vec, const QString &id = "");
  void getImages(const QStringList &ids, QVector<QString> &vec);

  bool index(int &id, const QString &table, const QString &field, const QString &value);
  bool add(int &id, const QString &type, const BaseItem *item);
  bool del(const int &id, const QString &type);
  bool set(const QString &id, const QString &type, const QString &field, const QString &value);
  bool check(int &id, const QString &type, const QVector<int> &ids, const QVector<int> &amounts, const QString &total, const QString &tax);
  bool checkPassword(const QString &login, const QByteArray &password, QString &error, QString &time);
  bool startSession(const QString &login, QString &time);
  bool endSession();

private:

  Printer *getPrinter()
  {
    return pr_;
  }

  bool getTable(QString &table, const QString &type);
  bool send(const QString &query);

  QSqlQuery *query_;
  QSqlDatabase db_;

  QString sessionId_;

  Printer *pr_;
};

#endif // DATABASE_H
