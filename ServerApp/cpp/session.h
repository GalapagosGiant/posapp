#ifndef SESSION_H
#define SESSION_H

#include <QWebSocket>
#include <QThread>
#include <QTimer>

#include "object.h"

class Server;
class Database;

class Session : public QObject, public Object
{
  Q_OBJECT

public:
  explicit Session(Server *server, Logger *logger, Options *options, Database *db, QWebSocket *socket_, QObject *parent = nullptr);
  ~Session();

  QString getName() override
  {
    return "Session";
  }

  void run();

  void sendData(const QString &type, const QString &id = "");
  void sendDel(const QString &type, const QString &id);

signals:
  void terminated();

public slots:
  void terminating();

  qint64 sendToClient(QString path);
  qint64 sendMessageToClient(QString message);

private Q_SLOTS:
  void processTextMessage(QString message);
  void processBinaryMessage(const QByteArray &array);
  void socketDisconnected();
  void displayError();

private:
  qint64 send(QString command, QString type, QString value = "");

  void parse(QString &message);
  void parseSet(const QString &type, const QString &value);
  void parseDel(const QString &type, const QString &value);
  void parseLogin(const QString &login, const QByteArray &passw);
  void parseCheck(const QString &type, const QVector<int> &ids, const QVector<int> &amounts, const QString &total, const QString &tax);
  void parseAdd(const QString &type, QDataStream &in);

  void sendDocuments(const QString &id = "");
  void sendSuppliers(const QString &id = "");
  void sendEmployees(const QString &id = "");
  void sendSessions(const QString &id = "");
  void sendGroups(const QString &id = "");
  void sendProducts(const QString &id = "");
  void sendImages(const QString &value);
  void parseHello(const QString &type);

  void cleanThreadPaths();

  typedef enum
  {
    WaitingLogin,
    Working

  } State;

  void setState(State state);
  QString getStateName(State state);

  Server *getServer()
  {
    return server_;
  }

  Database *getDatabase()
  {
    return db_;
  }

  QString HomePath;
  QString ThreadPath;

  Database *db_;

  QWebSocket *socket_;

  QString type_;

  Server *server_;

  State state_;
};

#endif // SESSION_H
