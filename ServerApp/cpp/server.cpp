#include "server.h"

#include <QWebSocketServer>
#include <QWebSocket>

Server::Server(Logger *logger, Options *options, Database *db) :
  Object(logger, options), port_(4949), host_("192.168.0.206"), db_(db),
  socket_(new QWebSocketServer(QStringLiteral("Server"), QWebSocketServer::NonSecureMode, this))
{
  log("Server started successfuly with thread id: 0x" +
      QString::number((uint64_t)QThread::currentThread(), 16 ));
}

Server::~Server()
{
  socket_ -> close();
}

void Server::start()
{
  if (socket_ -> listen(QHostAddress::Any, port_))
  {
    log("Listening on port [" + QString::number(port_) + "]");

    connect(socket_, &QWebSocketServer::newConnection,
            this, &Server::onNewConnection);
    connect(socket_, &QWebSocketServer::closed, this, &Server::stopClients);
  }
  else
  {
    log("Not listening.");
  }
}

void Server::stopClients()
{
  // Terminate all threads

  for (int i(0); i < sessions_.size(); ++i)
  {
//    sessions_.at(i) -> terminate();
  }
}

void Server::onNewConnection()
{
  log("Incoming connection on descriptor: " /*+ QString::number(socketDescriptor)*/);

  QWebSocket *socket = socket_ -> nextPendingConnection();

  Session *session = new Session(this, logger_, options_, getDatabase(), socket);

  sessions_.push_back(session);

//  socket -> moveToThread(session);

  connect(session, SIGNAL(terminated()), session, SLOT(deleteConnection()));

//  session -> start();
  session -> run();
}

void Server::broadCast(const QString &type, const QString &id, int del)
{
  log("Propagate type '" + type + "' to all clients");

  for (int i(0); i < sessions_.size(); ++i)
  {
    if (del == 0)
    {
      sessions_.at(i) -> sendData(type, id);
    }
    else
    {
      sessions_.at(i) -> sendDel(type, id);
    }
  }
}

void Server::deleteConnection()
{
  log("Remove connection");

  Session *session = qobject_cast<Session *>(sender());

  if (session)
  {
    sessions_.removeAll(session);
  }
}
