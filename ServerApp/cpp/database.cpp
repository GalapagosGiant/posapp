#include "database.h"
#include "printer.h"

#include <QDateTime>
#include <QSqlError>
#include <QDebug>
#include <QMap>

Database::Database(Logger *logger, Options *options, Printer *printer) :
    Object(logger, options),
    query_(nullptr), pr_(printer)
{
  connect();
}

Database::~Database()
{
  delete query_;
}

void Database::connect()
{
  db_ = QSqlDatabase::addDatabase("QMYSQL");

  db_.setHostName("localhost");
  db_.setDatabaseName("mydb");
  db_.setUserName("root");
  db_.setPassword("root");

  bool res = db_.open();

  if (res == false)
  {
    logErr("Unable to connect to the database");
    logErr(db_.lastError().text());
  }

  log("Connected to " + db_.databaseName());

  query_ = new QSqlQuery(db_);
}

bool Database::send(const QString &query)
{
  log("Send '" + query + "' to database");

  if (!query_ -> exec(query))
  {
    logErr(db_.lastError().text());

    return false;
  }

  return true;
}

void Database::getDocuments(QVector<DocumentItem> &vec, const QString &id)
{
  log("Get documents");

  QString query = "SELECT Check.check_id,Check.check_timestamp,Check.check_price,"
                         "Employee_login,Check.check_document,Check.check_type "
                  "FROM mydb.Check "
                  "INNER JOIN mydb.Employee ON Check.check_employee=Employee.employee_id";

  if (id.isEmpty())
  {
    query += ";";
  }
  else
  {
    query += " WHERE check_id='" + id + "';";
  }

  send(query);

  while (query_ -> next())
  {
    int id = query_ -> value(0).toInt();

    QDateTime date   = query_ -> value(1).toDateTime();
    double price     = query_ -> value(2).toDouble();
    QString login    = query_ -> value(3).toString();
    QString document = query_ -> value(4).toString();
    QString name     = query_ -> value(5).toString();

    DocumentItem item(id, -1, name, date, document, login, price);

    vec.push_back(item);
  }

  log("Got " + QString::number(vec.size()) + " items");
}

void Database::getSuppliers(QVector<CustomerItem> &vec, const QString &id)
{
  log("Get suppliers");

  if (id.isEmpty())
  {
    send("SELECT * FROM mydb.Supplier;");
  }
  else
  {
    send("SELECT * FROM mydb.Supplier WHERE supplier_id='" + id + "';");
  }

  while (query_ -> next())
  {
    int id = query_ -> value(0).toInt();

    QString name  = query_ -> value(1).toString();
    QString mail  = query_ -> value(2).toString();
    QString tel   = query_ -> value(3).toString();

    CustomerItem item(id, -1, name, "", mail, tel);

    vec.push_back(item);
  }

  log("Got " + QString::number(vec.size()) + " items");
}

void Database::getEmployees(QVector<EmployeeItem> &vec, const QString &id)
{
  log("Get emloyees");

  QString query = "SELECT Employee.employee_id,Employee.employee_name,Employee.employee_surname,"
                         "Employee.employee_mail,Employee.employee_login,mydb.Position.position_name "
                  "FROM mydb.Employee "
                  "INNER JOIN mydb.Position ON Employee.employee_id=Position.position_id";

  if (id.isEmpty())
  {
    query += ";";
  }
  else
  {
    query += " WHERE employee_id='" + id + "';";
  }

  send(query);

  while (query_ -> next())
  {
    int id = query_ -> value(0).toInt();

    QString name     = query_ -> value(1).toString();
    QString surname  = query_ -> value(2).toString();
    QString mail     = query_ -> value(3).toString();
    QString login    = query_ -> value(4).toString();
    QString position = query_ -> value(5).toString();

    EmployeeItem item(id, -1, name, surname, position, login, "", mail);

    vec.push_back(item);
  }

  log("Got " + QString::number(vec.size()) + " items");
}

void Database::getSessions(QVector<SessionItem> &vec, const QString &id)
{
  log("Get emloyees");

  QString query = "SELECT * FROM mydb.Session";

  if (id.isEmpty())
  {
    query += ";";
  }
  else
  {
    query += " WHERE session_id='" + id + "';";
  }

  send(query);

  while (query_ -> next())
  {
    int id     = query_ -> value(0).toInt();
    int parent = query_ -> value(3).toInt();

    QDateTime stamp1 = query_ -> value(1).toDateTime();
    QDateTime stamp2 = query_ -> value(2).toDateTime();

    SessionItem item(id, parent, stamp1.date(), stamp1.time(), stamp2.time());

    vec.push_back(item);
  }

  log("Got " + QString::number(vec.size()) + " items");
}

void Database::getCategories(QVector<Item> &vec, const QString &id)
{
  log("Get categories");

  if (id.isEmpty())
  {
    send("SELECT * FROM mydb.Category;");
  }
  else
  {
    send("SELECT * FROM mydb.Category WHERE category_id='" + id + "';");
  }

  while (query_ -> next())
  {
    int id = query_ -> value(0).toInt();

    QString name  = query_ -> value(1).toString();
    QString color = query_ -> value(2).toString();

    Item item(name, color, "", -1, -1, id, 0);

    vec.push_back(item);
  }

  log("Got " + QString::number(vec.size()) + " items");
}

void Database::getProducts(QVector<Item> &vec, const QString &id)
{
  log("Get items");

  QString query = "SELECT item_id,item_name,item_price,item_quantity,item_img,Category_category_id "
       "FROM mydb.Item WHERE item_deleted<>'1'";

  if (id.isEmpty())
  {
    query += ";";
  }
  else
  {
    query += " && item_id='" + id + "';";
  }

  send(query);

  while (query_ -> next())
  {
    int id     = query_ -> value(0).toInt();
    int cat    = query_ -> value(5).toInt();
    int price  = query_ -> value(2).toInt();
    int amount = query_ -> value(3).toInt();

    QString name  = query_ -> value(1).toString();
    QString img   = query_ -> value(4).toString();

    Item item(name, "red", img, price, amount, id, cat);

    vec.push_back(item);
  }

  log("Got " + QString::number(vec.size()) + " items");
}

void Database::getImages(const QStringList &ids, QVector<QString> &vec)
{
  log("Get " + QString::number(ids.size()) + " product images");

  if (ids.size() == 0)
  {
    return;
  }

  // Prepare query

  QString query = "SELECT item_img FROM mydb.Item WHERE item_id='" + ids.at(0) + "'";

  for (int i = 1; i < ids.size(); i++)
  {
    query += " || item_id='" + ids.at(i) + "'";
  }

  query += ";";

  // Exec query

  send(query);

  while (query_ -> next())
  {
    QString name  = query_ -> value(0).toString();

    vec.push_back(name);
  }

  log("Got " + QString::number(ids.size()) + " items");
}

bool Database::index(int &id, const QString &table, const QString &field, const QString &value)
{
  qDebug() << "TEST 55 " << id << table << field << value;

  QString query = "SELECT " + table.toLower() + "_id FROM mydb." + table + " WHERE " + field + "='" + value + "';";

  qDebug() << "TEST 6";

  if (send(query) == false)
  {
    return false;
  }

  qDebug() << "TEST 8";

  while (query_ -> next())
  {
    id = query_ -> value(0).toInt();

    log("Got item '" + field + "=" + value + "' id " + id + " from table '" + table + "'");

    return true;
  }

  log("Item '" + field + "=" + value + "' does not exist in table '" + table + "'");

  return true;
}

bool Database::add(int &id, const QString &type, const BaseItem *item)
{
  log("Add new " + type + " item");

  QString table;
  QString uqValue;
  QString uqColumn;

  getTable(table, type);

  QMap<QString, QString> map;

  if (QString::compare(type, "suppliers") == 0)
  {
    CustomerItem *itm = (CustomerItem *) item;

    map.insert("supplier_name", itm -> name());
    map.insert("supplier_mail", itm -> mail());
    map.insert("supplier_tel",  itm -> tel());

    uqColumn = "supplier_name";
    uqValue  = itm -> name();
  }
  else if (QString::compare(type, "employees") == 0)
  {
    EmployeeItem *itm = (EmployeeItem *) item;

    map.insert("employee_name", itm -> name());
    map.insert("employee_surname", itm -> surname());
    map.insert("employee_mail",  itm -> mail());
    map.insert("employee_login",  itm -> login());
    map.insert("employee_password",  itm -> passw());

    uqColumn = "employee_login";
    uqValue  = itm -> login();
  }
  else if (QString::compare(type, "categories") == 0)
  {
    Item *itm = (Item *) item;

    qDebug() << "TEST 1" << itm -> name();

    map.insert("category_name",  itm -> name());

    qDebug() << "TEST 2" << itm -> color();
    map.insert("category_color", itm -> color());

    qDebug() << "TEST 3";

    uqColumn = "category_name";
    uqValue  = itm -> name();
  }
  else if (QString::compare(type, "products") == 0)
  {
    Item *itm = (Item *) item;

    map.insert("item_name",  itm -> name());
    map.insert("item_price", QString::number(itm -> price()));
    map.insert("item_quantity", QString::number(itm -> amount()));
    map.insert("item_img", itm -> img());
    map.insert("Category_category_id", QString::number(itm -> parentId()));
    map.insert("Supplier_supplier_id", "0");

    uqColumn = "item_name";
    uqValue  = itm -> name();
  }
  else
  {
    logErr("Unknown type '" + type + "'");

    return false;
  }

  qDebug() << "TEST 4";

  // Check if item exists

  qDebug() << "TEST 44 index(" << id << table << uqColumn << uqValue << ")";

  if (index(id, table, uqColumn, uqValue) == true)
  {
    if (id != -1)
    {
      logErr("Item already exists in table");

      return false;
    }
  }
  else
  {
    qDebug() << "TEST 5";
    return false;
  }

  // Send INSERT query message

  QString fields;
  QString values;

  log("TEST: " + QString::number(map.size()));

  for (QMap<QString, QString>::iterator it = map.begin(); it != map.end(); it++)
  {
    if (!it.key().isEmpty() && !it.value().isEmpty())
    {
      fields = it.key() + ",";
      values = "'" + it.value() + "',";
    }
  }

  fields.remove(fields.size() - 1, 1);
  values.remove(values.size() - 1, 1);

  QString query = "INSERT INTO mydb." + table + " (" + fields + ") "
                  "VALUES (" + values + ");";

  send(query);

  // Get id of created row

  return index(id, table, uqColumn, uqValue);
}

bool Database::del(const int &id, const QString &type)
{
  log("Remove " + type + " item with id '" + QString::number(id) + "'");

  QString table;

  if (getTable(table, type) == false)
  {
    return false;
  }

  QString query = "UPDATE mydb." + table + " SET " + table.toLower() + "_deleted='1' "
                  "WHERE " + table.toLower() + "_id='" + QString::number(id) + "';";

  return send(query);
}

bool Database::set(const QString &id, const QString &type, const QString &field, const QString &value)
{
  log("Set '" + type + "' field '" + field + "' value '" + value + "' for id '" + id + "'");

  QString table;

  if (QString::compare(type, "suppliers") == 0)
  {
    table = "Supplier";
  }
  else if (QString::compare(type, "employees") == 0)
  {
    table = "Employee";
  }
  else if (QString::compare(type, "categories") == 0)
  {
    table = "Category";
  }
  else if (QString::compare(type, "products") == 0)
  {
    table = "Item";
  }
  else
  {
    logErr("Unknown type '" + type + "'");

    return false;
  }

  QString query = "UPDATE mydb." + table + " SET " + table.toLower() + "_" + field + "='" + value +
                "' WHERE " + table.toLower() + "_id='" + id + "';";

  return send(query);
}

bool Database::check(int &id, const QString &type, const QVector<int> &ids, const QVector<int> &amounts, const QString &total, const QString &tax)
{
  if (ids.size() != amounts.size())
  {
    logErr("Wrong id/amount vector size " + QString::number(ids.size()) + "/" + QString::number(amounts.size()));

    return false;
  }

  QString table    = "Check";
  QString employee = "1";
  QString stamp = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");

  // Print check

  QString file = type + "_" + stamp;

  QVector<Item> vec;

  getProducts(vec);

  getPrinter() -> printCheck(type, vec, stamp, total, tax, file);

  // Save to db

  QString query = "INSERT INTO mydb." + table + " (check_timestamp,check_employee,check_document,check_price,check_type) "
                  "VALUES ('" + stamp + "','"  + employee + "','" + file + "','" + tax + "','" + type + "');";

  send(query);

  return index(id, table, "check_timestamp", stamp);
}

bool Database::checkPassword(const QString &login, const QByteArray &password, QString &error, QString &time)
{
  log("Check user '" + login + "' password");

  // Check if user exists in db

  send("SELECT employee_id FROM mydb.Employee WHERE employee_login='" + login + "';");

  if (query_ -> size() <= 0)
  {
    error = "No such user '" + login + "'";

    logErr(error);

    return false;
  }

  // Get user id

  query_ -> first();

  int id = query_ -> value(0).toInt();

  query_ -> clear();

  // Get user password

  QString query = "SELECT employee_pass FROM mydb.Employee WHERE employee_id='" + QString::number(id) + "';";

  send(query);

  while (query_ -> next())
  {
    QString pass = query_ -> value(0).toString();

    qDebug() << password << pass;

    if (password == pass)
    {
      log("Valid user '" + login + "' password");

      startSession(login, time);

      return true;
    }
  }

  error = "Not valid password for user '" + login + "'";

  logErr(error);

  return false;
}

bool Database::startSession(const QString &login, QString &datetime)
{
  datetime = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");

  QString query = "INSERT INTO mydb.Session (session_start,session_employee) "
                  "VALUES ('" + datetime + "',(SELECT employee_id FROM mydb.Employee WHERE employee_login='"  + login + "'));";

  send(query);

  query = "SELECT session_id FROM mydb.Session WHERE session_start='" + datetime + "';";

  send(query);

  while (query_ -> next())
  {
    sessionId_ = query_ -> value(0).toString();

    log("Created session with id '" + sessionId_ + "'");
  }

  if (sessionId_.isEmpty())
  {
    log("Cannot get session id");

    return false;
  }

  return true;
}

bool Database::endSession()
{
  QString datetime = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");

  QString query = "UPDATE mydb.Session SET session_stop='" + datetime + "' WHERE session_id='"  + sessionId_ + "';";

  return send(query);
}

bool Database::getTable(QString &table, const QString &type)
{
  if (QString::compare(type, "suppliers") == 0)
  {
    table = "Supplier";
  }
  else if (QString::compare(type, "employees") == 0)
  {
    table = "Employee";
  }
  else if (QString::compare(type, "sessions") == 0)
  {
    table = "Session";
  }
  else if (QString::compare(type, "categories") == 0)
  {
    table = "Category";
  }
  else if (QString::compare(type, "products") == 0)
  {
    table = "Item";
  }
  else
  {
    logErr("Unknown type '" + type + "'");

    return false;
  }

  log("Table '" + table + "' for type " + type + "'");

  return true;
}
