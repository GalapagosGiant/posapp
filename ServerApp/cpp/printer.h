#ifndef PRINTER_H
#define PRINTER_H

#include "items/item.h"

#include "object.h"

class Printer : public Object
{
public:
  Printer(Logger *logger, Options *options);

  QString getName() override
  {
      return "Printer";
  }

  void printCheck(const QString &type, const QVector<Item> &vec, const QString &stamp, const QString &total, const QString &tax, const QString &file);

private:
  QString getCheckCss();
  QString getCheckHtml(const QVector<Item> &vec, const QString &stamp, const QString &total, const QString &tax);
  QString getSupplyHtml(const QVector<Item> &vec, const QString &stamp, const QString &total, const QString &tax);
};

#endif // PRINTER_H
