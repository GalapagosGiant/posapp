#include "session.h"

#include <QDataStream>
#include <QBuffer>
#include <QHostAddress>
#include <QStandardPaths>
#include <QDir>
#include <QVector>
#include <QPixmap>

#include "items/item.h"
#include "items/customeritem.h"
#include "items/employeeitem.h"
#include "items/sessionitem.h"
#include "server.h"
#include "database.h"

Session::Session(Server *server, Logger *logger, Options *options, Database *db, QWebSocket *socket, QObject *parent) :
  QObject(parent), Object(logger, options), db_(db), socket_(socket), server_(server)
{
  state_ = WaitingLogin;
}

Session::~Session()
{
  //    cleanThreadPaths();

  getDatabase() -> endSession();

  delete socket_;
}

void Session::run()
{
  log("New thread started with id: 0x" +
      QString::number((uint64_t)QThread::currentThread(), 16 ));

  //
  // Create paths.
  //

  QString random /* RandomString()*/;

  if (!random.isEmpty())
  {

  }

  ThreadPath = HomePath + "/" + random;

  if (!QDir(ThreadPath).exists())
  {
    QDir(HomePath).mkdir(random);
    QDir(ThreadPath).mkdir("image_sequence");
    QDir(ThreadPath).mkdir("output");

    log("Created directory ThreadPath=" + ThreadPath);
  }

  // Client connection.

  connect(socket_, &QWebSocket::textMessageReceived, this, &Session::processTextMessage);
  connect(socket_, &QWebSocket::binaryMessageReceived, this, &Session::processBinaryMessage);
  connect(socket_, &QWebSocket::disconnected, this, &Session::socketDisconnected);

  QString ip = socket_ -> peerAddress().toString();

  //    log("New client connected with ip: " + Logger::ipConvert(ip));

  //
  // Send HELLO message.
  //

//  send("hello", "server");

  sendMessageToClient("<HELLO> Client connected.");

//  exec();
}

qint64 Session::send(QString command, QString type, QString value)
{
  log("Send command '" + command + "'" + " type '" + type + "' value '" + value + "'");

  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  out << command << type;

  if (value.isEmpty() != 1)
  {
    out << value;
  }

  qint64 result = socket_ -> sendBinaryMessage(block);

  log("Sent " + QString::number(result) + " bytes to server");

  return result;
}

void Session::processTextMessage(QString message)
{
  log("Text message received from client");

  parse(message);
}

void Session::processBinaryMessage(const QByteArray &array)
{
  log("Received " + QString::number(array.size()) + " bytes from server");

  QDataStream in(array);

  QString command, type, value;

  in >> command >> type;

  if (QString::compare(command, "exec") == 0)
  {
    log("Parse command '" + command + "' type '" + type + "'");

    QVector<int> ids, amounts;

    QString tax, total;

    in >> ids >> amounts >> total >> tax;

    parseCheck(type, ids, amounts, total, tax);

    return;
  }
  else if (QString::compare(command, "login") == 0)
  {
    log("Parse command '" + command + "' type '" + type + "'");

    QByteArray array;

    in >> array;

    parseLogin(type, array);

    return;
  }
  else if (QString::compare(command, "add") == 0)
  {
    parseAdd(type, in);

    return;
  }

  in >> value;

  log("Parse command '" + command + "' type '" + type + "' value '" + value + "'");

  if (QString::compare(command, "hello") == 0)
  {
    parseHello(type);
  }
  else if (QString::compare(command, "get") == 0)
  {
    sendData(type, value);
  }
  else if (QString::compare(command, "set") == 0)
  {
    parseSet(type, value);
  }
  else if (QString::compare(command, "del") == 0)
  {
    parseDel(type, value);
  }
  else
  {
    logErr("Unknown command '" + command + "'");
  }
}

void Session::parseSet(const QString &type, const QString &value)
{
  log("Parse set command");

  QStringList list = type.split(QLatin1Char('.'), Qt::SkipEmptyParts);

  QString id   = list.at(0);
  QString name = list.at(1);

  if (getDatabase() -> set(id, name, list.at(2), value))
  {
    getServer() -> broadCast(name, id);
  }
}

void Session::parseAdd(const QString &type, QDataStream &in)
{
  log("Parse add new " + type + " item");

  int id = -1;

  BaseItem *item = nullptr;

  if (QString::compare(type, "suppliers") == 0)
  {
    item = new CustomerItem;

    in >> *item;
  }
  else if (QString::compare(type, "employees") == 0)
  {
    item = new EmployeeItem;

    in >> *item;
  }
  else if (QString::compare(type, "categories") == 0)
  {
    item = new Item;

    in >> *item;
  }
  else if (QString::compare(type, "products") == 0)
  {
    item = new Item;

    in >> *item;
  }
  else
  {
    logErr("Unknown data type '" + type + "'");

    return;
  }

  getDatabase() -> add(id, type, item);

  delete item;

  if (id >= 0)
  {
    getServer() -> broadCast(type, QString::number(id));
  }
}

void Session::parseDel(const QString &type, const QString &value)
{
  int id = value.toInt();

  if (getDatabase() -> del(id, type) == false)
  {
    return;
  }

  getServer() -> broadCast(type, QString::number(id), 1);

  return;
}

void Session::socketDisconnected()
{
  log("Client " + socket_ -> peerAddress().toString() + " disconnected");

  //    cleanThreadPaths();

  getDatabase() -> endSession();

  socket_ -> deleteLater();

//  exit();
  emit terminated();
}

void Session::parse(QString &message)
{
  log("Parse message '" + message + "'");

  if (QString::compare(message, "<HELLO>") == 0)
  {
    //
    // Greeting message.
    //

    log("Greeting from client.");
  }
  else if (QString::compare(message, "<ping>") == 0)
  {
    sendMessageToClient("<pong>");
  }
  else if (QString::compare(message, "<timeout> No video received.") == 0)
  {
    log("Try to send video one more time");

    //        QString path = videos_.last();

    //        if (!path.isEmpty())
    //        {
    //            sendFileToClient(path);
    //        }
  }
}

qint64 Session::sendToClient(QString path)
{
  log("Write video file to data stream");

  if (socket_ == nullptr || socket_ -> state() != QTcpSocket::ConnectedState)
    return -1;

  QByteArray arrBlock;
  QDataStream out(&arrBlock, QIODevice::WriteOnly);

  qDebug() << "Path " << path;

  QFile file(path);

  if (file.open(QIODevice::ReadOnly))
  {
    log("Opened file '" + path + "' with size " + QString::number(file.size()) + " bytes");

    QByteArray tmp = file.readAll();
    qDebug() << "Tmp size = " << tmp.size();

    quint32 size = static_cast<quint32>(tmp.size());

    out << size << tmp;
  }

  qDebug() << "quint32 size = " << sizeof(quint32);
  qDebug() << "arrBlock size = " << arrBlock.size();

  if (socket_->state() == QTcpSocket::ConnectedState)
  {
    //        qint64 result = socket_ -> write(arrBlock);

    //        log("Send to client " + QString::number(result) + " bytes");
  }

  //    socket_ -> waitForBytesWritten();

  //    socket_ -> disconnectFromHost();

  return 0;
}

qint64 Session::sendMessageToClient(QString message)
{
  log("Send '" + message + "' to client");

  if (socket_ == nullptr || socket_ -> state() != QTcpSocket::ConnectedState || message.isEmpty())
    return -1;

  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  QString type = "string";

  QByteArray bytes = message.toUtf8();

  out << bytes.size() << type << message;

  //    qint64 result = socket_ -> write(block);

  //    log("Sent " + QString::number(result) + " bytes to client");

  //    return result;
  return 0;
}

void Session::cleanThreadPaths()
{
  if (QDir(ThreadPath).exists())
  {
    QDir(ThreadPath).removeRecursively();

    log("Remove directory" + ThreadPath);
  }
}

void Session::terminating()
{
  log("Terminating current 0x" +
      QString::number((uint64_t)QThread::currentThread(), 16 ) + " thread");

  //    cleanThreadPaths();

  //    if (m_window != nullptr)
  //    {
  //        delete m_window;
  //    }

  if (socket_ != nullptr)
  {
    delete socket_;
  }
}

void Session::parseHello(const QString &type)
{
  log("Parse hello message from '" + type + "'");

  type_ = type;
}

void Session::sendData(const QString &type, const QString &id)
{
  log("Send data to client");

  if (QString::compare(type, "documents") == 0)
  {
    sendDocuments(id);
  }
  else if (QString::compare(type, "suppliers") == 0)
  {
    sendSuppliers(id);
  }
  else if (QString::compare(type, "employees") == 0)
  {
    sendEmployees(id);
  }
  else if (QString::compare(type, "sessions") == 0)
  {
    sendSessions(id);
  }
  else if (QString::compare(type, "categories") == 0)
  {
    sendGroups(id);
  }
  else if (QString::compare(type, "products") == 0)
  {
    sendProducts();
  }
  else if (QString::compare(type, "images") == 0)
  {
    sendImages(id);
  }
  else
  {
    logErr("Unknown data type '" + type + "'");
  }
}

void Session::sendDel(const QString &type, const QString &id)
{
  log("Send delete type '" + type + "' id '" + id + "'");

  send("del", type, id);
}

void Session::sendSuppliers(const QString &id)
{
  log("Send suppliers");

  QVector<CustomerItem> vec;

  getDatabase() -> getSuppliers(vec, id);

  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  QString cmd  = "set";
  QString type = "suppliers";

  out << cmd << type << vec;

  log("Send command '" + cmd + "'" + " type '" + type + "'");

  qint64 result = socket_ -> sendBinaryMessage(block);

  log("Sent " + QString::number(result) + " bytes to client");
}

void Session::sendDocuments(const QString &id)
{
  log("Send documents");

  QVector<DocumentItem> vec;

  getDatabase() -> getDocuments(vec, id);

  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  QString cmd  = "set";
  QString type = "documents";

  out << cmd << type << vec;

  log("Send command '" + cmd + "'" + " type '" + type + "'");

  qint64 result = socket_ -> sendBinaryMessage(block);

  log("Sent " + QString::number(result) + " bytes to client");
}

void Session::sendEmployees(const QString &id)
{
  log("Send employees");

  QVector<EmployeeItem> vec;

  getDatabase() -> getEmployees(vec, id);

  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  QString cmd  = "set";
  QString type = "employees";

  out << cmd << type << vec;

  log("Send command '" + cmd + "'" + " type '" + type + "'");

  qint64 result = socket_ -> sendBinaryMessage(block);

  log("Sent " + QString::number(result) + " bytes to client");
}

void Session::sendSessions(const QString &id)
{
  log("Send sessions");

  QVector<SessionItem> vec;

  getDatabase() -> getSessions(vec, id);

  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  QString cmd  = "set";
  QString type = "sessions";

  out << cmd << type << vec;

  log("Send command '" + cmd + "'" + " type '" + type + "'");

  qint64 result = socket_ -> sendBinaryMessage(block);

  log("Sent " + QString::number(result) + " bytes to client");
}

void Session::sendGroups(const QString &id)
{
  log("Send categories");

  QVector<Item> vec;

  getDatabase() -> getCategories(vec, id);

  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  QString cmd  = "set";
  QString type = "categories";

  out << cmd << type << vec;

  log("Send command '" + cmd + "'" + " type '" + type + "'");

  qint64 result = socket_ -> sendBinaryMessage(block);

  log("Sent " + QString::number(result) + " bytes to client");
}

void Session::sendProducts(const QString &id)
{
  log("Send products");

  QVector<Item> vec;

  getDatabase() -> getProducts(vec, id);

  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  QString cmd  = "set";
  QString type = "products";

  out << cmd << type << vec;

  log("Send command '" + cmd + "'" + " type '" + type + "'");

  qint64 result = socket_ -> sendBinaryMessage(block);

  log("Sent " + QString::number(result) + " bytes to client");
}

void Session::sendImages(const QString &value)
{
  log("Send images");

  // Parse ids

  QStringList ids = value.split(QLatin1Char(','), Qt::SkipEmptyParts);

  // Get image names

  QVector<QString> vec;

  getDatabase() -> getImages(ids, vec);

  // Send images

  for (int i = 0; i < vec.size(); i++)
  {
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);

    QString cmd  = "set";
    QString type = "image";

    out << cmd << type;

    QPixmap pixmap;

    QString name = vec.at(i);

    QString home = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);

    QString path = home + "/PosApp/project/" + name;

    if (!pixmap.load(path))
    {
      logErr("Can't load image '" + path + "'");
    }

    if (!name.isEmpty() && !pixmap.isNull())
    {
      qDebug() << name << pixmap;
      out << name << pixmap;
    }

    log("Send command '" + cmd + "'" + " type '" + type + "'");

    qint64 result = socket_ -> sendBinaryMessage(block);

    log("Sent " + QString::number(result) + " bytes to client");
  }
}

void Session::displayError()
{
    qDebug() << socket_ -> error();
    qDebug() << socket_ -> errorString();
}

void Session::setState(State state)
{
  if (state_ != state)
  {
    log("Set state '" + getStateName(state) + "'");

    state_ = state;

    return;
  }

  log("Alredy in state " + getStateName(state));
}

QString Session::getStateName(State state)
{
  switch (state)
  {
    case WaitingLogin:
    {
      return "WaitingLogin";
    }
    case Working:
    {
      return "Working";
    }
    default:
    {
      log("ERROR! Unknown state " + getStateName(state_) + " name");
    }
  }

  return "";
}

void Session::parseLogin(const QString &login, const QByteArray &passw)
{
  QString error, time;

  if (getDatabase() -> checkPassword(login, passw, error, time))
  {
    setState(Working);

    send("login", "done", time);
  }
  else
  {
    send("login", error);
  }
}

void Session::parseCheck(const QString &type, const QVector<int> &ids, const QVector<int> &amounts, const QString &total, const QString &tax)
{
  log("Parse " + type);

  int id = -1;

  getDatabase() -> check(id, type, ids, amounts, total, tax);

  getServer() -> broadCast("documents", QString::number(id));
}
