#include "printer.h"
#include "options.h"

#include <QDateTime>
#include <QTextDocument>
#include <QPrinter>

Printer::Printer(Logger *logger, Options *options) :
  Object(logger, options)
{

}

void Printer::printCheck(const QString &type, const QVector<Item> &vec, const QString &stamp, const QString &total, const QString &tax, const QString &file)
{
  if (vec.size() <= 0)
  {
    return;
  }

  QString path = options_ -> DocsPath + "/" + file;

  log("Print check to path '" + path + "'");

  QString html;
  QString css;

  if (QString::compare(type, "check") == 0)
  {
    html = getCheckHtml(vec, stamp, total, tax);
    css  = getCheckCss();
  }
  else if (QString::compare(type, "supply") == 0)
  {
    html = getSupplyHtml(vec, stamp, total, tax);
    css  = getCheckCss();
  }
  else
  {
    logErr("Unknown type '" + type + "' to print");

    return;
  }

  QTextDocument document;

  document.setDefaultStyleSheet(css);
  document.setHtml(html);

  QPrinter printer(QPrinter::PrinterResolution);

  printer.setOutputFormat(QPrinter::PdfFormat);
  printer.setPaperSize(QPrinter::A4);
  printer.setOutputFileName(path);
  printer.setPageMargins(QMarginsF(15, 15, 15, 15));

  document.print(&printer);
}

QString Printer::getCheckCss()
{
  QString css = ".block1 {"
    "width: 400px;"
    "height: 400px;"
    "background: #fff;"
    "float: left;"
   "}";

  return css;
}

QString Printer::getCheckHtml(const QVector<Item> &vec, const QString &stamp, const QString &total, const QString &tax)
{
  // Fill table

  QString table =

  "<div>"
    "<table width=100%>"
      "<tr>"
        "<th align=left>Description</th>"
        "<th align=right>Qty</th>"
        "<th align=right>Price</th>"
      "</tr>";


  for (int i = 0; i < vec.size(); i++)
  {
    Item item = vec.at(i);

    table +=
    "<tr>"
      "<td align=left>"  + item.name()   + "</td>"
      "<td align=right>" + QString::number(item.amount()) + "</td>"
      "<td align=right>" + QString::number(item.price())  + "</td>"
    "</tr>";
  }

  table +=

    "</table>"
  "</div>";

  // Fill doc

  QString firm = "PosApp Demo";

  QString html =

  "<div align=right>"
     "Wroclaw, " + stamp +
  "</div>"

  "<div align=left>"
     "<b>" + firm + "</b><br>"
     "street 12/34<br>"
     "123-45 Wroclaw"
  "</div>"

  "<h1 align=center>PAYMENT RECEIPT</h1>"

  + table +

  "<div class='block1'>"
  "</div>"

  "<table 'block2'"
    "<tr>"
      "<td align=left>Total</td>"
      "<td align=right>" + tax + "</td>"
    "</tr>"
    "<tr>"
      "<td align=left>Tax</td>"
      "<td align=right>" + total + "</td>"
    "</tr>"
    "<tr>"
      "<td align=left><b>Total of Purchase</b></td>"
      "<td align=right><b>" + tax + "$</b></td>"
    "</tr>"
  "</table>";

  return html;
}

QString Printer::getSupplyHtml(const QVector<Item> &vec, const QString &stamp, const QString &total, const QString &tax)
{
  // Fill table

  QString table =

  "<div>"
    "<table width=100%>"
      "<tr>"
        "<th align=left>Description</th>"
        "<th align=right>Qty</th>"
        "<th align=right>Price</th>"
      "</tr>";


  for (int i = 0; i < vec.size(); i++)
  {
    Item item = vec.at(i);

    table +=
    "<tr>"
      "<td align=left>"  + item.name()   + "</td>"
      "<td align=right>" + QString::number(item.amount()) + "</td>"
      "<td align=right>" + QString::number(item.price())  + "</td>"
    "</tr>";
  }

  table +=

    "</table>"
  "</div>";

  // Fill doc

  QString firm = "PosApp Demo";

  QString html =

  "<div align=right>"
     "Wroclaw, " + stamp +
  "</div>"

  "<div align=left>"
     "<b>" + firm + "</b><br>"
     "street 12/34<br>"
     "123-45 Wroclaw"
  "</div>"

  "<h1 align=center>INVENTARY FORM</h1>"

  + table +

  "<div class='block1'>"
  "</div>"

  "<table 'block2'"
    "<tr>"
      "<td align=left>Total</td>"
      "<td align=right>" + tax + "</td>"
    "</tr>"
    "<tr>"
      "<td align=left>Tax</td>"
      "<td align=right>" + total + "</td>"
    "</tr>"
    "<tr>"
      "<td align=left><b>Total of Purchase</b></td>"
      "<td align=right><b>" + tax + "$</b></td>"
    "</tr>"
  "</table>";

  return html;
}
