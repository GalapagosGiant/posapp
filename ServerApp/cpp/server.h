#ifndef SERVER_H
#define SERVER_H

#include "object.h"
#include "session.h"

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)

class Database;

class Server : public QObject, public Object
{
    Q_OBJECT

public:
    explicit Server(Logger *logger, Options *options, Database *db);
    ~Server();

    void start();
    void stopClients();

    void broadCast(const QString &type, const QString &id, int del = 0);

    QString getName() override
    {
        return "Server";
    }

    Database *getDatabase()
    {
      return db_;
    }

signals:
    void serverStarted(QString, QString);
    void serverStopped();

private Q_SLOTS:
    void onNewConnection();
    void deleteConnection();

private:

    int port_;
    QString host_;

    Database *db_;

    QVector<Session *> sessions_;

    QWebSocketServer *socket_;
};

#endif // SERVER_H
