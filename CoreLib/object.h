#ifndef OBJECT_H
#define OBJECT_H

#include <QString>
#include "logger.h"

class Options;

#include "CoreLib_global.h"

class CORELIB_EXPORT Object
{
public:
  Object(Logger *logger, Options *options);
  ~Object();

  virtual QString getName() = 0;

  void log(const QString &message) {
    logger_ -> write(Logger::LogLevel::DEBUG, getName(), message);
  }

  void logErr(const QString &message) {
    logger_ -> write(Logger::LogLevel::ERROR, getName(), message);
  }

  Logger *logger_;
  Options *options_;
};

#endif // OBJECT_H
