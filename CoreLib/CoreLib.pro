QT -= gui qt
QT += network websockets

TEMPLATE = lib
DEFINES += CORELIB_LIBRARY

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    client.cpp \
    clientmanager.cpp \
    corelib.cpp \
    items/baseitem.cpp \
    items/customeritem.cpp \
    items/documentitem.cpp \
    items/employeeitem.cpp \
    items/item.cpp \
    items/sessionitem.cpp \
    itemsortmodel.cpp \
    lists/customerlist.cpp \
    lists/documentlist.cpp \
    lists/employeelist.cpp \
    lists/itemlist.cpp \
    lists/sessionlist.cpp \
    logger.cpp \
    models/customermodel.cpp \
    models/documentmodel.cpp \
    models/employeemodel.cpp \
    models/graphmodel.cpp \
    models/itemlistmodel.cpp \
    models/sessionmodel.cpp \
    object.cpp \
    options.cpp \
    provider.cpp \
    style.cpp

HEADERS += \
    CoreLib_global.h \
    client.h \
    clientmanager.h \
    corelib.h \
    items/baseitem.h \
    items/customeritem.h \
    items/documentitem.h \
    items/employeeitem.h \
    items/item.h \
    items/sessionitem.h \
    itemsortmodel.h \
    lists/customerlist.h \
    lists/documentlist.h \
    lists/employeelist.h \
    lists/itemlist.h \
    lists/sessionlist.h \
    logger.h \
    models/customermodel.h \
    models/documentmodel.h \
    models/employeemodel.h \
    models/graphmodel.h \
    models/itemlistmodel.h \
    models/sessionmodel.h \
    object.h \
    options.h \
    provider.h \
    style.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
