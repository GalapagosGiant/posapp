#ifndef ITEMSORTMODEL_H
#define ITEMSORTMODEL_H

#include <QSortFilterProxyModel>

class ItemSortModel : public QSortFilterProxyModel
{
  Q_OBJECT

public:
  ItemSortModel(QObject *parent = nullptr);

  void setCategory(int parent);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
//    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

    bool itemInCat(int item) const;

    int category;
};

#endif // ITEMSORTMODEL_H
