#ifndef LOGGER_H
#define LOGGER_H

#include "CoreLib_global.h"

#include <QString>

class QFile;

class CORELIB_EXPORT Logger
{
public:
  explicit Logger(int pid, QString type);
  ~Logger();

  typedef enum
  {
    DEBUG,
    ERROR

  } LogLevel;

  void write(LogLevel level, const QString &parentClass, const QString &message);
  void startMessage();

private:

  QString prefix_;

  QFile *file;
};

#endif // LOGGER_H
