#include "object.h"

Object::Object(Logger *logger, Options *options)
  : logger_(logger), options_(options)
{

}

Object::~Object()
{
  log("Destroying object");
}
