#ifndef STYLE_H
#define STYLE_H

#include <QObject>

class Style : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString colorBack READ colorBack WRITE setColorBack NOTIFY colorBackChanged)
  Q_PROPERTY(QString colorMain READ colorMain WRITE setColorMain NOTIFY colorMainChanged)
  Q_PROPERTY(QString colorText READ colorText WRITE setColorText NOTIFY colorTextChanged)

  Q_PROPERTY(QString buttonColor READ buttonColor WRITE setButtonColor NOTIFY buttonColorChanged)
  Q_PROPERTY(QString buttonBorderColor READ buttonBorderColor WRITE setButtonBorderColor NOTIFY buttonBorderColorChanged)

  Q_PROPERTY(QString borderColorActive READ borderColorActive WRITE setBorderColorActive NOTIFY borderColorActiveChanged)
  Q_PROPERTY(QString borderColor READ borderColor WRITE setBorderColor NOTIFY borderColorChanged)
  Q_PROPERTY(int borderWidth READ borderWidth WRITE setBorderWidth NOTIFY borderWidthChanged)

  Q_PROPERTY(int radius READ radius WRITE setRadius NOTIFY radiusChanged)

public:
  Style(QString theme);

  QString colorBack() const;
  QString colorMain() const;
  QString colorText() const;

  QString buttonColor() const;
  QString buttonBorderColor() const;

  QString borderColorActive() const;
  QString borderColor() const;
  int borderWidth() const;

  int radius() const;

  void setColorBack(const QString &value);
  void setColorMain(const QString &value);
  void setColorText(const QString &value);

  void setButtonColor(const QString &value);
  void setButtonBorderColor(const QString &value);

  void setBorderColorActive(const QString &value);
  void setBorderColor(const QString &value);
  void setBorderWidth(const int &value);

  void setRadius(const int &value);

signals:

  void colorBackChanged();
  void colorMainChanged();
  void colorTextChanged();

  void buttonColorChanged();
  void buttonBorderColorChanged();

  void borderColorActiveChanged();
  void borderColorChanged();
  void borderWidthChanged();

  void radiusChanged();

private:

  QString colorBack_;
  QString colorMain_;
  QString colorText_;

  QString buttonColor_;
  QString buttonBorderColor_;

  QString borderColorActive_;
  QString borderColor_;
  int borderWidth_;

  int radius_;
};

#endif // STYLE_H
