#ifndef CLIENTMANAGER_H
#define CLIENTMANAGER_H

#include <QObject>
#include <QtGui/QPixmap>

#include "items/documentitem.h"
#include "items/customeritem.h"
#include "items/employeeitem.h"
#include "items/sessionitem.h"
#include "items/item.h"
#include "client.h"
#include "object.h"

class Provider;
class Client;

class ClientManager :  public QObject, public Object
{
  Q_OBJECT

public:
  ClientManager(QString type, Logger *logger, Options *options, Provider *provider, QObject *parent = nullptr);

  QString getName()
  {
    return "Manager";
  }

  Client client_;

  void parseDocuments(QVector<DocumentItem> &vec);
  void parseSuppliers(QVector<CustomerItem> &vec);
  void parseEmployees(QVector<EmployeeItem> &vec);
  void parseSessions(QVector<SessionItem> &vec);
  void parseGroups(QVector<Item> &vec);
  void parseProducts(QVector<Item> &vec);
  void parseImage(const QString &name, const QPixmap &map);
  void parseImages();
  void parseDel(const QString &type, const int &id);

private slots:
  void clientConnected();
  void clientLogged(QString *time);

private:

  void init();

  void updateDocuments();
  void updateSuppliers();
  void updateEmployees();
  void updateSessions();
  void updateGroups();
  void updateProducts();
  void updateData();

  int updateImages(QVector<Item> &vec);

  bool isStockType();

  // State machine

  typedef enum
  {
    Initializing,
    UpdatingDocuments,
    UpdatingSuppliers,
    UpdatingEmployees,
    UpdatingSessions,
    UpdatingGroups,
    UpdatingItems,
    UpdatingData,
    WaitingUpdate,
    Working,
    Terminating,
    Terminated

  } State;

  void runState();
  void setState(State state);
  QString getStateName(State state);

  State state_;

  QString type_;

  Provider *provider_;
};

#endif // CLIENTMANAGER_H
