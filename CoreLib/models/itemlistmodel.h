#ifndef ITEMLISTMODEL_H
#define ITEMLISTMODEL_H

#include <QAbstractListModel>

class ItemList;

class ItemListModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(ItemList *list READ list WRITE setList)

public:
  explicit ItemListModel(QObject *parent = nullptr);

  enum
  {
    NameRole = Qt::UserRole,
    ColorRole,
    ImageRole,
    PriceRole,
    AmountRole,
    IdRole,
    ParentRole
  };

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  bool setData(const QModelIndex &index, const QVariant &value,
               int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

  virtual QHash<int, QByteArray> roleNames() const override;

  ItemList *list() const;
  void setList(ItemList *list);

public slots:
  void itemUpdatedSlot(int indx, int role);

private:
  ItemList *list_;
};

#endif // ITEMLISTMODEL_H
