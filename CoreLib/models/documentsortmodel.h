#ifndef DOCUMENTSORTMODEL_H
#define DOCUMENTSORTMODEL_H

#include <QSortFilterProxyModel>

class DocumentSortModel : public QSortFilterProxyModel
{
  Q_OBJECT

public:
  DocumentSortModel(QObject *parent = nullptr);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
};

#endif // DOCUMENTSORTMODEL_H
