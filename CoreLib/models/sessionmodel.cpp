#include "sessionmodel.h"
#include "lists/sessionlist.h"

#include <QDebug>

SessionModel::SessionModel(QObject *parent)
  : QAbstractListModel(parent)
  , list_(nullptr)
{
}

int SessionModel::rowCount(const QModelIndex &parent) const
{
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if (parent.isValid() || !list_)
    return 0;

  return list_ -> items().size();
}

QVariant SessionModel::data(const QModelIndex &index, int role) const
{
//  qDebug() << "SessionModel: index.isValid() " << index.isValid();
//  qDebug() << "SessionModel: list_ " << list_;

  if (!index.isValid() || !list_)
    return QVariant();

  const SessionItem item = list_ -> items().at(index.row());

//  qDebug() << "SessionModel: name " << item.name();
//  qDebug() << "SessionModel: role " << role;

  switch (role)
  {
    case NameRole:
      return QVariant(item.name());
    case DateRole:
      return QVariant(item.date());
    case StartRole:
      return QVariant(item.start());
    case EndRole:
      return QVariant(item.end());
    case ParentRole:
      return QVariant(item.parentId());
    case IdRole:
      return QVariant(item.id());
  }

  return QVariant();
}

bool SessionModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (!list_)
    return false;

  SessionItem item = list_ -> items().at(index.row());

  switch (role)
  {
    case NameRole:
      item.setName(value.toString());
      break;
    case DateRole:
      item.setDate(value.toDate());
      break;
    case StartRole:
      item.setStart(value.toTime());
      break;
    case EndRole:
      item.setEnd(value.toTime());
      break;
    case ParentRole:
      item.setParentId(value.toInt());
      break;
    case IdRole:
      item.setId(value.toInt());
      break;
  }

  if (list_ -> setItemAt(index.row(), item))
  {
    emit dataChanged(index, index, QVector<int>() << role);

    return true;
  }
  return false;
}

Qt::ItemFlags SessionModel::flags(const QModelIndex &index) const
{
  if (!index.isValid())
    return Qt::NoItemFlags;

  return Qt::ItemIsEditable;
}

QHash<int, QByteArray> SessionModel::roleNames() const
{
  QHash<int, QByteArray> names;

  names[NameRole]   = "name";
  names[DateRole]   = "date";
  names[StartRole]  = "start";
  names[EndRole]    = "end";
  names[ParentRole] = "parentId";
  names[IdRole]     = "id";

  return names;
}

SessionList *SessionModel::list() const
{
  return list_;
}

void SessionModel::setList(SessionList *list)
{
  beginResetModel();

  if (list_)
    list_ -> disconnect(this);

  list_ = list;

  if (list_)
  {
    connect(list_, &SessionList::itemUpdated, this, &SessionModel::itemUpdatedSlot);

    connect(list_, &SessionList::preItemAppended, this, [=]() {
      const int index = list_ -> items().size();
      beginInsertRows(QModelIndex(), index, index);
    });
    connect(list_, &SessionList::postItemAppended, this, [=]() {
      endInsertRows();
    });

    connect(list_, &SessionList::preItemRemoved, this, [=](int index) {
      beginRemoveRows(QModelIndex(), index, index);
    });
    connect(list_, &SessionList::postItemRemoved, this, [=]() {
      endRemoveRows();
    });
  }

  endResetModel();
}

void SessionModel::itemUpdatedSlot(int indx, int role)
{
  role += NameRole;

  QModelIndex i = index(indx);

  qDebug() << "TESTEST: index(" << indx << "," << role << ") : " << i;

  emit dataChanged(i, i, QVector<int>() << role);
}
