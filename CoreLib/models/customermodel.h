#ifndef CUSTOMERMODEL_H
#define CUSTOMERMODEL_H

#include <QAbstractListModel>

#include "CoreLib_global.h"

class CustomerList;

class CORELIB_EXPORT CustomerModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(CustomerList *list READ list WRITE setList)

public:
  explicit CustomerModel(QObject *parent = nullptr);

  enum
  {
    NameRole = Qt::UserRole,
    ColorRole,
    MailRole,
    TelRole,
    IdRole,
    ParentRole
  };

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  bool setData(const QModelIndex &index, const QVariant &value,
               int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

  virtual QHash<int, QByteArray> roleNames() const override;

  CustomerList *list() const;
  void setList(CustomerList *list);  

public slots:
  void itemUpdatedSlot(int indx, int role);

private:
  CustomerList *list_;
};

#endif // CUSTOMERMODEL_H
