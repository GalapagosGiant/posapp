#include "graphmodel.h"
#include "lists/documentlist.h"

#include <QDebug>

GraphModel::GraphModel(QObject *parent)
  : QAbstractTableModel(parent)
  , list_(nullptr)
{
}

int GraphModel::columnCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent)

  return roleNames().size();
}

int GraphModel::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid() || !list_)
    return 0;

  return list_ -> items().size();
}

QVariant GraphModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid() || !list_ || list_ -> items().size() - 1 < index.row())
    return QVariant();

  const DocumentItem item = list_ -> items().at(index.row());

  if (role == Qt::DisplayRole)
  {
    if(index.column() == 0)
    {
      return QVariant(item.date().toMSecsSinceEpoch());
    }
    else if(index.column() == 1)
    {
      return QVariant(item.price());
    }
    else
    {
      return QVariant();
    }
  }

  return QVariant();
}

Qt::ItemFlags GraphModel::flags(const QModelIndex &index) const
{
  if (!index.isValid())
    return Qt::NoItemFlags;

  return Qt::ItemIsEditable;
}

QVariant GraphModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role != Qt::DisplayRole)
    return QVariant();

  if (orientation == Qt::Horizontal) {
    if (section % 2 == 0)
      return "x";
    else
      return "y";
  } else {
    return QString("%1").arg(section + 1);
  }
}

QHash<int, QByteArray> GraphModel::roleNames() const
{
  QHash<int, QByteArray> names;

  names[DateRole]  = "date";
  names[ValueRole] = "value";

  return names;
}

DocumentList *GraphModel::list() const
{
  return list_;
}

void GraphModel::setList(DocumentList *list)
{
  beginResetModel();

  if (list_)
    list_ -> disconnect(this);

  list_ = list;

  if (list_)
  {
    connect(list_, &DocumentList::itemUpdated, this, &GraphModel::itemUpdatedSlot);

    connect(list_, &DocumentList::preItemAppended, this, [=]() {
      const int index = list_ -> items().size();
      beginInsertRows(QModelIndex(), index, index);
    });
    connect(list_, &DocumentList::postItemAppended, this, [=]() {
      endInsertRows();
    });

    connect(list_, &DocumentList::preItemRemoved, this, [=](int index) {
      beginRemoveRows(QModelIndex(), index, index);
    });
    connect(list_, &DocumentList::postItemRemoved, this, [=]() {
      endRemoveRows();
    });
  }

  endResetModel();
}

void GraphModel::itemUpdatedSlot(int indx, int role)
{
  role += DateRole;

  QModelIndex i = index(indx, role);

  emit dataChanged(i, i, QVector<int>() << role);
}

void GraphModel::addData(QDateTime time, float value)
{
  data_.append(Data{time, value}); //Previously tried dynamic row count with begin/end row inserted.
  emit dataChanged(createIndex(0,0),createIndex(23,4));
}

