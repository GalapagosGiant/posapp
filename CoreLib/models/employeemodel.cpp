#include "employeemodel.h"
#include "lists/employeelist.h"

#include <QDebug>

EmployeeModel::EmployeeModel(QObject *parent)
  : QAbstractListModel(parent)
  , list_(nullptr)
{
}

int EmployeeModel::rowCount(const QModelIndex &parent) const
{
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if (parent.isValid() || !list_)
    return 0;

  return list_ -> items().size();
}

QVariant EmployeeModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid() || !list_)
    return QVariant();

  const EmployeeItem item = list_ -> items().at(index.row());

  switch (role)
  {
    case NameRole:
      return QVariant(item.name());
    case ColorRole:
      return QVariant(item.color());
    case PositionRole:
      return QVariant(item.position());
    case SurnameRole:
      return QVariant(item.surname());
    case LoginRole:
      return QVariant(item.login());
    case PassRole:
      return QVariant(item.passw());
    case MailRole:
      return QVariant(item.mail());
    case ParentRole:
      return QVariant(item.parentId());
    case IdRole:
      return QVariant(item.id());
  }

  return QVariant();
}

bool EmployeeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (!list_)
    return false;

  EmployeeItem item = list_ -> items().at(index.row());

  switch (role)
  {
    case NameRole:
      item.setName(value.toString());
      break;
    case ColorRole:
      item.setColor(value.toString());
      break;
    case PositionRole:
      item.setPosition(value.toString());
      break;
    case SurnameRole:
      item.setSurname(value.toString());
      break;
    case LoginRole:
      item.setLogin(value.toString());
      break;
    case PassRole:
      item.setPassw(value.toString());
      break;
    case MailRole:
      item.setMail(value.toString());
      break;
    case ParentRole:
      item.setParentId(value.toInt());
      break;
    case IdRole:
      item.setId(value.toInt());
      break;
  }

  if (list_ -> setItemAt(index.row(), item))
  {
    emit dataChanged(index, index, QVector<int>() << role);

    return true;
  }
  return false;
}

Qt::ItemFlags EmployeeModel::flags(const QModelIndex &index) const
{
  if (!index.isValid())
    return Qt::NoItemFlags;

  return Qt::ItemIsEditable;
}

QHash<int, QByteArray> EmployeeModel::roleNames() const
{
  QHash<int, QByteArray> names;

  names[NameRole]   = "name";
  names[ColorRole]  = "color";
  names[PositionRole] = "position";
  names[SurnameRole]  = "surname";
  names[LoginRole]  = "login";
  names[PassRole]   = "passwd";
  names[MailRole]   = "mail";
  names[IdRole]     = "id";
  names[ParentRole] = "parentId";

  return names;
}

EmployeeList *EmployeeModel::list() const
{
  return list_;
}

void EmployeeModel::setList(EmployeeList *list)
{
  beginResetModel();

  if (list_)
    list_ -> disconnect(this);

  list_ = list;

  if (list_)
  {
    connect(list_, &EmployeeList::itemUpdated, this, &EmployeeModel::itemUpdatedSlot);

    connect(list_, &EmployeeList::preItemAppended, this, [=]() {
      const int index = list_ -> items().size();
      beginInsertRows(QModelIndex(), index, index);
    });
    connect(list_, &EmployeeList::postItemAppended, this, [=]() {
      endInsertRows();
    });

    connect(list_, &EmployeeList::preItemRemoved, this, [=](int index) {
      beginRemoveRows(QModelIndex(), index, index);
    });
    connect(list_, &EmployeeList::postItemRemoved, this, [=]() {
      endRemoveRows();
    });
  }

  endResetModel();
}

void EmployeeModel::itemUpdatedSlot(int indx, int role)
{
  role += NameRole;

  QModelIndex i = index(indx);

  qDebug() << "TESTEST: index(" << indx << "," << role << ") : " << i;

  emit dataChanged(i, i, QVector<int>() << role);
}
