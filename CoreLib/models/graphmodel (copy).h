#ifndef GRAPHMODEL_H
#define GRAPHMODEL_H

#include <QAbstractTableModel>
#include <QDateTime>

#include "CoreLib_global.h"

class DocumentList;

class Data {
public:
  Data(QDateTime t, float v) : time(t), value(v) {};
  QDateTime time;
  float value;
};

class CORELIB_EXPORT GraphModel : public QAbstractTableModel
{
  Q_OBJECT
  Q_PROPERTY(DocumentList *list READ list WRITE setList)

public:
  explicit GraphModel(QObject *parent = nullptr);

  enum
  {
    NameRole = Qt::UserRole,
    DateRole,
    ValueRole
  };

  int columnCount(const QModelIndex& parent) const override;
  int rowCount(const QModelIndex& parent) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QHash<int, QByteArray> roleNames() const override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;

  QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

  DocumentList *list() const;
  void setList(DocumentList *list);

  void addData(QDateTime time, float value);

public slots:

  void itemUpdatedSlot(int indx, int role);

private:

  DocumentList *list_;
  QList<Data> data_;
};

#endif // GRAPHMODEL_H
