#ifndef SESSIONSORTMODEL_H
#define SESSIONSORTMODEL_H

#include <QSortFilterProxyModel>

class SessionSortModel : public QSortFilterProxyModel
{
  Q_OBJECT

public:
  SessionSortModel(QObject *parent = nullptr);

  void setParentId(int id);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

    bool itemIsChild(int parent) const;

    int id_;
};

#endif // SESSIONSORTMODEL_H
