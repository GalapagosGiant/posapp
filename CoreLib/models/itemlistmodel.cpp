#include "itemlistmodel.h"
#include "lists/itemlist.h"

#include <QDebug>

ItemListModel::ItemListModel(QObject *parent)
  : QAbstractListModel(parent)
  , list_(nullptr)
{
}

int ItemListModel::rowCount(const QModelIndex &parent) const
{
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if (parent.isValid() || !list_)
    return 0;

  return list_ -> items().size();
}

QVariant ItemListModel::data(const QModelIndex &index, int role) const
{
//  qDebug() << "ItemListModel: index.isValid() " << index.isValid();
//  qDebug() << "ItemListModel: list_ " << list_;

  if (!index.isValid() || !list_)
    return QVariant();

  const Item item = list_ -> items().at(index.row());

//  qDebug() << "ItemListModel: name " << item.name();
//  qDebug() << "ItemListModel: role " << role;

  switch (role)
  {
    case NameRole:
      return QVariant(item.name());
    case ColorRole:
      return QVariant(item.color());
    case ImageRole:
      return QVariant(item.img());
    case PriceRole:
      return QVariant(item.price());
    case AmountRole:
      return QVariant(item.amount());
    case ParentRole:
      return QVariant(item.parentId());
    case IdRole:
      return QVariant(item.id());
  }

  return QVariant();
}

bool ItemListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (!list_)
    return false;

  Item item = list_ -> items().at(index.row());

  switch (role)
  {
    case NameRole:
      item.setName(value.toString());
      break;
    case ColorRole:
      item.setColor(value.toString());
      break;
    case ImageRole:
      item.setImg(value.toString());
      break;
    case PriceRole:
      item.setPrice(value.toInt());
      break;
    case AmountRole:
      item.setAmount(value.toInt());
      break;
    case ParentRole:
      item.setParentId(value.toInt());
      break;
    case IdRole:
      item.setId(value.toInt());
      break;
  }

  if (list_ -> setItemAt(index.row(), item)) {
    emit dataChanged(index, index, QVector<int>() << role);
    return true;
  }
  return false;
}



Qt::ItemFlags ItemListModel::flags(const QModelIndex &index) const
{
  if (!index.isValid())
    return Qt::NoItemFlags;

  return Qt::ItemIsEditable;
}

QHash<int, QByteArray> ItemListModel::roleNames() const
{
  QHash<int, QByteArray> names;

  names[NameRole] = "name";
  names[ColorRole] = "color";
  names[ImageRole] = "img";
  names[PriceRole] = "price";
  names[AmountRole] = "amount";
  names[ParentRole] = "parentId";
  names[IdRole]     = "id";

  return names;
}

ItemList *ItemListModel::list() const
{
  return list_;
}

void ItemListModel::setList(ItemList *list)
{
  beginResetModel();

  if (list_)
    list_ -> disconnect(this);

  list_ = list;

  if (list_)
  {
    connect(list_, &ItemList::itemUpdated, this, &ItemListModel::itemUpdatedSlot);

    connect(list_, &ItemList::preItemAppended, this, [=]() {
      const int index = list_->items().size();
      beginInsertRows(QModelIndex(), index, index);
    });
    connect(list_, &ItemList::postItemAppended, this, [=]() {
      endInsertRows();
    });

    connect(list_, &ItemList::preItemRemoved, this, [=](int index) {
      beginRemoveRows(QModelIndex(), index, index);
    });
    connect(list_, &ItemList::postItemRemoved, this, [=]() {
      endRemoveRows();
    });
  }

  endResetModel();
}

void ItemListModel::itemUpdatedSlot(int indx, int role)
{
  role += NameRole;

  QModelIndex i = index(indx);

  emit dataChanged(i, i, QVector<int>() << role);
}
