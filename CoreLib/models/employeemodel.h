#ifndef EMPLOYEEMODEL_H
#define EMPLOYEEMODEL_H

#include <QAbstractListModel>

#include "CoreLib_global.h"

class EmployeeList;

class CORELIB_EXPORT EmployeeModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(EmployeeList *list READ list WRITE setList)

public:
  explicit EmployeeModel(QObject *parent = nullptr);

  enum
  {
    NameRole = Qt::UserRole,
    ColorRole,
    PositionRole,
    SurnameRole,
    LoginRole,
    PassRole,
    MailRole,
    IdRole,
    ParentRole
  };

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  bool setData(const QModelIndex &index, const QVariant &value,
               int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

  virtual QHash<int, QByteArray> roleNames() const override;

  EmployeeList *list() const;
  void setList(EmployeeList *list);

public slots:
  void itemUpdatedSlot(int indx, int role);

private:
  EmployeeList *list_;
};

#endif // EMPLOYEEMODEL_H
