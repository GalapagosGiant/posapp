#include "sessionsortmodel.h"
#include "models/sessionmodel.h"

#include <QDebug>

SessionSortModel::SessionSortModel(QObject *parent) :
  QSortFilterProxyModel(parent)
{

}

void SessionSortModel::setParentId(int id)
{
  id_ = id;

  invalidateFilter();
}

bool SessionSortModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
  QModelIndex index0 = sourceModel() -> index(sourceRow, 0, sourceParent);

  return (itemIsChild(sourceModel() -> data(index0, SessionModel::ParentRole).toInt()));
}

bool SessionSortModel::itemIsChild(int parent) const
{
  return id_ == parent;
}
