#include "documentmodel.h"
#include "lists/documentlist.h"

#include <QDebug>

DocumentModel::DocumentModel(QObject *parent)
  : QAbstractListModel(parent)
  , list_(nullptr)
{
}

int DocumentModel::rowCount(const QModelIndex &parent) const
{
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if (parent.isValid() || !list_)
    return 0;

  return list_ -> items().size();
}

QVariant DocumentModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid() || !list_)
    return QVariant();

  const DocumentItem item = list_ -> items().at(index.row());

  switch (role)
  {
    case NameRole:
      return QVariant(item.name());
    case DateRole:
      return QVariant(item.date());
    case DocumentRole:
      return QVariant(item.document());
    case LoginRole:
      return QVariant(item.login());
    case PriceRole:
      return QVariant(item.price());
    case ParentRole:
      return QVariant(item.parentId());
    case IdRole:
      return QVariant(item.id());
  }

  return QVariant();
}

bool DocumentModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (!list_)
    return false;

  DocumentItem item = list_ -> items().at(index.row());

  switch (role)
  {
    case NameRole:
      item.setName(value.toString());
      break;
    case DateRole:
      item.setDate(value.toDateTime());
      break;
    case DocumentRole:
      item.setDocument(value.toString());
      break;
    case LoginRole:
      item.setLogin(value.toString());
      break;
    case PriceRole:
      item.setPrice(value.toInt());
      break;
    case ParentRole:
      item.setParentId(value.toInt());
      break;
    case IdRole:
      item.setId(value.toInt());
      break;
  }

  if (list_ -> setItemAt(index.row(), item))
  {
    emit dataChanged(index, index, QVector<int>() << role);

    return true;
  }
  return false;
}

Qt::ItemFlags DocumentModel::flags(const QModelIndex &index) const
{
  if (!index.isValid())
    return Qt::NoItemFlags;

  return Qt::ItemIsEditable;
}

QHash<int, QByteArray> DocumentModel::roleNames() const
{
  QHash<int, QByteArray> names;

  names[NameRole]   = "name";
  names[DateRole]   = "date";
  names[DocumentRole]  = "document";
  names[LoginRole]  = "login";
  names[PriceRole]  = "price";
  names[IdRole]     = "id";
  names[ParentRole] = "parentId";

  return names;
}

DocumentList *DocumentModel::list() const
{
  return list_;
}

void DocumentModel::setList(DocumentList *list)
{
  beginResetModel();

  if (list_)
    list_ -> disconnect(this);

  list_ = list;

  if (list_)
  {
    connect(list_, &DocumentList::itemUpdated, this, &DocumentModel::itemUpdatedSlot);

    connect(list_, &DocumentList::preItemAppended, this, [=]() {
      const int index = list_ -> items().size();
      beginInsertRows(QModelIndex(), index, index);
    });
    connect(list_, &DocumentList::postItemAppended, this, [=]() {
      endInsertRows();
    });

    connect(list_, &DocumentList::preItemRemoved, this, [=](int index) {
      beginRemoveRows(QModelIndex(), index, index);
    });
    connect(list_, &DocumentList::postItemRemoved, this, [=]() {
      endRemoveRows();
    });
  }

  endResetModel();
}

void DocumentModel::itemUpdatedSlot(int indx, int role)
{
  role += NameRole;

  QModelIndex i = index(indx);

  qDebug() << "TESTEST: index(" << indx << "," << role << ") : " << i;

  emit dataChanged(i, i, QVector<int>() << role);
}
