#ifndef SESSIONMODEL_H
#define SESSIONMODEL_H

#include <QAbstractListModel>

#include "CoreLib_global.h"

class SessionList;

class CORELIB_EXPORT SessionModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(SessionList *list READ list WRITE setList)

public:
  explicit SessionModel(QObject *parent = nullptr);

  enum
  {
    NameRole = Qt::UserRole,
    DateRole,
    StartRole,
    EndRole,
    IdRole,
    ParentRole
  };

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  bool setData(const QModelIndex &index, const QVariant &value,
               int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

  virtual QHash<int, QByteArray> roleNames() const override;

  SessionList *list() const;
  void setList(SessionList *list);

public slots:
  void itemUpdatedSlot(int indx, int role);

private:
  SessionList *list_;
};

#endif // SESSIONMODEL_H
