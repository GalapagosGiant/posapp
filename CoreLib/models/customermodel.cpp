#include "customermodel.h"
#include "lists/customerlist.h"

#include <QDebug>

CustomerModel::CustomerModel(QObject *parent)
  : QAbstractListModel(parent)
  , list_(nullptr)
{
}

int CustomerModel::rowCount(const QModelIndex &parent) const
{
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if (parent.isValid() || !list_)
    return 0;

  return list_ -> items().size();
}

QVariant CustomerModel::data(const QModelIndex &index, int role) const
{
//  qDebug() << "CustomerModel: index.isValid() " << index.isValid();
//  qDebug() << "CustomerModel: list_ " << list_;

  if (!index.isValid() || !list_)
    return QVariant();

  const CustomerItem item = list_ -> items().at(index.row());

//  qDebug() << "CustomerModel: name " << item.name();
//  qDebug() << "CustomerModel: role " << role;

  switch (role)
  {
    case NameRole:
      return QVariant(item.name());
    case ColorRole:
      return QVariant(item.color());
    case MailRole:
      return QVariant(item.mail());
    case TelRole:
      return QVariant(item.tel());
    case ParentRole:
      return QVariant(item.parentId());
    case IdRole:
      return QVariant(item.id());
  }

  return QVariant();
}

bool CustomerModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (!list_)
    return false;

  CustomerItem item = list_ -> items().at(index.row());

  switch (role)
  {
    case NameRole:
      item.setName(value.toString());
      break;
    case ColorRole:
      item.setColor(value.toString());
      break;
    case MailRole:
      item.setMail(value.toString());
      break;
    case TelRole:
      item.setTel(value.toString());
      break;
    case ParentRole:
      item.setParentId(value.toInt());
      break;
    case IdRole:
      item.setId(value.toInt());
      break;
  }

  if (list_ -> setItemAt(index.row(), item))
  {
    emit dataChanged(index, index, QVector<int>() << role);

    return true;
  }
  return false;
}

Qt::ItemFlags CustomerModel::flags(const QModelIndex &index) const
{
  if (!index.isValid())
    return Qt::NoItemFlags;

  return Qt::ItemIsEditable;
}

QHash<int, QByteArray> CustomerModel::roleNames() const
{
  QHash<int, QByteArray> names;

  names[NameRole]   = "name";
  names[ColorRole]  = "color";
  names[MailRole]   = "mail";
  names[TelRole]    = "tel";
  names[ParentRole] = "parentId";
  names[IdRole]     = "id";

  return names;
}

CustomerList *CustomerModel::list() const
{
  return list_;
}

void CustomerModel::setList(CustomerList *list)
{
  beginResetModel();

  if (list_)
    list_ -> disconnect(this);

  list_ = list;

  if (list_)
  {
    connect(list_, &CustomerList::itemUpdated, this, &CustomerModel::itemUpdatedSlot);

    connect(list_, &CustomerList::preItemAppended, this, [=]() {
      const int index = list_ -> items().size();
      beginInsertRows(QModelIndex(), index, index);
    });
    connect(list_, &CustomerList::postItemAppended, this, [=]() {
      endInsertRows();
    });

    connect(list_, &CustomerList::preItemRemoved, this, [=](int index) {
      beginRemoveRows(QModelIndex(), index, index);
    });
    connect(list_, &CustomerList::postItemRemoved, this, [=]() {
      endRemoveRows();
    });
  }

  endResetModel();
}

void CustomerModel::itemUpdatedSlot(int indx, int role)
{
  role += NameRole;

  QModelIndex i = index(indx);

  qDebug() << "TESTEST: index(" << indx << "," << role << ") : " << i;

  emit dataChanged(i, i, QVector<int>() << role);
}
