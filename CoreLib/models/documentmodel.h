#ifndef DOCUMENTMODEL_H
#define DOCUMENTMODEL_H

#include <QAbstractListModel>

#include "CoreLib_global.h"

class DocumentList;

class CORELIB_EXPORT DocumentModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(DocumentList *list READ list WRITE setList)

public:
  explicit DocumentModel(QObject *parent = nullptr);

  enum
  {
    NameRole = Qt::UserRole,
    DateRole,
    DocumentRole,
    LoginRole,
    PriceRole,
    IdRole,
    ParentRole
  };

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  bool setData(const QModelIndex &index, const QVariant &value,
               int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

  virtual QHash<int, QByteArray> roleNames() const override;

  DocumentList *list() const;
  void setList(DocumentList *list);

public slots:
  void itemUpdatedSlot(int indx, int role);

private:
  DocumentList *list_;
};

#endif // DOCUMENTMODEL_H
