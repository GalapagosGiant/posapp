#include "documentsortmodel.h"
#include "models/documentmodel.h"

#include <QDebug>

DocumentSortModel::DocumentSortModel(QObject *parent) :
  QSortFilterProxyModel(parent)
{

}

bool DocumentSortModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
  QModelIndex index0 = sourceModel() -> index(sourceRow, 0, sourceParent);

  qDebug() << "TEST: "<<sourceModel() -> data(index0, DocumentModel::NameRole)
              .toString() ;
  qDebug() << "TEST: "<<sourceModel() -> data(index0, DocumentModel::NameRole)
              .toString().contains(filterRegExp());

  return (sourceModel() -> data(index0, DocumentModel::NameRole)
            .toString().contains(filterRegExp()));
}
