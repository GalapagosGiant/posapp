#include "clientmanager.h"
#include "client.h"
#include "options.h"
#include "provider.h"

#include <QDateTime>
#include <QFile>
#include <QDir>

ClientManager::ClientManager(QString type, Logger *logger, Options *options, Provider *provider, QObject *parent)
  : QObject(parent),
    Object(logger, options),
    client_(type, logger, options, this),
    provider_(provider)
{
  log("Created object");

  state_ = Initializing;

  connect(&client_, SIGNAL(clientConnected()), this, SLOT(clientConnected()));
  connect(&client_, SIGNAL(clientLogged(QString *time)), this, SLOT(clientLogged(QString *time)));
}

void ClientManager::clientConnected()
{
  runState();
}

void ClientManager::runState()
{
  int wait = 0;

  while (wait == 0)
  {
    log("Run in '" + getStateName(state_) + "' state");

    switch (state_)
    {
      case Initializing:
      {
        if (isStockType())
        {
          setState(UpdatingDocuments);
        }
        else
        {
          setState(UpdatingGroups);
        }

        init();

        break;
      }
      case UpdatingDocuments:
      {
        setState(WaitingUpdate);

        updateDocuments();

        break;
      }
      case UpdatingSuppliers:
      {
        setState(WaitingUpdate);

        updateSuppliers();

        break;
      }
      case UpdatingEmployees:
      {
        setState(WaitingUpdate);

        updateEmployees();

        break;
      }
      case UpdatingSessions:
      {
        setState(WaitingUpdate);

        updateSessions();

        break;
      }
      case UpdatingGroups:
      {
        setState(WaitingUpdate);

        updateGroups();

        break;
      }
      case UpdatingItems:
      {
        setState(WaitingUpdate);

        updateProducts();

        break;
      }
      case UpdatingData:
      {
        setState(WaitingUpdate);

        updateData();

        break;
      }
      case WaitingUpdate:
      {
        wait = 1;

        break;
      }
      case Working:
      {
        wait = 1;

        break;
      }
      case Terminating:
      {
        wait = 1;

        break;
      }
      case Terminated:
      {
        wait = 1;

        break;
      }
      default:
      {
        wait = 1;

        logErr("Unknown state " + getStateName(state_));
      }
    }
  }
}

QString ClientManager::getStateName(State state)
{
  switch (state)
  {
    case Initializing:
    {
      return "Initializing";
    }
    case UpdatingDocuments:
    {
      return "UpdatingDocuments";
    }
    case UpdatingSuppliers:
    {
      return "UpdatingSuppliers";
    }
    case UpdatingEmployees:
    {
      return "UpdatingEmployees";
    }
    case UpdatingSessions:
    {
      return "UpdatingSessions";
    }
    case UpdatingGroups:
    {
      return "UpdatingGroups";
    }
    case UpdatingItems:
    {
      return "UpdatingItems";
    }
    case UpdatingData:
    {
      return "UpdatingData";
    }
    case WaitingUpdate:
    {
      return "WaitingUpdate";
    }
    case Working:
    {
      return "Working";
    }
    case Terminating:
    {
      return "Terminating";
    }
    case Terminated:
    {
      return "Terminated";
    }
    default:
    {
      log("Unknown state " + getStateName(state_) + " name");
    }
  }

  return "";
}

void ClientManager::setState(State state)
{
  if (state_ != state)
  {
    if (state_ != Terminating)
    {
      log("Set state '" + getStateName(state) + "'");

      state_ = state;

      return;
    }
  }

  log("Alredy in state " + getStateName(state));
}

void ClientManager::init()
{
}

void ClientManager::updateDocuments()
{
  client_.sendDataRequest("documents");
}

void ClientManager::updateSuppliers()
{
  client_.sendDataRequest("suppliers");
}

void ClientManager::updateEmployees()
{
  client_.sendDataRequest("employees");
}

void ClientManager::updateSessions()
{
  client_.sendDataRequest("sessions");
}

void ClientManager::updateGroups()
{
  client_.sendDataRequest("categories");
}

void ClientManager::updateProducts()
{
  client_.sendDataRequest("products");
}

void ClientManager::updateData()
{
  client_.sendDataRequest("images");
}

void ClientManager::parseDocuments(QVector<DocumentItem> &vec)
{
  log("Got documents from server");

  provider_ -> updateDocuments(vec);

  if (state_ == WaitingUpdate)
  {
    setState(UpdatingSuppliers);
  }

  runState();
}

void ClientManager::parseSuppliers(QVector<CustomerItem> &vec)
{
  log("Got suppliers from server");

  provider_ -> updateSuppliers(vec);

  if (state_ == WaitingUpdate)
  {
    setState(UpdatingEmployees);
  }

  runState();
}

void ClientManager::parseEmployees(QVector<EmployeeItem> &vec)
{
  log("Got employees from server");

  provider_ -> updateEmployees(vec);

  if (state_ == WaitingUpdate)
  {
    setState(UpdatingSessions);
  }

  runState();
}

void ClientManager::parseSessions(QVector<SessionItem> &vec)
{
  log("Got sessions from server");

  provider_ -> updateSessions(vec);

  if (state_ == WaitingUpdate)
  {
    setState(UpdatingGroups);
  }

  runState();
}

void ClientManager::parseGroups(QVector<Item> &vec)
{
  log("Got groups from server");

  provider_ -> updateGroups(vec);

  if (state_ == WaitingUpdate)
  {
    setState(UpdatingItems);
  }

  runState();
}

void ClientManager::parseProducts(QVector<Item> &vec)
{
  log("Got products from server");

  setState(Working);

  updateImages(vec);

  provider_ -> updateProducts(vec);

  runState();
}

void ClientManager::parseImage(const QString &name, const QPixmap &map)
{
  log("Got image '" + name + "' from server");

  QString path = options_ -> ImgPath + "/" + name;

  QFile file(path);

  file.open(QIODevice::WriteOnly);

  if (map.save(&file, "PNG"))
  {
    log("Saved image to '" + path + "'");
  }
  else
  {
    logErr("Can't save image to '" + path + "'");
    logErr(file.errorString());
  }
}

void ClientManager::parseImages()
{
  log("Got images from server");

  setState(Working);

  runState();
}

void ClientManager::parseDel(const QString &type, const int &id)
{
  log("Delete item type '" + type + "' wid id '" + id + "'");

  provider_ -> del(type, id);
}

int ClientManager::updateImages(QVector<Item> &vec)
{
  QVector<int> ids;

  for (int i = 0; i < vec.size(); i++)
  {
    QString path = options_ -> ImgPath + "/" + vec.at(i).img();

    vec[i].setImg("file:" + path);

    if (!QFile::exists(path))
    {
      ids.push_back(vec.at(i).id());
    }
  }

  if (ids.size() > 0)
  {
    client_.getImages(ids);

    setState(WaitingUpdate);
  }

  return ids.size();
}

bool ClientManager::isStockType()
{
  if (QString::compare(client_.getType(), "stock") == 0)
  {
    return true;
  }
  return false;
}

void ClientManager::clientLogged(QString *time)
{
  QDateTime stamp = QDateTime::fromString(*time,"yyyy-MM-dd hh:mm:ss");

  provider_ -> setTime(stamp.time().hour(), stamp.time().minute());
}
