#include "provider.h"

#include "items/item.h"
#include <QDebug>
#include <QStandardPaths>

Provider::Provider(Logger *logger, Options *options, QObject *parent) :
  QObject(parent), Object(logger, options), client_(nullptr)
{
  hour_ = 0;
  min_  = 0;

  modelGroups_.setList(&listGroups_);
  model_.setList(&listProducts_);

  proxyProducts_.setSourceModel(&model_);
  proxyProducts_.setDynamicSortFilter(true);
  proxyProducts_.setFilterRole(ItemListModel::NameRole);
}

void Provider::setHour(const int &value)
{
  if (value != hour_) {
    hour_ = value;
    emit hourChanged();
  }
}

void Provider::setMin(const int &value)
{
  if (value != min_) {
    min_ = value;
    emit minChanged();
  }
}

void Provider::setClient(Client *client)
{
  client_ = client;
}

void Provider::updateDocuments(QVector<DocumentItem> &vec)
{
// Should be overriden
}

void Provider::updateSuppliers(QVector<CustomerItem> &vec)
{
// Should be overriden
}

void Provider::updateEmployees(QVector<EmployeeItem> &vec)
{
// Should be overriden
}

void Provider::updateSessions(QVector<SessionItem> &vec)
{
// Should be overriden
}

void Provider::updateGroups(QVector<Item> &vec)
{
  log("Update " + QString::number(vec.size()) + " groups");

  listGroups_.updateList(vec);
}

void Provider::updateProducts(QVector<Item> &vec)
{
  log("Update " + QString::number(vec.size()) + " products");

  listProducts_.updateList(vec);
}

void Provider::setCategory(int id)
{
  log("Set current category id '" + QString::number(id) + "'");

  proxyProducts_.setCategory(id);
}

void Provider::filterChanged(QString text)
{
  log("Search filter changed '" + text + "'");

  QRegExp regExp(text, Qt::CaseInsensitive, QRegExp::RegExp2);

  proxyProducts_.setFilterRegExp(regExp);
}

void Provider::del(const QString &type, const int &id)
{
  log("Delete item type '" + type + "' wid id '" + id + "'");

  if (QString::compare(type, "categories") == 0)
  {
    listGroups_.removeItem(id);
  }
  else if (QString::compare(type, "products") == 0)
  {
    listProducts_.removeItem(id);
  }
  else
  {
    logErr("Unknown item type '" + type + "'");
  }
}

void Provider::setTime(const int &hour, const int &min)
{
  setHour(hour);
  setMin(min);
}

int Provider::hour() const
{
  return hour_;
}

int Provider::min() const
{
  return min_;
}
