#include "logger.h"

#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QStandardPaths>
#include <QDebug>
#include <QThread>

Logger::Logger(int pid, QString type)
{
  prefix_ = QString::number(pid) + " ";

  file = new QFile(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/" + type + ".log");

  if (file -> open(QIODevice::Append | QIODevice::Text))
  {
    qDebug() << "Log file is opened.";
  }
}

Logger::~Logger()
{
  write(DEBUG, "Logger", "Destroy logger");

  if (file -> exists())
    file -> close();
}

void Logger::write(LogLevel level, const QString &parentClass, const QString &message)
{
  qDebug() << parentClass << " " << message;

  QString text = prefix_ + QString("0x%1").arg((quintptr) this,
                                               QT_POINTER_SIZE * 2, 16, QChar('0')) + " "
          + QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ");

//  if (parentClass == "Client")
//    text += "Client\t\t";
//  else
    text += parentClass +"\t";

  if (level == ERROR)
  {
    text += "ERROR. ";
  }

  text += message + "\n";

  QTextStream out(file);

  out.setCodec("UTF-8");

  if (file -> exists()) {
    out << text;
  }
}

void Logger::startMessage()
{
  QString text = "\n\n\n\n"
                 "============================================================================\n"
                 "=========================== Starting Application ===========================\n"
                 "============================================================================\n"
                 "\n\n";

  QTextStream out(file);

  out.setCodec("UTF-8");

  if (file -> exists()) {
    out << text;
  }
}
