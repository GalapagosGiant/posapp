#include "client.h"

#include <QTimer>
#include <QDebug>
#include <QtGui/QPixmap>
#include <QFile>
#include <QDir>
#include <QStandardPaths>
#include <QHostAddress>
#include <QCryptographicHash>

#include "clientmanager.h"
#include <QMap>

Client::Client(QString type, Logger *logger, Options *options, ClientManager *manager, QObject *parent) :
  QObject(parent), Object(logger, options),
  status_("disconnected"),
  HomePath(""),
//  host_(QStringLiteral("ws://192.168.0.206:4949")),
   host_(QStringLiteral("ws://127.0.0.1:4949")),
  port_(4949),
  loadingVideo(false),
  socket_(nullptr),
  manager_(manager),
  type_(type)
{
  log("Created object");

  connect(&timerConnect_,   SIGNAL(timeout()), this, SLOT(timeout()));
  connect(&timerReconnect_, SIGNAL(timeout()), this, SLOT(timeout()));

  state_ = Initializing;
}

Client::~Client()
{
  socket_.close();
}

void Client::run()
{
  //  log("New client thread started with id: 0x" +
  //      QString::number((uint64_t)QThread::currentThread(), 16 ));

  runState();

  //  exec();
}

void Client::runState()
{
  log("Run in " + getStateName(state_) + " state");

  int wait = 0;

  while (wait == 0)
  {
    switch (state_)
    {
    case Initializing:
    {
      init();

      connectToServer();

      break;
    }
    case Connecting:
    {
      wait = 1;

      break;
    }
    case Reconnecting:
    {
      wait = 1;

      break;
    }
    case WaitingLogin:
    {
      wait = 1;

      break;
    }
    case Working:
    {
      wait = 1;

      break;
    }
    case Terminating:
    {
      wait = 1;

      break;
    }
    case Terminated:
    {
      wait = 1;

      break;
    }
    default:
    {
      wait = 1;

      log("ERROR! Unknown state " + getStateName(state_));
    }
    }
  }
}

QString Client::getStateName(State state)
{
  switch (state)
  {
    case Initializing:
    {
      return "Initializing";
    }
    case Connecting:
    {
      return "Connecting";
    }
    case Reconnecting:
    {
      return "Reconnecting";
    }
    case WaitingLogin:
    {
      return "WaitingLogin";
    }
    case Working:
    {
      return "Working";
    }
    case Terminating:
    {
      return "Terminating";
    }
    case Terminated:
    {
      return "Terminated";
    }
    default:
    {
      log("ERROR! Unknown state " + getStateName(state_) + " name");
    }
  }

  return "";
}

void Client::setState(State state)
{
  if (state_ != state)
  {
    if (state_ != Terminating)
    {
      log("Set state '" + getStateName(state) + "'");

      state_ = state;

      return;
    }
  }

  log("Alredy in state " + getStateName(state));
}

void Client::init()
{
  createPaths();
}

void Client::createPaths()
{
  log("Create paths.");

  // Set home path

  HomePath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);

  log("Set HomePath=" + HomePath);

  // Create directories if needed

  if (!QDir(HomePath + "/asl").exists())
  {
    QDir(HomePath).mkdir("asl");

    log("Created folder " + HomePath + "/asl");
  }
}

void Client::parse(const QString &message)
{
  log("Parse '" + message + "' message.");

  if (QString::compare(message, "<pong>") == 0)
  {
    QTimer::singleShot(20000, this, SLOT(sendPing()));
  }
  else
  {
    logErr("Unknown message");
  }
}

void Client::connectToServer(const QUrl &host, const int &port)
{
  host_ = host;
  port_ = port;

  connectToServer();
}

void Client::connectToServer()
{
  if (state_ == Working)
  {
    log("Already connected to server");

    return;
  }
  else if (state_ == Terminating || state_ == Terminated)
  {
    return;
  }

//  // Set connect timeout

//  int timeout = 5000;

//  log("Set connection timeout " + QString::number(timeout) + " ms");

//  timerConnect_.start(timeout);

  //

  //    log("Connect to " + host_ + ":" + QString::number(port_) + ".");

  setState(Connecting);

  connect(&socket_, &QWebSocket::connected, this, &Client::onConnected);
  connect(&socket_, &QWebSocket::disconnected, this, &Client::disconnected);
  connect(&socket_, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(gotError(QAbstractSocket::SocketError)));

  socket_.open(QUrl(host_));
}

void Client::onConnected()
{
  log("Successfully connected to " + host_.toString() + ".");

  timerConnect_.stop();
  timerReconnect_.stop();

  setState(WaitingLogin);

  setStatus("connected");

  connect(&socket_, &QWebSocket::textMessageReceived, this, &Client::onTextMessageReceived);
  connect(&socket_, &QWebSocket::binaryMessageReceived, this, &Client::onBinaryMessageReceived);

  emit qmlSignalStatusChanged(status_);

  // Send hello message

  send("hello", type_);

  emit clientConnected();
}

void Client::disconnected()
{
  qDebug() << "Client::sockDisc";

  setStatus("disconnected");

//  socket_.deleteLater();

  reconnect();
}

void Client::onTextMessageReceived(QString message)
{
  log("Received text message from server");

  parse(message);
}

void Client::onBinaryMessageReceived(const QByteArray &array)
{
  log("Received " + QString::number(array.size()) + " bytes from server");

  QDataStream in(array);

  QString command, type, value;

  in >> command >> type;

  log("Parse command '" + command + "' type '" + type +
      "' in state '" + getStateName(state_) + "'");

  if (QString::compare(command, "hello") == 0)
  {
    log("Got hello from server");
  }
  else if (QString::compare(command, "login") == 0)
  {
    in >> value;

    parseLogin(type, value);
  }
  else if (QString::compare(command, "set") == 0)
  {
    if (QString::compare(type, "documents") == 0)
    {
      log("Parse documents list");

      QVector<DocumentItem> vec;

      in >> vec;

      manager_ -> parseDocuments(vec);
    }
    else if (QString::compare(type, "suppliers") == 0)
    {
      log("Parse suppliers list");

      QVector<CustomerItem> vec;

      in >> vec;

      manager_ -> parseSuppliers(vec);
    }
    else if (QString::compare(type, "employees") == 0)
    {
      log("Parse employees list");

      QVector<EmployeeItem> vec;

      in >> vec;

      manager_ -> parseEmployees(vec);
    }
    else if (QString::compare(type, "sessions") == 0)
    {
      log("Parse sessions list");

      QVector<SessionItem> vec;

      in >> vec;

      manager_ -> parseSessions(vec);
    }
    else if (QString::compare(type, "categories") == 0)
    {
      log("Parse category list");

      QVector<Item> vec;

      in >> vec;

      manager_ -> parseGroups(vec);
    }
    else if (QString::compare(type, "products") == 0)
    {
      log("Parse product list");

      QVector<Item> vec;

      in >> vec;

      manager_ -> parseProducts(vec);
    }
    else if (QString::compare(type, "image") == 0)
    {
      log("Parse image");

      QString name;
      QPixmap map;

      in >> name >> map;

      manager_ -> parseImage(name, map);
    }
    else
    {
      logErr("Unknown type '" + type + "'");
    }
  }
  else if (QString::compare(command, "del") == 0)
  {
    in >> value;

    manager_ -> parseDel(type, value.toInt());
  }
  else
  {
    logErr("Unknown command '" + command + "'");
  }
}

qint64 Client::send(QString message)
{
  log("Send '" + message + "' to server");

  socket_.sendTextMessage(message);

  return 1;
}

qint64 Client::send(QString command, QString type, QString value)
{
  log("Send command '" + command + "'" + " type '" + type + "' value '" + value + "'");

  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  out << command << type;

  if (value.isEmpty() != 1)
  {
    out << value;
  }

  qint64 result = socket_.sendBinaryMessage(block);

  log("Sent " + QString::number(result) + " bytes to server");

  return result;
}

void Client::gotError(QAbstractSocket::SocketError err)
{
  QString error = socket_.errorString();

  log("Error! Cannot connect to host");
  log("Error: " + error);

  emit qmlGotError(error);

  if (QAbstractSocket::ConnectingState)
  {
    socket_.close();
  }

  // Set connection timer

  reconnect();
}

void Client::reconnect()
{
  if (state_ == Working || state_ == Reconnecting)
  {
    return;
  }

  // Set timer to reconnect

  int timeout = 5000;

  log("Set reconnect timeout " + QString::number(timeout) + " ms");

  timerReconnect_.start(timeout);

  setState(Reconnecting);
}

void Client::timeout()
{
  QObject *timer = sender();

  log("Timeout event");

  if (timer == &timerReconnect_)
  {
    log("Handle reconnect timer event");

    if (QAbstractSocket::ConnectingState)
    {
      socket_.close();
    }

    connectToServer();
  }
  else if (timer == &timerConnect_)
  {
    log("Trying to connect timeout");

    reconnect();
  }

  runState();
}

qint64 Client::sendPing()
{
  if (loadingVideo != 1)
  {
    send("<ping>");
  }

  return 0;
}

void Client::timeoutLoading()
{
  if (loadingVideo)
  {
    send("<timeout> No video received.");
  }

  loadingVideo = false;
}

void Client::qmlLog(QString message)
{
  logger_ -> write(Logger::LogLevel::DEBUG, "QmlApplication", message);
}

void Client::setStatus(const QString &status)
{
  if (status != status_) {
    status_ = status;
    emit statusChanged();
  }
}

QString Client::status() const
{
  return status_;
}

void Client::sendDataRequest(QString type)
{
  send("get", type);
}

void Client::getImages(const QVector<int> &vec)
{
  log("Get images for " + QString::number(vec.size()) + " items");

  QString value;

  for (int i = 0; i < vec.size(); i++)
  {
    value += QString::number(vec.at(i)) + ",";
  }

  send("get", "images", value);
}

void Client::add(const QString &type, const BaseItem *item)
{
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  QString cmd  = "add";

  out << cmd << type;

  if (QString::compare(type, "suppliers") == 0)
  {
    CustomerItem *itm = (CustomerItem *) item;

    out << *itm;
  }
  else if (QString::compare(type, "employees") == 0)
  {
    EmployeeItem *itm = (EmployeeItem *) item;

    out << *itm;
  }
  else if (QString::compare(type, "categories") == 0)
  {
    Item *itm = (Item *) item;

    out << *itm;
  }
  else if (QString::compare(type, "products") == 0)
  {
    Item *itm = (Item *) item;

    out << *itm;
  }
  else
  {
    logErr("Unknown data type '" + type + "'");

    return;
  }

  log("Send command '" + cmd + "'" + " type '" + type + "'");

  qint64 result = socket_.sendBinaryMessage(block);

  log("Sent " + QString::number(result) + " bytes to server");
}

void Client::set(const QString &id, const QString &type, const QString &value)
{
  send("set", id + "." + type, value);
}

void Client::del(const QString &id, const QString &type)
{
  send("del", type, id);
}

void Client::sendCheck(const QString type, const QVector<Item> &vec, const QString &total, const QString &tax)
{
  if (vec.empty())
  {
    return;
  }

  log("Send check");

  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);

  QString cmd  = "exec";

  out << cmd << type;

  QVector<int> ids, amounts;

  for (int i = 0; i < vec.size(); i++)
  {
    ids.push_back(vec.at(i).id());
    amounts.push_back(vec.at(i).amount());
  }

  out << ids << amounts << tax << total;

  log("Send command '" + cmd + "'" + " type '" + type + "'");

  qint64 result = socket_.sendBinaryMessage(block);

  log("Sent " + QString::number(result) + " bytes to server");
}

void Client::login(QString login, QString password)
{
  log("Send login");

  if (state_ == WaitingLogin)
  {
    QByteArray array(password.toStdString().c_str());

    QByteArray encoded = QCryptographicHash::hash(array, QCryptographicHash::Md5).toHex();

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);

    QString cmd  = "login";

    out << cmd << login << encoded;

    log("Send command '" + cmd + "'" + " username '" + login + "' pass '*****'");

    qint64 result = socket_.sendBinaryMessage(block);

    log("Sent " + QString::number(result) + " bytes to server");
  }
  else
  {
    logErr("Wrong state to send login");
  }
}

void Client::parseLogin(const QString &message, QString &time)
{
  if (message == "done")
  {
    log("Successfull login at ");

    setState(Working);

    emit clientLogged(&time);
  }
  else
  {
    logErr("Failed login with error '" + message + "'");

    if (message.isEmpty())
    {
      emit qmlGotError("Wrong user or password");
    }
    else
    {
      emit qmlGotError(message);
    }
  }
}
