#ifndef GOODSGRID_H
#define GOODSGRID_H

#include <QList>
#include <QObject>

#include "items/item.h"
#include "itemsortmodel.h"
#include "models/itemlistmodel.h"
#include "object.h"
#include "items/documentitem.h"
#include "items/customeritem.h"
#include "items/employeeitem.h"
#include "items/sessionitem.h"

#include "lists/itemlist.h"

class Client;

class Provider : public QObject, public Object
{
  Q_OBJECT
  Q_PROPERTY(double hour READ hour WRITE setHour NOTIFY hourChanged)
  Q_PROPERTY(double min  READ min  WRITE setMin  NOTIFY minChanged)

public:
  explicit Provider(Logger *logger, Options *options, QObject *parent = nullptr);

  QString getName() override
  {
    return "Provider";
  }

  void setHour(const int &value);
  void setMin(const int &value);

  int hour() const;
  int min() const;

  void setTime(const int &hour,  const int &min);
  void setClient(Client *client);

  virtual void updateDocuments(QVector<DocumentItem> &vec);
  virtual void updateSuppliers(QVector<CustomerItem> &vec);
  virtual void updateEmployees(QVector<EmployeeItem> &vec);
  virtual void updateSessions(QVector<SessionItem> &vec);
  virtual void del(const QString &type, const int &id);

  void updateGroups(QVector<Item> &vec);
  void updateProducts(QVector<Item> &vec);

  ItemList listGroups_;
  ItemList listProducts_;

  ItemListModel modelGroups_;
  ItemListModel model_;
  ItemSortModel proxyProducts_;

signals:
  void hourChanged();
  void minChanged();

public slots:
  void setCategory(int id);
  void filterChanged(QString text);

protected:
  int hour_;
  int min_;

  Client *client_;
};

#endif // GOODSGRID_H
