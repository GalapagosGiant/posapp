#ifndef CLIENT_H
#define CLIENT_H

#include "CoreLib_global.h"

#include <QObject>
#include <QtWebSockets/QWebSocket>
#include <QBuffer>
#include <QTimer>

#include "object.h"
#include "items/baseitem.h"
#include "items/item.h"

class CustomerItem;

class ClientManager;

class CORELIB_EXPORT Client : public QObject, public Object
{
  Q_OBJECT

  Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)

public:
  explicit Client(QString type, Logger *logger, Options *options, ClientManager *manager, QObject *parent = nullptr);
  ~Client();

  QString getName() override
  {
    return "Client";
  }

  QString &getType()
  {
    return type_;
  }

  void run();

  void setStatus(const QString &status);

  QString status() const;

  void sendDataRequest(QString type);
  void getImages(const QVector<int> &vec);
  void set(const QString &id, const QString &type, const QString &value);
  void del(const QString &id, const QString &type);

  void add(const QString &type, const BaseItem *item);

  // CashApp
  void sendCheck(const QString type, const QVector<Item> &vec, const QString &total, const QString &tax);

public slots:
  void login(QString login, QString password);
  void connectToServer(const QUrl &host, const int &port);

  void disconnected();
  void gotError(QAbstractSocket::SocketError);
  void reconnect();

  void timeout();

  qint64 send(QString message);
  qint64 sendPing();

  void timeoutLoading();

  void qmlLog(QString message);

Q_SIGNALS:
  void qmlSignalStatusChanged(QString status);
  void qmlSignalNewHints(QVector<QString> hosts, QVector<QString> ports);
  void qmlGotError(QString error);

  void statusChanged();

  void clientConnected();
  void clientLogged(QString *time);

private Q_SLOTS:
    void onConnected();
    void onTextMessageReceived(QString message);
    void onBinaryMessageReceived(const QByteArray &array);

private:
  void init();
  void createPaths();
  void connectToServer();
  void parse(const QString &message);
  void parseLogin(const QString &message, QString &time);

  qint64 send(QString command, QString type, QString value = "");

  // State machine

  typedef enum
  {
    Initializing,
    Connecting,
    Reconnecting,
    WaitingLogin,
    Working,
    Terminating,
    Terminated

  } State;

  void runState();
  void setState(State state);
  QString getStateName(State state);

  // Members

  QString status_;
  QString HomePath;
  QUrl host_;

  int port_;

  QMap<QString, int> hints_;

  QString currentMessage;

  int frameNumber = 0;
  int expectedSize = 0;

  bool loadingVideo;

  QByteArray buffer;

  State state_;

  QTimer timerReconnect_;
  QTimer timerConnect_;

  QUrl m_url;
  QWebSocket socket_;

  ClientManager *manager_;

  QString type_;
};

#endif // CLIENT_H
