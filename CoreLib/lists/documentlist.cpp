#include "documentlist.h"

#include <QDebug>

DocumentList::DocumentList(QObject *parent) : QObject(parent)
{
}

QVector<DocumentItem> DocumentList::items() const
{
  return list_;
}

bool DocumentList::setItemAt(int index, const DocumentItem &item)
{
  if (index < 0 || index >= list_.size())
    return false;

//  const Item &oldItem = items_.at(index);
  //    if (item.done == oldItem.done && item.description == oldItem.description)
  //        return false;

//  items_[index] = item;
  return true;
}

void DocumentList::removeItem(const int &id)
{
  for (int i = 0; i < list_.size(); i++)
  {
    if (list_.at(i).id() == id)
    {
      emit preItemRemoved(i);

      list_.removeAt(i);

      emit postItemRemoved();

      return;
    }
  }
}

void DocumentList::updateList(QVector<DocumentItem> &vec)
{
  for (int i = 0; i < vec.size(); i++)
  {
    appendItem(vec.at(i));
  }
}

void DocumentList::appendItem(const DocumentItem &item)
{
  bool found = false;

  for (int i = 0; i < list_.size(); i++)
  {
    if (item.id() == list_.at(i).id())
    {
      // Update item

      if (item.name() != list_.at(i).name())
      {
        list_[i].setName(item.name());

        emit itemUpdated(i, 0);
      }

      if (item.date() != list_.at(i).date())
      {
        list_[i].setDate(item.date());

        emit itemUpdated(i, 1);
      }

      if (item.document() != list_.at(i).document())
      {
        list_[i].setDocument(item.document());

        emit itemUpdated(i, 2);
      }

      if (item.login() != list_.at(i).login())
      {
        list_[i].setLogin(item.login());

        emit itemUpdated(i, 3);
      }

      if (item.price() != list_.at(i).price())
      {
        list_[i].setPrice(item.price());

        emit itemUpdated(i, 4);
      }

      found = true;

      break;
    }
  }

  if (!found)
  {
    emit preItemAppended();

    list_.append(item);

    emit postItemAppended();
  }
}

const DocumentItem &DocumentList::at(int index)
{
  return list_.at(index);
}
