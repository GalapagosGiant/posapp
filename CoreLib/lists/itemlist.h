#ifndef ITEMLIST_H
#define ITEMLIST_H

#include <QObject>
#include <QVector>

#include "items/item.h"

class ItemList : public QObject
{
    Q_OBJECT
public:
    explicit ItemList(QObject *parent = nullptr);

    QVector<Item> items() const;
    Item *newItems();

    void appendNewItem(int parent = -1);
    void removeNewItem();
    void updateNewItem(QString type, QString value);

    bool setItemAt(int index, const Item &item);

    void removeItem(const int &id);
    void updateList(QVector<Item> &vec);

    const Item &at(int index);

    void clear();
    void removeOrReduce(const int &id);

signals:
    void itemUpdated(int index, int role);

    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem(const Item &item);

private:
    QVector<Item> list_;

    QVector<Item *> newlist_;
};

#endif // ITEMLIST_H
