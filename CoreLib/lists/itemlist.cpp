#include "itemlist.h"

#include <QDebug>

ItemList::ItemList(QObject *parent) : QObject(parent)
{
//  items_.append({ true, QStringLiteral("Wash the car") });
//  items_.append({ false, QStringLiteral("Fix the sink") });

//  items_.append({ "Test Name Initial", "red" });
}

QVector<Item> ItemList::items() const
{
  return list_;
}

Item *ItemList::newItems()
{
  if (newlist_.size() <= 0)
    return nullptr;

  return newlist_[0];
}

void ItemList::updateNewItem(QString type, QString value)
{
  if (newlist_.size() <= 0)
  {
    return;
  }

  int i = list_.size() - 1;

  Item *item = newlist_[0];

  if (QString::compare(type, "name") == 0)
  {
    item -> setName(value);

    emit itemUpdated(i, 0);
  }
  else if (QString::compare(type, "color") == 0)
  {
    item -> setColor(value);

    emit itemUpdated(i, 1);
  }
  else if (QString::compare(type, "img") == 0)
  {
    item -> setImg(value);

    emit itemUpdated(i, 2);
  }
  else if (QString::compare(type, "price") == 0)
  {
    item -> setPrice(value.toInt());

    emit itemUpdated(i, 3);
  }
  else if (QString::compare(type, "amount") == 0)
  {
    item -> setAmount(value.toInt());

    emit itemUpdated(i, 4);
  }
}

void ItemList::appendNewItem(int parent)
{
  Item item;

  item.setAmount(0);
  item.setParentId(parent);

  emit preItemAppended();

  list_.append(item);

  newlist_.append(&list_.last());

  emit postItemAppended();
}

void ItemList::removeNewItem()
{
  if (newlist_.size() <= 0)
  {
    return;
  }

  int i = list_.size() - 1;

  emit preItemRemoved(i);

  list_.removeAt(i);
  newlist_.clear();

  emit postItemRemoved();
}

bool ItemList::setItemAt(int index, const Item &item)
{
  if (index < 0 || index >= list_.size())
    return false;

//  const Item &oldItem = items_.at(index);
  //    if (item.done == oldItem.done && item.description == oldItem.description)
  //        return false;

//  items_[index] = item;
  return true;
}

void ItemList::removeItem(const int &id)
{
  for (int i = 0; i < list_.size(); i++)
  {
    if (list_.at(i).id() == id)
    {
      emit preItemRemoved(i);

      list_.removeAt(i);

      emit postItemRemoved();

      return;
    }
  }
}

void ItemList::updateList(QVector<Item> &vec)
{
  for (int i = 0; i < vec.size(); i++)
  {
    appendItem(vec.at(i));
  }
}

void ItemList::appendItem(const Item &item)
{
  bool found = false;

  for (int i = 0; i < list_.size(); i++)
  {
    if (item.id() == list_.at(i).id())
    {
      // Update item

      if (item.name() != list_.at(i).name())
      {
        list_[i].setName  (item.name());

        emit itemUpdated(i, 0);
      }

      if (item.color() != list_.at(i).color())
      {
        list_[i].setColor  (item.color());

        emit itemUpdated(i, 1);
      }

      if (item.img() != list_.at(i).img())
      {
        list_[i].setImg(item.img());

        emit itemUpdated(i, 2);
      }

      if (item.price() != list_.at(i).price())
      {
        list_[i].setPrice(item.price());

        emit itemUpdated(i, 3);
      }

//      if (item.amount() != list_.at(i).amount())
      {
        int amount = list_[i].amount();

        list_[i].setAmount(amount + 1);

        emit itemUpdated(i, 4);
      }

      found = true;

      break;
    }
  }

  if (!found)
  {
    emit preItemAppended();

    list_.append(item);

    emit postItemAppended();
  }
}

const Item &ItemList::at(int index)
{
  return list_.at(index);
}

void ItemList::clear()
{
  while (list_.size() != 0)
  {
    int i = list_.size() - 1;

    emit preItemRemoved(i);

    list_.removeAt(i);

    emit postItemRemoved();
  }
}

void ItemList::removeOrReduce(const int &id)
{
  for (int i = 0; i < list_.size(); i++)
  {
    if (list_.at(i).id() == id)
    {
      if (list_.at(i).amount() > 1)
      {
        int amount = list_[i].amount();

        list_[i].setAmount(amount - 1);

        emit itemUpdated(i, 4);
      }
      else
      {
        emit preItemRemoved(i);

        list_.removeAt(i);

        emit postItemRemoved();
      }

      return;
    }
  }
}

