#include "sessionlist.h"

#include <QDebug>

SessionList::SessionList(QObject *parent) : QObject(parent)
{
}

QVector<SessionItem> SessionList::items() const
{
  return list_;
}

SessionItem *SessionList::newItems()
{
  if (newlist_.size() <= 0)
    return nullptr;

  return newlist_[0];
}

void SessionList::updateNewItem(QString type, QString value)
{
  if (newlist_.size() <= 0)
  {
    return;
  }

  if (QString::compare(type, "name") == 0)
  {
    newlist_[0] -> setName(value);

    emit itemUpdated(list_.size() - 1, 0);
  }
}

bool SessionList::setItemAt(int index, const SessionItem &item)
{
  if (index < 0 || index >= list_.size())
    return false;

//  const Item &oldItem = items_.at(index);
  //    if (item.done == oldItem.done && item.description == oldItem.description)
  //        return false;

//  items_[index] = item;
  return true;
}

void SessionList::removeItem(const int &id)
{
  for (int i = 0; i < list_.size(); i++)
  {
    if (list_.at(i).id() == id)
    {
      emit preItemRemoved(i);

      list_.removeAt(i);

      emit postItemRemoved();

      return;
    }
  }
}

void SessionList::updateList(QVector<SessionItem> &vec)
{
  for (int i = 0; i < vec.size(); i++)
  {
    appendItem(vec.at(i));
  }
}

void SessionList::appendItem()
{
  SessionItem item;

  emit preItemAppended();

  list_.append(item);

  newlist_.append(&list_.last());

  emit postItemAppended();
}

void SessionList::appendItem(const SessionItem &item)
{
  bool found = false;

  for (int i = 0; i < list_.size(); i++)
  {
    if (item.id() == list_.at(i).id())
    {
      // Update item

      if (item.name() != list_.at(i).name())
      {
        list_[i].setName(item.name());

        emit itemUpdated(i, 0);
      }

      if (item.date() != list_.at(i).date())
      {
        list_[i].setDate(item.date());

        emit itemUpdated(i, 1);
      }

      if (item.start() != list_.at(i).start())
      {
        list_[i].setStart(item.start());

        emit itemUpdated(i, 2);
      }

      if (item.end() != list_.at(i).end())
      {
        list_[i].setEnd(item.end());

        emit itemUpdated(i, 3);
      }

      found = true;

      break;
    }
  }

  if (!found)
  {
    emit preItemAppended();

    list_.append(item);

    emit postItemAppended();
  }
}

const SessionItem &SessionList::at(int index)
{
  return list_.at(index);
}

void SessionList::removeCompletedItems()
{
//  for (int i = 0; i < items_.size(); )
//  {
//    if (items_.at(i).amount() == 0)
//    {
//      emit preItemRemoved(i);

//      items_.removeAt(i);

//      emit postItemRemoved();
//    }
//    else
//    {
//      ++i;
//    }
//  }
}

