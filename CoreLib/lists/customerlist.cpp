#include "customerlist.h"

#include <QDebug>

CustomerList::CustomerList(QObject *parent) : QObject(parent)
{
}

QVector<CustomerItem> CustomerList::items() const
{
  return list_;
}

CustomerItem *CustomerList::newItems()
{
  if (newlist_.size() <= 0)
    return nullptr;

  return newlist_[0];
}

void CustomerList::appendNewItem()
{
  CustomerItem item;

  emit preItemAppended();

  list_.append(item);

  newlist_.append(&list_.last());

  emit postItemAppended();
}

void CustomerList::removeNewItem()
{
  if (newlist_.size() <= 0)
  {
    return;
  }

  int i = list_.size() - 1;

  emit preItemRemoved(i);

  list_.removeAt(i);
  newlist_.clear();

  emit postItemRemoved();
}

void CustomerList::updateNewItem(QString type, QString value)
{
  if (newlist_.size() <= 0)
  {
    return;
  }

  if (QString::compare(type, "name") == 0)
  {
    newlist_[0] -> setName(value);

    emit itemUpdated(list_.size() - 1, 0);
  }
}

bool CustomerList::setItemAt(int index, const CustomerItem &item)
{
  if (index < 0 || index >= list_.size())
    return false;

//  const Item &oldItem = items_.at(index);
  //    if (item.done == oldItem.done && item.description == oldItem.description)
  //        return false;

//  items_[index] = item;
  return true;
}

void CustomerList::removeItem(const int &id)
{
  for (int i = 0; i < list_.size(); i++)
  {
    if (list_.at(i).id() == id)
    {
      emit preItemRemoved(i);

      list_.removeAt(i);

      emit postItemRemoved();

      return;
    }
  }
}

void CustomerList::updateList(QVector<CustomerItem> &vec)
{
  for (int i = 0; i < vec.size(); i++)
  {
    appendItem(vec.at(i));
  }
}

void CustomerList::appendItem(const CustomerItem &item)
{
  bool found = false;

  for (int i = 0; i < list_.size(); i++)
  {
    if (item.id() == list_.at(i).id())
    {
      // Update item

      if (item.name() != list_.at(i).name())
      {
        list_[i].setName  (item.name());

        emit itemUpdated(i, 0);
      }

      if (item.color() != list_.at(i).color())
      {
        list_[i].setColor  (item.color());

        emit itemUpdated(i, 1);
      }

      if (item.mail() != list_.at(i).mail())
      {
        list_[i].setMail  (item.mail());

        emit itemUpdated(i, 2);
      }

      if (item.tel() != list_.at(i).tel())
      {
        list_[i].setTel  (item.tel());

        emit itemUpdated(i, 3);
      }

      found = true;

      break;
    }
  }

  if (!found)
  {
    emit preItemAppended();

    list_.append(item);

    emit postItemAppended();
  }
}

const CustomerItem &CustomerList::at(int index)
{
  return list_.at(index);
}

void CustomerList::removeCompletedItems()
{
//  for (int i = 0; i < items_.size(); )
//  {
//    if (items_.at(i).amount() == 0)
//    {
//      emit preItemRemoved(i);

//      items_.removeAt(i);

//      emit postItemRemoved();
//    }
//    else
//    {
//      ++i;
//    }
//  }
}

