#ifndef SESSIONLIST_H
#define SESSIONLIST_H

#include <QObject>
#include <QVector>


#include "CoreLib_global.h"

#include "items/sessionitem.h"

class CORELIB_EXPORT SessionList : public QObject
{
    Q_OBJECT
public:
    explicit SessionList(QObject *parent = nullptr);

    QVector<SessionItem> items() const;
    SessionItem *newItems();

    void updateNewItem(QString type, QString value);

    bool setItemAt(int index, const SessionItem &item);

    void removeItem(const int &id);
    void updateList(QVector<SessionItem> &vec);

    const SessionItem &at(int index);

signals:
    void itemUpdated(int index, int role);

    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem();
    void appendItem(const SessionItem &item);
    void removeCompletedItems();

private:
    QVector<SessionItem> list_;

    QVector<SessionItem *> newlist_;
};

#endif // SESSIONLIST_H
