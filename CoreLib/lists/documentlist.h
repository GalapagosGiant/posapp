#ifndef DOCUMENTLIST_H
#define DOCUMENTLIST_H

#include <QObject>
#include <QVector>

#include "CoreLib_global.h"

#include "items/documentitem.h"

class CORELIB_EXPORT DocumentList : public QObject
{
    Q_OBJECT
public:
    explicit DocumentList(QObject *parent = nullptr);

    QVector<DocumentItem> items() const;

    bool setItemAt(int index, const DocumentItem &item);

    void removeItem(const int &id);
    void updateList(QVector<DocumentItem> &vec);

    const DocumentItem &at(int index);

signals:
    void itemUpdated(int index, int role);

    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem(const DocumentItem &item);

private:
    QVector<DocumentItem> list_;
};

#endif // DOCUMENTLIST_H
