#ifndef CUSTOMERLIST_H
#define CUSTOMERLIST_H

#include <QObject>
#include <QVector>


#include "CoreLib_global.h"

#include "items/customeritem.h"

class CORELIB_EXPORT CustomerList : public QObject
{
    Q_OBJECT
public:
    explicit CustomerList(QObject *parent = nullptr);

    QVector<CustomerItem> items() const;
    CustomerItem *newItems();

    void appendNewItem();
    void removeNewItem();
    void updateNewItem(QString type, QString value);

    bool setItemAt(int index, const CustomerItem &item);

    void removeItem(const int &id);
    void updateList(QVector<CustomerItem> &vec);

    const CustomerItem &at(int index);

signals:
    void itemUpdated(int index, int role);

    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem(const CustomerItem &item);
    void removeCompletedItems();

private:
    QVector<CustomerItem> list_;

    QVector<CustomerItem *> newlist_;
};

#endif // CUSTOMERLIST_H
