#include "employeelist.h"

#include <QDebug>

EmployeeList::EmployeeList(QObject *parent) : QObject(parent)
{
}

QVector<EmployeeItem> EmployeeList::items() const
{
  return list_;
}

EmployeeItem *EmployeeList::newItems()
{
  if (newlist_.size() <= 0)
    return nullptr;

  return newlist_[0];
}

void EmployeeList::appendNewItem()
{
  EmployeeItem item;

  emit preItemAppended();

  list_.append(item);

  newlist_.append(&list_.last());

  emit postItemAppended();
}

void EmployeeList::removeNewItem()
{
  if (newlist_.size() <= 0)
  {
    return;
  }

  int i = list_.size() - 1;

  emit preItemRemoved(i);

  list_.removeAt(i);
  newlist_.clear();

  emit postItemRemoved();
}

void EmployeeList::updateNewItem(QString type, QString value)
{
  if (newlist_.size() <= 0)
  {
    return;
  }

  int i = list_.size() - 1;

  EmployeeItem *item = newlist_[0];

  if (QString::compare(type, "name") == 0)
  {
    item -> setName(value);

    emit itemUpdated(i, 0);
  }
  else if (QString::compare(type, "color") == 0)
  {
    item -> setColor(value);

    emit itemUpdated(i, 1);
  }
  else if (QString::compare(type, "position") == 0)
  {
    item -> setPosition(value);

    emit itemUpdated(i, 2);
  }
  else if (QString::compare(type, "surname") == 0)
  {
    item -> setSurname(value);

    emit itemUpdated(i, 3);
  }
  else if (QString::compare(type, "name") == 0)
  {
    item -> setLogin(value);

    emit itemUpdated(i, 4);
  }
  else if (QString::compare(type, "passw") == 0)
  {
    item -> setPassw(value);

    emit itemUpdated(i, 5);
  }
  else if (QString::compare(type, "mail") == 0)
  {
    item -> setMail(value);

    emit itemUpdated(i, 6);
  }
}

bool EmployeeList::setItemAt(int index, const EmployeeItem &item)
{
  if (index < 0 || index >= list_.size())
    return false;

//  const Item &oldItem = items_.at(index);
  //    if (item.done == oldItem.done && item.description == oldItem.description)
  //        return false;

//  items_[index] = item;
  return true;
}

void EmployeeList::removeItem(const int &id)
{
  for (int i = 0; i < list_.size(); i++)
  {
    if (list_.at(i).id() == id)
    {
      emit preItemRemoved(i);

      list_.removeAt(i);

      emit postItemRemoved();

      return;
    }
  }
}

void EmployeeList::updateList(QVector<EmployeeItem> &vec)
{
  for (int i = 0; i < vec.size(); i++)
  {
    appendItem(vec.at(i));
  }
}

void EmployeeList::appendItem(const EmployeeItem &item)
{
  bool found = false;

  for (int i = 0; i < list_.size(); i++)
  {
    if (item.id() == list_.at(i).id())
    {
      // Update item

      if (item.name() != list_.at(i).name())
      {
        list_[i].setName  (item.name());

        emit itemUpdated(i, 0);
      }

      if (item.color() != list_.at(i).color())
      {
        list_[i].setColor  (item.color());

        emit itemUpdated(i, 1);
      }

      if (item.position() != list_.at(i).position())
      {
        list_[i].setPosition  (item.position());

        emit itemUpdated(i, 2);
      }

      if (item.surname() != list_.at(i).surname())
      {
        list_[i].setSurname  (item.surname());

        emit itemUpdated(i, 3);
      }

      if (item.login() != list_.at(i).login())
      {
        list_[i].setLogin  (item.login());

        emit itemUpdated(i, 4);
      }

      if (item.passw() != list_.at(i).passw())
      {
        list_[i].setPassw  (item.passw());

        emit itemUpdated(i, 5);
      }

      if (item.mail() != list_.at(i).mail())
      {
        list_[i].setMail  (item.mail());

        emit itemUpdated(i, 6);
      }

      found = true;

      break;
    }
  }

  if (!found)
  {
    emit preItemAppended();

    list_.append(item);

    emit postItemAppended();
  }
}

const EmployeeItem &EmployeeList::at(int index)
{
  return list_.at(index);
}

void EmployeeList::removeCompletedItems()
{
//  for (int i = 0; i < items_.size(); )
//  {
//    if (items_.at(i).amount() == 0)
//    {
//      emit preItemRemoved(i);

//      items_.removeAt(i);

//      emit postItemRemoved();
//    }
//    else
//    {
//      ++i;
//    }
//  }
}

