#ifndef EMPLOYEELIST_H
#define EMPLOYEELIST_H

#include <QObject>
#include <QVector>

#include "CoreLib_global.h"

#include "items/employeeitem.h"

class CORELIB_EXPORT EmployeeList : public QObject
{
    Q_OBJECT
public:
    explicit EmployeeList(QObject *parent = nullptr);

    QVector<EmployeeItem> items() const;
    EmployeeItem *newItems();

    void appendNewItem();
    void removeNewItem();
    void updateNewItem(QString type, QString value);

    bool setItemAt(int index, const EmployeeItem &item);

    void removeItem(const int &id);
    void updateList(QVector<EmployeeItem> &vec);

    const EmployeeItem &at(int index);

signals:
    void itemUpdated(int index, int role);

    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem(const EmployeeItem &item);
    void removeCompletedItems();

private:
    QVector<EmployeeItem> list_;

    QVector<EmployeeItem *> newlist_;
};

#endif // EMPLOYEELIST_H
