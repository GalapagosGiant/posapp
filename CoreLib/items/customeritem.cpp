#include "customeritem.h"

#include <QDebug>

CustomerItem::CustomerItem() :
  CustomerItem(-1, -1, "", "", "", "")
{

}


CustomerItem::CustomerItem(int id, int parentId, QString name, QString color, QString mail, QString tel, QObject *parent)
  : BaseItem(id, parentId, name, color, parent),
    mail_(mail),
    tel_(tel)
{

}

CustomerItem::CustomerItem(const CustomerItem &item) :
  CustomerItem(item.id(), item.parentId(), item.name(), item.color(), item.mail(), item.tel())
{

}

CustomerItem &CustomerItem::operator=(const CustomerItem &item)
{
  id_       = item.id();
  parentId_ = item.parentId();

  name_  = item.name();
  color_ = item.color();
  mail_  = item.mail();
  tel_   = item.tel();

  return *this;
}

bool CustomerItem::operator==(const CustomerItem &item)
{
  return item.name() == name_
           && item.color() == color_
             && item.mail() == mail_
               && item.tel() == tel_
                 && item.id() == id_
                   && item.parentId() == parentId_;
}

QDataStream &operator<<(QDataStream &out, const CustomerItem &item)
{
  out << item.name_;
  out << item.color_;
  out << item.mail_;
  out << item.tel_;

  QString id     = QString::number(item.id());
  QString parentId = QString::number(item.parentId());

  out << id;
  out << parentId;

  return out;
}

QDataStream &operator>>(QDataStream &in, CustomerItem &item)
{
  QString id, parentId;

  in >> item.name_;
  in >> item.color_;
  in >> item.mail_;
  in >> item.tel_;

  in >> id;
  in >> parentId;

  qDebug() << "TESTEST: name " << item.name_ << "color" << item.color_
           << "mail" << item.mail_ << "tel" << item.tel_
           << "id" << id << "parent" << parentId;

  item.setId      (id.toInt());
  item.setParentId(parentId.toInt());

  return in;
}

QString CustomerItem::mail() const
{
  return mail_;
}

QString CustomerItem::tel() const
{
  return tel_;
}

void CustomerItem::setMail(const QString &value)
{
  if (value != mail_)
  {
    mail_ = value;

    emit mailChanged();
  }
}

void CustomerItem::setTel(const QString &value)
{
  if (value != tel_)
  {
    tel_ = value;

    qDebug() << "TEST: emit telChanged()";

    emit telChanged();
  }
}
