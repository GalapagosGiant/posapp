#ifndef ITEM_H
#define ITEM_H

#include "items/baseitem.h"

class Item : public BaseItem
{
  Q_OBJECT

  Q_PROPERTY(QString img READ img WRITE setImg NOTIFY imgChanged)
  Q_PROPERTY(int amount READ amount WRITE setAmount NOTIFY amountChanged)
  Q_PROPERTY(int price READ price WRITE setPrice NOTIFY priceChanged)

public:
  Item();
  Item(QString name, QString color, QString img, int price, int amount,
          int id, int parentId, QObject *parent = nullptr);
  Item(const Item &item);

  Item &operator=(const Item &item);
  bool operator==(const Item &item);

  friend QDataStream &operator<<(QDataStream &out, const Item &item);
  friend QDataStream &operator>>(QDataStream &in, Item &item);

  QString img() const;
  int price() const;
  int amount() const;

  void setImg(const QString &value);
  void setPrice(const int &value);
  void setAmount(const int &value);

signals:

  void imgChanged();
  void priceChanged();
  void amountChanged();

private:

  QString img_;
  int amount_;
  int price_;
};

#endif // ITEM_H
