#include "item.h"

#include <QDebug>

Item::Item() :
  Item("", "", "", -1, -1, -1, -1)
{

}

Item::Item(QString name, QString color, QString img, int price, int amount, int id, int parentId, QObject *parent) :
  BaseItem(id, parentId, name, color, parent),
  img_(img),
  amount_(amount),
  price_(price)
{

}

Item::Item(const Item &item) :
  Item(item.name(), item.color(), item.img(), item.price(), item.amount(), item.id(), item.parentId())
{

}

Item &Item::operator=(const Item &item)
{
  name_   = item.name();
  color_  = item.color();
  amount_ = item.amount();
  price_  = item.price();
  img_    = item.img();

  return *this;
}

bool Item::operator==(const Item &item)
{
  return item.name() == name_
           && item.color() == color_
             && item.amount() == amount_
               && item.price() == price_
                   && item.img() == img_
                     && item.id() == id_
                       && item.parentId() == parentId_;
}

QDataStream &operator<<(QDataStream &out, const Item &item)
{
  out << item.name_;
  out << item.color_;
  out << item.img_;

  QString amount = QString::number(item.amount());
  QString price  = QString::number(item.price());
  QString id     = QString::number(item.id());
  QString parentId = QString::number(item.parentId());

  out << amount;
  out << price;
  out << id;
  out << parentId;

  return out;
}

QDataStream &operator>>(QDataStream &in, Item &item)
{
  QString amount, price, id, parentId;

  in >> item.name_;
  in >> item.color_;
  in >> item.img_;

  in >> amount;
  in >> price;
  in >> id;
  in >> parentId;

  qDebug() << "TESTEST: name " << item.name_ << "amount" << amount
           << "price" << price
           << "id" << id << "parent" << parentId;

  item.setAmount  (amount.toInt());
  item.setPrice   (price.toInt());
  item.setId      (id.toInt());
  item.setParentId(parentId.toInt());

  return in;
}

QString Item::img() const
{
  return img_;
}

int Item::price() const
{
  return price_;
}

int Item::amount() const
{
  return amount_;
}

void Item::setImg(const QString &value)
{
  if (value != img_)
  {
    img_ = value;

    emit imgChanged();
  }
}

void Item::setPrice(const int &value)
{
  if (value != price_)
  {
    price_ = value;

    emit priceChanged();
  }
}

void Item::setAmount(const int &value)
{
  if (value != amount_)
  {
    amount_ = value;

    emit amountChanged();
  }
}
