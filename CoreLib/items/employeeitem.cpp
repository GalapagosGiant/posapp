#include "employeeitem.h"

#include <QDebug>

EmployeeItem::EmployeeItem() :
  EmployeeItem(-1, -1, "", "", "", "", "", "")
{

}


EmployeeItem::EmployeeItem(int id, int parentId, QString name, QString surname, QString position,
                             QString login, QString password, QString mail, QObject *parent)
  : BaseItem(id, parentId, name, "", parent),
    position_(position),
    surname_(surname),
    login_(login),
    passw_(password),
    mail_(mail)
{

}

EmployeeItem::EmployeeItem(const EmployeeItem &item) :
  EmployeeItem(item.id(), item.parentId(), item.name(), item.surname(), item.position(), item.login(),
                 item.passw(), item.mail())
{

}

EmployeeItem &EmployeeItem::operator=(const EmployeeItem &item)
{
  id_       = item.id();
  parentId_ = item.parentId();

  name_  = item.name();
  color_ = item.color();

  position_ = item.position();
  surname_  = item.surname();
  login_    = item.login();
  passw_    = item.passw();
  mail_     = item.mail();

  return *this;
}

bool EmployeeItem::operator==(const EmployeeItem &item)
{
  return item.name() == name_
           && item.color() == color_
              && item.surname() == surname_
              && item.position() == position_
              && item.login() == login_
              && item.passw() == passw_
              && item.mail() == mail_
                 && item.id() == id_
                   && item.parentId() == parentId_;
}

QDataStream &operator<<(QDataStream &out, const EmployeeItem &item)
{
  out << item.name_;
  out << item.surname_;
  out << item.position_;
  out << item.login_;
  out << item.passw_;
  out << item.mail_;

  QString id     = QString::number(item.id());
  QString parentId = QString::number(item.parentId());

  out << id;
  out << parentId;

  qDebug() << "TESTESTEST: name_ " << item.name_
           << "surname_ " << item.surname_
              << "position_ " << item.position_
                 << "login_ " << item.login_
                    << "passw_ " << item.passw_
                       << "mail_ " << item.mail_
                          << "id " << item.id_
                             << "parentId " << item.parentId_;

  return out;
}

QDataStream &operator>>(QDataStream &in, EmployeeItem &item)
{
  QString id, parentId;

  in >> item.name_;
  in >> item.surname_;
  in >> item.position_;
  in >> item.login_;
  in >> item.passw_;
  in >> item.mail_;

  in >> id;
  in >> parentId;

  item.setId      (id.toInt());
  item.setParentId(parentId.toInt());

  qDebug() << "TESTESTEST: name_ " << item.name_
           << "surname_ " << item.surname_
              << "position_ " << item.position_
                 << "login_ " << item.login_
                    << "passw_ " << item.passw_
                       << "mail_ " << item.mail_
                          << "id " << item.id_
                             << "parentId " << item.parentId_;


  return in;
}

QString EmployeeItem::position() const
{
  return position_;
}

QString EmployeeItem::surname() const
{
  return surname_;
}

QString EmployeeItem::login() const
{
  return login_;
}

QString EmployeeItem::passw() const
{
  return passw_;
}

QString EmployeeItem::mail() const
{
  return mail_;
}

void EmployeeItem::setPosition(const QString &value)
{
  if (value != position_)
  {
    position_ = value;

    emit positionChanged();
  }
}

void EmployeeItem::setSurname(const QString &value)
{
  if (value != surname_)
  {
    surname_ = value;

    emit surnameChanged();
  }
}

void EmployeeItem::setLogin(const QString &value)
{
  if (value != login_)
  {
    login_ = value;

    emit loginChanged();
  }
}

void EmployeeItem::setPassw(const QString &value)
{
  if (value != passw_)
  {
    passw_ = value;

    emit passwChanged();
  }
}

void EmployeeItem::setMail(const QString &value)
{
  if (value != mail_)
  {
    mail_ = value;

    emit mailChanged();
  }
}
