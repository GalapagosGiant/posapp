#ifndef SESSIONITEM_H
#define SESSIONITEM_H

#include "items/baseitem.h"

#include <QDate>
#include <QTime>

class SessionItem : public BaseItem
{
  Q_OBJECT

  Q_PROPERTY(QTime start READ start WRITE setStart NOTIFY startChanged)
  Q_PROPERTY(QTime end   READ end   WRITE setEnd   NOTIFY endChanged)
  Q_PROPERTY(QDate date  READ date  WRITE setDate  NOTIFY dateChanged)

public:
  SessionItem();
  SessionItem(int id, int parentId, QDate date, QTime start, QTime end, QObject *parent = nullptr);
  SessionItem(const SessionItem &item);

  SessionItem &operator=(const SessionItem &item);
  bool operator==(const SessionItem &item);

  friend QDataStream &operator<<(QDataStream &out, const SessionItem &item);
  friend QDataStream &operator>>(QDataStream &in, SessionItem &item);

  QTime start() const;
  QTime end() const;
  QDate date() const;

  void setStart(const QTime &value);
  void setEnd(const QTime &value);
  void setDate(const QDate &value);

signals:

  void startChanged();
  void endChanged();
  void dateChanged();

private:

  QTime start_;
  QTime end_;
  QDate date_;
};

#endif // SESSIONITEM_H
