#ifndef DOCUMENTITEM_H
#define DOCUMENTITEM_H

#include "items/baseitem.h"

#include <QDateTime>

class DocumentItem : public BaseItem
{
  Q_OBJECT

  Q_PROPERTY(QDateTime date   READ date     WRITE setDate     NOTIFY dateChanged)
  Q_PROPERTY(QString document READ document WRITE setDocument NOTIFY documentChanged)
  Q_PROPERTY(QString login    READ login    WRITE setLogin    NOTIFY loginChanged)
  Q_PROPERTY(double price     READ price    WRITE setPrice    NOTIFY priceChanged)

public:
  DocumentItem();
  DocumentItem(int id, int parentId, QString name, QDateTime date, QString document, QString login, double price, QObject *parent = nullptr);
  DocumentItem(const DocumentItem &item);

  DocumentItem &operator=(const DocumentItem &item);
  bool operator==(const DocumentItem &item);

  friend QDataStream &operator<<(QDataStream &out, const DocumentItem &item);
  friend QDataStream &operator>>(QDataStream &in, DocumentItem &item);

  QDateTime date() const;
  QString document() const;
  QString login() const;
  double price() const;

  void setDate(const QDateTime &value);
  void setDocument(const QString &value);
  void setLogin(const QString &value);
  void setPrice(const double &value);
  void setMail(const QString &value);

signals:

  void dateChanged();
  void documentChanged();
  void loginChanged();
  void priceChanged();

private:

  QDateTime date_;
  QString document_;
  QString login_;
  double price_;
};

#endif // DOCUMENTITEM_H
