#include "sessionitem.h"

#include <QDebug>

SessionItem::SessionItem() :
  SessionItem(-1, -1, QDate(), QTime(), QTime())
{
}

SessionItem::SessionItem(int id, int parentId, QDate date, QTime start, QTime end, QObject *parent) :
  BaseItem(id, parentId, "", "", parent),
  start_(start),
  end_(end),
  date_(date)
{
}

SessionItem::SessionItem(const SessionItem &item) :
  SessionItem(item.id(), item.parentId(), item.date(), item.start(), item.end())
{
}

SessionItem &SessionItem::operator=(const SessionItem &item)
{
  setName (item.name());
  setColor(item.color());
  setDate (item.date());
  setStart(item.start());
  setEnd  (item.end());

  return *this;
}

bool SessionItem::operator==(const SessionItem &item)
{
  return item.name() == name_
           && item.color() == color_
             && item.date() == date_
               && item.start() == start_
                   && item.end() == end_
                     && item.id() == id_
                       && item.parentId() == parentId_;
}

QDataStream &operator<<(QDataStream &out, const SessionItem &item)
{
  out << item.start_;
  out << item.end_;
  out << item.date_;

  QString id     = QString::number(item.id());
  QString parentId = QString::number(item.parentId());

  out << id;
  out << parentId;

  return out;
}

QDataStream &operator>>(QDataStream &in, SessionItem &item)
{
  QString amount, price, id, parentId;

  in >> item.start_;
  in >> item.end_;
  in >> item.date_;

  in >> id;
  in >> parentId;

  qDebug() << "TESTEST: start_ " << item.start_ << "end_" << item.end_
           << "date_" << item.date_
           << "id" << id << "parent" << parentId;

  item.setId      (id.toInt());
  item.setParentId(parentId.toInt());

  return in;
}

QDate SessionItem::date() const
{
  return date_;
}

QTime SessionItem::start() const
{
  return start_;
}

QTime SessionItem::end() const
{
  return end_;
}

void SessionItem::setDate(const QDate &value)
{
  if (value != date_)
  {
    date_ = value;

    emit dateChanged();
  }
}

void SessionItem::setStart(const QTime &value)
{
  if (value != start_)
  {
    start_ = value;

    emit startChanged();
  }
}

void SessionItem::setEnd(const QTime &value)
{
  if (value != end_)
  {
    end_ = value;

    emit endChanged();
  }
}
