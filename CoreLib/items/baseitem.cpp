#include "baseitem.h"

#include <QDebug>

BaseItem::BaseItem()
  : BaseItem(-1, -1, "", "")
{

}

BaseItem::BaseItem(int id, int parentId, QString name, QString color, QObject *parent)
  : QObject(parent),
    id_(id),
    parentId_(parentId),
    name_(name),
    color_(color)
{

}

BaseItem::BaseItem(const BaseItem &item) :
  BaseItem(item.id(), item.parentId(), item.name(), item.color())
{

}

BaseItem &BaseItem::operator=(const BaseItem &item)
{
  id_       = item.id();
  parentId_ = item.parentId();
  name_     = item.name();
  color_    = item.color();

  return *this;
}

bool BaseItem::operator==(const BaseItem &item)
{
  return item.name() == name_
           && item.color() == color_
               && item.id() == id_
                 && item.parentId() == parentId_;
}

QDataStream &operator<<(QDataStream &out, const BaseItem &item)
{
  out << item.name_;
  out << item.color_;

  QString id     = QString::number(item.id());
  QString parentId = QString::number(item.parentId());

  out << id;
  out << parentId;

  return out;
}

QDataStream &operator>>(QDataStream &in, BaseItem &item)
{
  QString id, parentId;

  in >> item.name_;
  in >> item.color_;

  in >> id;
  in >> parentId;

  qDebug() << "TESTEST: name " << item.name_
           << "id" << id << "parent" << parentId;

  item.setId      (id.toInt());
  item.setParentId(parentId.toInt());

  return in;
}

QString BaseItem::name() const {
  return name_;
}

QString BaseItem::color() const {
  return color_;
}

int BaseItem::id() const
{
  return id_;
}

int BaseItem::parentId() const
{
  return parentId_;
}

void BaseItem::setName(const QString &value)
{
  if (value != name_) {
    name_ = value;
    emit nameChanged();
  }
}

void BaseItem::setColor(const QString &value)
{
  if (value != color_) {
    color_ = value;
    emit colorChanged();
  }
}

void BaseItem::setId(const int &value)
{
  if (value != id_)
  {
    id_ = value;

    emit idChanged();
  }
}

void BaseItem::setParentId(const int &value)
{
  if (value != parentId_)
  {
    parentId_ = value;
  }
}
