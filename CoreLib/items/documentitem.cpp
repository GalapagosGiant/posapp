#include "documentitem.h"

#include <QDebug>

DocumentItem::DocumentItem() :
  DocumentItem(-1, -1, "", QDateTime(), "", "", -1)
{

}


DocumentItem::DocumentItem(int id, int parentId, QString name, QDateTime date, QString document, QString login, double price, QObject *parent)
  : BaseItem(id, parentId, name, "", parent),
    date_(date),
    document_(document),
    login_(login),
    price_(price)
{

}

DocumentItem::DocumentItem(const DocumentItem &item) :
  DocumentItem(item.id(), item.parentId(), item.name(), item.date(), item.document(), item.login(), item.price())
{

}

DocumentItem &DocumentItem::operator=(const DocumentItem &item)
{
  id_       = item.id();
  parentId_ = item.parentId();

  name_  = item.name();
  date_  = item.date();
  login_ = item.login();
  price_ = item.price();
  document_  = item.document();

  return *this;
}

bool DocumentItem::operator==(const DocumentItem &item)
{
  return item.name() == name_
              && item.date() == date_
              && item.document() == document_
              && item.login() == login_
              && item.price() == price_
                 && item.id() == id_
                   && item.parentId() == parentId_;
}

QDataStream &operator<<(QDataStream &out, const DocumentItem &item)
{
  out << item.name_;
  out << item.date_;
  out << item.document_;
  out << item.login_;

  QString id     = QString::number(item.id());
  QString parentId = QString::number(item.parentId());
  QString price    = QString::number(item.price());

  out << id;
  out << parentId;
  out << price;

  qDebug() << "TEST: doc name_" << item.name_ << "date_" << item.date_ << "document_" << item.document_
           << "login_" << item.login_ << "id" << id << "parentId" << parentId
           << "price" << price;

  return out;
}

QDataStream &operator>>(QDataStream &in, DocumentItem &item)
{
  QString id, parentId, price;

  in >> item.name_;
  in >> item.date_;
  in >> item.document_;
  in >> item.login_;

  in >> id;
  in >> parentId;
  in >> price;

  item.setId      (id.toInt());
  item.setParentId(parentId.toInt());
  item.setPrice(price.toInt());

  qDebug() << "TEST: doc name_" << item.name_ << "date_" << item.date_ << "document_" << item.document_
           << "login_" << item.login_ << "id" << id << "parentId" << parentId
           << "price" << price;

  return in;
}

QDateTime DocumentItem::date() const
{
  return date_;
}

QString DocumentItem::document() const
{
  return document_;
}

QString DocumentItem::login() const
{
  return login_;
}

double DocumentItem::price() const
{
  return price_;
}

void DocumentItem::setDate(const QDateTime &value)
{
  if (value != date_)
  {
    date_ = value;

    emit dateChanged();
  }
}

void DocumentItem::setDocument(const QString &value)
{
  if (value != document_)
  {
    document_ = value;

    emit documentChanged();
  }
}

void DocumentItem::setLogin(const QString &value)
{
  if (value != login_)
  {
    login_ = value;

    emit loginChanged();
  }
}

void DocumentItem::setPrice(const double &value)
{
  if (value != price_)
  {
    price_ = value;

    emit priceChanged();
  }
}
