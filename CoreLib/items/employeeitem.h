#ifndef EMPLOYEEITEM_H
#define EMPLOYEEITEM_H

#include "items/baseitem.h"

class EmployeeItem : public BaseItem
{
  Q_OBJECT

  Q_PROPERTY(QString position READ position WRITE setPosition NOTIFY positionChanged)
  Q_PROPERTY(QString surname READ surname WRITE setSurname NOTIFY surnameChanged)
  Q_PROPERTY(QString login READ login WRITE setLogin NOTIFY loginChanged)
  Q_PROPERTY(QString passw READ passw WRITE setPassw NOTIFY passwChanged)
  Q_PROPERTY(QString mail READ mail WRITE setMail NOTIFY mailChanged)

public:
  EmployeeItem();
  EmployeeItem(int id, int parentId, QString name, QString surname, QString position,
                 QString login, QString password, QString mail, QObject *parent = nullptr);
  EmployeeItem(const EmployeeItem &item);

  EmployeeItem &operator=(const EmployeeItem &item);
  bool operator==(const EmployeeItem &item);

  friend QDataStream &operator<<(QDataStream &out, const EmployeeItem &item);
  friend QDataStream &operator>>(QDataStream &in, EmployeeItem &item);

  QString position() const;
  QString surname() const;
  QString login() const;
  QString passw() const;
  QString mail() const;

  void setPosition(const QString &value);
  void setSurname(const QString &value);
  void setLogin(const QString &value);
  void setPassw(const QString &value);
  void setMail(const QString &value);

signals:


  void positionChanged();
  void surnameChanged();
  void loginChanged();
  void passwChanged();
  void mailChanged();

private:

  QString position_;
  QString surname_;
  QString login_;
  QString passw_;
  QString mail_;
};

#endif // EMPLOYEEITEM_H
