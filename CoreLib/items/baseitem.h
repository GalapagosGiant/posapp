#ifndef BASEITEM_H
#define BASEITEM_H

#include <QObject>

class BaseItem : public QObject
{
  Q_OBJECT

  Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
  Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
  Q_PROPERTY(QString color READ color WRITE setColor NOTIFY colorChanged)

public:

  BaseItem();
  virtual ~BaseItem() {};
  BaseItem(int id, int parentId, QString name, QString color, QObject *parent = nullptr);
  BaseItem(const BaseItem &item);

  BaseItem &operator=(const BaseItem &item);
  bool operator==(const BaseItem &item);

  friend QDataStream &operator<<(QDataStream &out, const BaseItem &item);
  friend QDataStream &operator>>(QDataStream &in, BaseItem &item);

  void setId(const int &value);
  void setParentId(const int &value);
  void setName(const QString &value);
  void setColor(const QString &value);

  int id() const;
  int parentId() const;

  QString name() const;
  QString color() const;

signals:

  void nameChanged();
  void colorChanged();
  void idChanged();

protected:

  int id_;
  int parentId_;

  QString name_;
  QString color_;
};

#endif // BASEITEM_H
