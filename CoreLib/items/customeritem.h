#ifndef CUSTOMERITEM_H
#define CUSTOMERITEM_H

#include "items/baseitem.h"

class CustomerItem : public BaseItem
{
  Q_OBJECT

  Q_PROPERTY(QString mail READ mail WRITE setMail NOTIFY mailChanged)
  Q_PROPERTY(QString tel READ tel WRITE setTel NOTIFY telChanged)

public:
  CustomerItem();
  CustomerItem(int id, int parentId, QString name, QString color, QString mail, QString tel, QObject *parent = nullptr);
  CustomerItem(const CustomerItem &item);

  CustomerItem &operator=(const CustomerItem &item);
  bool operator==(const CustomerItem &item);

  friend QDataStream &operator<<(QDataStream &out, const CustomerItem &item);
  friend QDataStream &operator>>(QDataStream &in, CustomerItem &item);

  QString mail() const;
  QString tel() const;

  void setMail(const QString &value);
  void setTel(const QString &value);

signals:

  void mailChanged();
  void telChanged();

private:

  QString mail_;
  QString tel_;
};

#endif // CUSTOMERITEM_H
