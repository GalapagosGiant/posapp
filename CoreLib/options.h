#ifndef OPTIONS_H
#define OPTIONS_H

#include <QString>

#include "CoreLib_global.h"

class CORELIB_EXPORT Options
{
public:
  Options();

  QString HomePath;
  QString DocsPath;
  QString ImgPath;
};

#endif // OPTIONS_H
