#include "style.h"

Style::Style(QString theme)
{
  setRadius(5);

  if (theme == "light")
  {
    setColorBack("#ececf1");
    setColorMain("#f7f7f7");
    setColorText("#f7f7f7");
  }
  else if (theme == "dark")
  {
    setColorBack("#272b30");
    setColorMain("#3a3f44");
    setColorText("#f7f7f7");

    setButtonColor("#36b7eb");
    setButtonBorderColor("#2f9dca");

    setBorderColorActive("#36b7eb");
    setBorderColor("#17191b");
    setBorderWidth(1);
  }
}

QString Style::colorBack() const {
  return colorBack_;
}

QString Style::colorMain() const {
  return colorMain_;
}

QString Style::colorText() const {
  return colorText_;
}

QString Style::buttonColor() const {
  return buttonColor_;
}

QString Style::buttonBorderColor() const {
  return buttonBorderColor_;
}

QString Style::borderColorActive() const {
  return borderColorActive_;
}

QString Style::borderColor() const {
  return borderColor_;
}

int Style::borderWidth() const {
  return borderWidth_;
}

int Style::radius() const {
  return radius_;
}

void Style::setColorBack(const QString &value) {
  if (value != colorBack_) {
    colorBack_ = value;
    emit colorBackChanged();
  }
}

void Style::setColorMain(const QString &value) {
  if (value != colorMain_) {
    colorMain_ = value;
    emit colorMainChanged();
  }
}

void Style::setColorText(const QString &value) {
  if (value != colorText_) {
    colorText_ = value;
    emit colorTextChanged();
  }
}

void Style::setButtonColor(const QString &value) {
  if (value != buttonColor_) {
    buttonColor_ = value;
    emit buttonColorChanged();
  }
}

void Style::setButtonBorderColor(const QString &value) {
  if (value != buttonColor_) {
    buttonColor_ = value;
    emit buttonBorderColorChanged();
  }
}

void Style::setBorderColorActive(const QString &value) {
  if (value != borderColorActive_) {
    borderColorActive_ = value;
    emit borderColorActiveChanged();
  }
}

void Style::setBorderColor(const QString &value) {
  if (value != borderColor_) {
    borderColor_ = value;
    emit borderColorChanged();
  }
}

void Style::setBorderWidth(const int &value) {
  if (value != borderWidth_) {
    borderWidth_ = value;
    emit borderWidthChanged();
  }
}

void Style::setRadius(const int &value) {
  if (value != radius_) {
    radius_ = value;
    emit radiusChanged();
  }
}
