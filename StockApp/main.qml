import QtQuick 2.12
import QtQuick.Window 2.12

import "qml" as MyComponents

Window {
//    width: 640
//    height: 480
    visible: true
    visibility: "Maximized"
    title: qsTr("Stock App")

    MyComponents.MainMenu {
        id: mainScreen

        anchors.fill: parent
    }
}
