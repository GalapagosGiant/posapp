import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    property string i_name   : ""
    property int    i_id     : -1
    property int    i_amount : -1

    property int    numWidth: 50
    property int    sepWidth : 1

    Rectangle {
        id: rectId
        width:  numWidth
        height: parent.height
        color:  "transparent"

        Text {
            anchors.fill: parent
            text:  i_id
            color: Style_.colorText
        }
    } // rectId

    Rectangle {
        id: rectName
        width:  parent.width - rectId.width - rectAmount.width
        height: parent.height
        color:  "transparent"
        anchors.left: rectId.right

        Text {
            anchors.fill: parent
            text:  i_name
            color: Style_.colorText
        }
    } // rectName

    Rectangle {
        id: rectAmount
        width:  numWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectName.right

        Text {
            anchors.fill: parent
            text:  i_amount
            color: Style_.colorText
            horizontalAlignment: Text.AlignRight
        }
    } // rectAmount

} // Item
