import QtQuick 2.0
import QtCharts 2.0

Item {

    property var locale: Qt.locale()

    PageHeader {
        id:     header
        width:  parent.width
        height: parent.height * 2/15
        headerText: "Sales & Gross Profit Margin"
    } // header

    Rectangle {
        width:  parent.width
        height: (parent.height - header.height) / 2
        color:  "transparent"

        anchors.top: header.bottom

        ChartView {
            anchors.fill: parent
            antialiasing: true
            legend.visible: false
            plotAreaColor:   "transparent"
            backgroundColor: "transparent"
            theme: ChartView.ChartThemeDark

            AreaSeries {
                axisX: DateTimeAxis {
                    format: "yyyy MM dd"
                    min: Date.fromLocaleString(locale, "2021-01-01", "yyyy-MM-dd")
                    max: Date.fromLocaleString(locale, "2021-01-10", "yyyy-MM-dd")
                }
                axisY: ValueAxis {
                    min: 20
                    max: 300
                }

                upperSeries: LineSeries  {
                    VXYModelMapper {
                        model: graph_
                        xColumn: 0
                        yColumn: 1
                    }
                }

            }
        } // ChartView

    } // rectChart


}
