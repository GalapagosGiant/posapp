import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.2

Item {
    property int spacing : 15
    property int fontSize: 15

    property string textSupplies : "New supply"
    property string textProducts : "Products"

    Rectangle {
        width:  parent.width  - 2 * spacing
        height: parent.height - 2 * spacing
        color: "transparent"

        anchors.verticalCenter:   parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        PageHeader {
            id:     header
            width:  parent.width
            height: parent.height * 2/15
            headerText: textSupplies

            onClickSignal: onBackButtonClicked();
        } // header

        Rectangle {
            id: rectSupplies
            width:  parent.width
            height: (parent.height - header.height - footer.height) / 2
            color: "transparent"

            anchors.top: header.bottom

            ListView {
                id: suppliesView
                width:  parent.width  * 0.95
                height: parent.height * 0.75

                anchors.verticalCenter:   parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter

                clip: true
                model: modelSupply_

                delegate: DelegateSupplies {
                    height: fontSize * 2
                    width:  parent.width
                    i_id:     model.id
                    i_name:   model.name
                    i_amount: model.amount

                    MouseArea {
                        anchors.fill: parent
                        onClicked: Provider_.removeFromSupply(model.id)
                    }
                }
            } // suppliesView
        } // rectSupplies

        Rectangle {
            width:  parent.width
            height: 1
            color:  Style_.colorBack

            anchors.bottom: rectSupplies.bottom
        } // Separator

        Rectangle {
            id: rectProducts
            width:  parent.width
            height: rectSupplies.height
            color: "transparent"

            anchors.top: rectSupplies.bottom

            SwipeView {
                id: swipeView
                width:  parent.width  * 0.95
                height: suppliesView.height

                interactive: false
                currentIndex: 0
                clip: true

                anchors.verticalCenter:   parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter

                Page {
                    background: {
                        color: "transparent"
                    }

                    ListView {
                        id: categoriesView
                        anchors.fill: parent

                        currentIndex: 0
                        clip: true
                        model: modelCategories_

                        delegate: DelegateCategories {
                            id: categoriesDelegate
                            height: fontSize * 2
                            width:  parent.width
                            i_id:    model.id
                            i_name:  model.name
                            i_color: model.color

                            onClickSignal: {
                                categorySelected(id)
                            }
                        }
                    } // categoriesView
                }

                Page {
                    background: {
                        color: "transparent"
                    }

                    GridView {
                        id: productsView
                        anchors.fill: parent

                        cellWidth:  productsView.width / 2
                        cellHeight: fontSize * 2
                        boundsBehavior: Flickable.DragOverBounds

                        clip: true
                        model: modelProducts_

                        delegate: DelegateSupplyItems {
                            id: productsDelegate
                            width:   productsView.cellWidth * 0.8
                            height:  productsView.cellHeight
                            i_id:     model.id
                            i_name:   model.name
                            i_price:  model.price

                            MouseArea {
                                anchors.fill: parent
                                onClicked: Provider_.addToSupply(model.id)
                            }
                        }
                    } // productsView
                }

            } // swipeView
        } // rectProducts

        PageFooter {
            id:     footer
            width:  parent.width
            height: parent.height * 1/15
            type:   "supplies"

            onSearchChanged: Provider_.filterChanged(text)

            anchors.bottom: parent.bottom
        } // footer

    } // mainRect

    function categorySelected(id)
    {
        console.log("Cat selected " + id)
        header.headerText = textProducts;
        footer.parentId = id;

        swipeView.setCurrentIndex(1);

        Provider_.setCategory(id);
    }

    function onBackButtonClicked()
    {
        header.headerText = textSupplies

        swipeView.setCurrentIndex(0)

        footer.clearSearch();
    }

} // Item
