import QtQuick 2.0
import QtQuick.Controls 1.4

Item {

    signal clickSignal(int id);

    property int    i_id    : -1
    property string i_name  : ""
    property string i_color : ""

    property int    numWidth: 50
    property int    fieWidth: 150
    property int    sepWidth : 1

    Rectangle {
        id: rectId
        width:  numWidth
        height: parent.height
        color:  "transparent"

        Text {
            anchors.fill: parent
            text:  i_id
            color: Style_.colorText
        }
    } // rectId

    Rectangle {
        id: rectName
        width:  parent.width - rectId.width - rectColor.width - rectTrash.width
        height: parent.height
        color:  "transparent"
        anchors.left: rectId.right

        Text {
            anchors.fill: parent
            text:  i_name
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(timerName.running)
                {
                    loaderName.visible = true
                    loaderName.item.forceActiveFocus()

                    timerName.stop()
                }
                else
                {
                    timerName.restart()
                }
            }
            Timer {
                id: timerName
                interval: 200
                onTriggered: clickSignal(i_id)
            }
        }
    } // rectName

    Rectangle {
        id: rectColor
        width:  fieWidth * 2
        height: parent.height
        color:  "transparent"
        anchors.left: rectName.right

        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                loaderColor.visible = true
                loaderColor.item.forceActiveFocus()
            }
        }

        Rectangle {
            id: rectColorRect
            width:  parent.width  * 0.7
            height: parent.height * 0.7
            color:  i_color
            radius: Style_.radius

            border.width: Style_.borderWidth
            border.color: Style_.colorBack

            Loader {
                id: loaderColor
                anchors.fill: parent
                visible: false
                sourceComponent: visible ? inputMail: undefined

                Component {
                    id: inputMail
                    ColorPicker {
                        anchors.fill: parent
                    }
                }
            } // loaderMail

        } // rectColor

    } // rectColor

    Rectangle {
        id: rectTrash
        width:  height
        height: parent.height
        color:  "transparent"
        anchors.left: rectColor.right

        Image {
            width:  parent.width * 0.8
            height: width
            source: "qrc:/img/trash-50.png"
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                Provider_.removeItem("categories", i_id)
            }
        }
    } // rectTrash

    Loader {
        id: loaderName
        height: rectName.height
        width:  rectName.width
        anchors { verticalCenter: rectName.verticalCenter; left: rectName.left}
        visible: false
        sourceComponent: visible ? inputName : undefined

        Component {
            id: inputName
            TextField {
                anchors { fill: rectName }
                text: ""
                onAccepted:{
                    // DO STUFF
                    if (text != "") {
                        if  (i_id == -1)
                            Provider_.updateNewField("categories.name", text);
                        else
                            Provider_.updateField(i_id, "categories.name", text);
                    }

                    loaderName.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        loaderName.visible = false
                        if (text != "" && i_id == -1) {
                           Provider_.updateNewField("categories.name", text);
                        }
                    }
                }
            }
        }
    } // loaderName

} // Item
