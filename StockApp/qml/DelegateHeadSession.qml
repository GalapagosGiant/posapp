import QtQuick 2.0
import QtQuick.Controls 1.4

Item {

    property string i_id    : "ID"
    property string i_date  : "DATE"
    property string i_start : "START"
    property string i_end   : "END"

    property int    numWidth : 50
    property int    fieWidth : (width - rectId.width) / 3
    property int    sepWidth : 1

    Rectangle {
        id: rectId
        width:  numWidth
        height: parent.height
        color:  "transparent"

        Text {
            anchors.fill: parent
            text:  i_id
            color: Style_.colorText
        }
    } // rectId

    Rectangle {
        id: rectDate
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectId.right

        Text {
            anchors.fill: parent
            text:  i_date
            color: Style_.colorText
        }
    } // rectDate

    Rectangle {
        id: rectStart
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectDate.right

        Text {
            anchors.fill: parent
            text:  i_start
            color: Style_.colorText
        }
     } // rectStart

    Rectangle {
        id: rectEnd
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectStart.right

        Text {
            anchors.fill: parent
            text:  i_end
            color: Style_.colorText
        }
    } // rectLogin

} // Item
