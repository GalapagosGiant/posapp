import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    property string i_id    : "ID"
    property string i_price : "TOTAL"
    property string i_name  : "NAME"
    property string i_login : "LOGIN"
    property string i_date  : "DATE"

    property int numWidth: 50
    property int colWidth: (parent.width - rectId.width - rectPrice.width - rectDate.width - rectDoc.width) / 2

    Rectangle {
        id: rectId
        width:  numWidth
        height: parent.height
        color:  "transparent"

        Text {
            anchors.fill: parent
            text:  i_id
            color: Style_.colorText
        }
    } // rectId

    Rectangle {
        id: rectName
        width:  colWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectId.right

        Text {
            anchors.fill: parent
            text:  i_name
            color: Style_.colorText
        }
    } // rectName

    Rectangle {
        id: rectDate
        width:  parent.width / 2.5
        height: parent.height
        color:  "transparent"
        anchors.left: rectName.right

        Text {
            anchors.fill: parent
            text:  i_date
            color: Style_.colorText
        }
    } // rectAmount

    Rectangle {
        id: rectPrice
        width:  numWidth * 2
        height: parent.height
        color:  "transparent"
        anchors.left: rectDate.right

        Text {
            anchors.fill: parent
            text:  i_price
            color: Style_.colorText
        }
    } // rectPrice

    Rectangle {
        id: rectLogin
        width:  colWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectPrice.right

        Text {
            anchors.fill: parent
            text:  i_login
            color: Style_.colorText
        }
    } // rectPrice

    Rectangle {
        id: rectDoc
        width:  height
        height: parent.height
        color:  "transparent"
        anchors.left: rectLogin.right
    } // rectTrash

} // Item
