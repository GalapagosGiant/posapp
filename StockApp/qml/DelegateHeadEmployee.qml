import QtQuick 2.0
import QtQuick.Controls 1.4

Item {

    property string i_id       : "ID"
    property string i_name     : "NAME"
    property string i_surname  : "SURNAME"
    property string i_login    : "LOGIN"
    property string i_passw    : "PASSW"

    property int    numWidth: 50
    property int    fieWidth: (width - rectId.width) / 5
    property int    sepWidth : 1

    Rectangle {
        id: rectId
        width:  numWidth
        height: parent.height
        color:  "transparent"

        Text {
            anchors.fill: parent
            text:  i_id
            color: Style_.colorText
        }
    } // rectId

    Rectangle {
        id: rectName
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectId.right

        Text {
            anchors.fill: parent
            text:  i_name
            color: Style_.colorText
        }

    } // rectName

    Rectangle {
        id: rectSurname
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectName.right

        Text {
            anchors.fill: parent
            text:  i_surname
            color: Style_.colorText
        }

    } // rectName

    Rectangle {
        id: rectLogin
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectSurname.right

        Text {
            anchors.fill: parent
            text:  i_login
            color: Style_.colorText
        }

    } // rectLogin

    Rectangle {
        id: rectPassw
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectLogin.right

        Text {
            anchors.fill: parent
            text:  i_passw
            color: Style_.colorText
        }
    } // rectPassw

} // Item
