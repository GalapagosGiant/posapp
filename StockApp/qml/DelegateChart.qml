import QtQuick 2.0

Item {

    property string i_name  :  ""
    property string i_color :  ""
    property double i_total :  0

    Rectangle {
        width:  parent.width  * 0.9
        height: parent.height * 0.9
        color:  "transparent"

        anchors.verticalCenter:   parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: name
            width:  parent.width
            height: parent.height / 4
            text:  i_name
            color: Style_.colorText
            verticalAlignment: TextInput.AlignVCenter

            font.pointSize: height * 0.3
        }

        Text {
            width:  parent.width
            height: parent.height - name.height
            text:  (i_name == "Number of checks") ? i_total : i_total.toLocaleCurrencyString(Qt.locale("en_GB"), "$")
            color: (i_name == "Sales") ? "#38ad6b" : ((i_name == "Invoices") ? "#F3714F" : Style_.colorText)
            font.pointSize: height * 0.3
//            verticalAlignment: TextInput.AlignVCenter

            anchors.top: name.bottom
        }
    } // mainRect
} // Item
