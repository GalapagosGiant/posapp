import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    property int    i_id    : -1
    property int    i_price : -1
    property string i_name  : ""
    property string i_path  : ""
    property string i_login : ""
    property date   i_date

    property int numWidth: 50
    property int colWidth: (parent.width - rectId.width - rectPrice.width - rectDate.width - rectDoc.width) / 2

    Rectangle {
        id: rectId
        width:  numWidth
        height: parent.height
        color:  "transparent"

        Text {
            anchors.fill: parent
            text:  i_id
            color: Style_.colorText
        }
    } // rectId

    Rectangle {
        id: rectName
        width:  colWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectId.right

        Text {
            anchors.fill: parent
            text:  i_name
            color: Style_.colorText
        }
    } // rectName

    Rectangle {
        id: rectDate
        width:  parent.width / 2.5
        height: parent.height
        color:  "transparent"
        anchors.left: rectName.right

        Text {
            anchors.fill: parent
            text:  i_date.toDateString("YYYY-MM-DD") + " " + i_date.toTimeString("hh:mm:ss")
            color: Style_.colorText
        }
    } // rectAmount

    Rectangle {
        id: rectPrice
        width:  numWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectDate.right

        Text {
            anchors.fill: parent
            text:  i_price
            color: Style_.colorText
        }
    } // rectPrice

    Rectangle {
        id: rectLogin
        width:  colWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectPrice.right

        Text {
            anchors.fill: parent
            text:  i_login
            color: Style_.colorText
        }
    } // rectPrice

    Rectangle {
        id: rectDoc
        width:  height
        height: parent.height
        color:  "transparent"
        anchors.left: rectLogin.right

        Image {
            width:  parent.width * 0.8
            height: width
            source: "qrc:/img/docs_50.png"
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                Provider_.openDoc(i_path)
            }
        }
    } // rectTrash

} // Item
