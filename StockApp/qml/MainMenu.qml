import QtQuick 2.0
import QtQuick.Controls 2.12

Item {

    property int tileSize: height / 3

    Rectangle {
        id: menuRect
        color: Style_.colorBack
        width:  tileSize * 2
        height: parent.height

        GridView {
            id: gridWiewimgimg
            anchors.fill: parent

            clip: true
            cellWidth:  tileSize
            cellHeight: tileSize
            interactive: false

            model: menuModel
            delegate:
                MainMenuItem {
                    width:  tileSize
                    height: tileSize
                    img: image
                    nm : name

                    MouseArea {
                        anchors.fill: parent
                        onClicked:
                        {
                          changePage(index)
                        }
                    } // MouseArea
                } // MainMenuItem
        } // GridView

        ListModel {
            id: menuModel

            ListElement {
                image: "qrc:/img/icon_goods.png"
                name : "stock"
            }
            ListElement {
                image: "qrc:/img/icon_documents.png"
                name : "docs"
            }
            ListElement {
                image: "qrc:/img/icon_suppliers.png"
                name : "suppliers"
            }
            ListElement {
                image: "qrc:/img/icon_supplies.png"
                name : "supplies"
            }
            ListElement {
                image: "qrc:/img/icon_employee.png"
                name : "employee"
            }
            ListElement {
                image: "qrc:/img/icon_statistics.png"
                name : "charts"
            }
        } // menuModel
    } // menuRect

    Rectangle {
        width:  parent.width - menuRect.width
        height: parent.height
        color:  Style_.colorMain
        anchors.left: menuRect.right

        StackView {
            id: stackArea
            anchors.fill: parent

            Component {
                id: pageDocs
                PageDocs {anchors.fill: parent}
            }

            Component {
                id: pageChart
                PageChart {anchors.fill: parent}
            }

            Component {
                id: pageStock
                PageStock {anchors.fill: parent}
            }

            Component {
                id: pageSuppliers
                PageSuppliers {anchors.fill: parent}
            }

            Component {
                id: pageSupplies
                PageSupplies {anchors.fill: parent}
            }


            Component {
                id: pageEmployee
                PageEmployee {anchors.fill: parent}
            }
        } // stackArea
    } // stackRect

    function changePage(index) {
        stackArea.pop()
        if (index === 0)
            stackArea.push(pageStock)
        else if (index === 1)
            stackArea.push(pageDocs)
        else if (index === 2)
            stackArea.push(pageSuppliers)
        else if (index === 3)
            stackArea.push(pageSupplies)
        else if (index === 4)
            stackArea.push(pageEmployee)
        else if (index === 5)
            stackArea.push(pageChart)
    }
} // Item
