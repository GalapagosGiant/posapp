import QtQuick 2.0
import QtQuick.Controls 1.4

Item {

    signal clickSignal(int id);

    property int    i_id    : -1
    property string i_name  : ""
    property string i_surname  : ""
    property string i_mail     : ""
    property string i_login    : ""
    property string i_passw    : "*****"
    property string i_position : ""

    property int    numWidth: 50
    property int    fieWidth: (width - rectId.width) / 5
    property int    sepWidth : 1

    Rectangle {
        id: rectId
        width:  numWidth
        height: parent.height
        color:  "transparent"

        Text {
            anchors.fill: parent
            text:  i_id
            color: Style_.colorText
        }
    } // rectId

    Rectangle {
        id: rectName
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectId.right

        Text {
            anchors.fill: parent
            text:  i_name
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(timerName.running)
                {
                    loaderName.visible = true
                    loaderName.item.forceActiveFocus()

                    timerName.stop()
                }
                else
                {
                    timerName.restart()
                }
            }
            Timer {
                id: timerName
                interval: 200
                onTriggered: clickSignal(i_id)
            }
        }
    } // rectName

    Rectangle {
        id: rectSurname
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectName.right

        Text {
            anchors.fill: parent
            text:  i_surname
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(timerSurname.running)
                {
                    loaderSurname.visible = true
                    loaderSurname.item.forceActiveFocus()

                    timerSurname.stop()
                }
                else
                {
                    timerSurname.restart()
                }
            }
            Timer {
                id: timerSurname
                interval: 200
                onTriggered: clickSignal(i_id)
            }
        }
    } // rectName

    Rectangle {
        id: rectLogin
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectSurname.right

        Text {
            anchors.fill: parent
            text:  i_login
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(timerLogin.running)
                {
                    loaderLogin.visible = true
                    loaderLogin.item.forceActiveFocus()

                    timerLogin.stop()
                }
                else
                {
                    timerLogin.restart()
                }
            }
            Timer {
                id: timerLogin
                interval: 200
                onTriggered: clickSignal(i_id)
            }
        }
    } // rectLogin

    Rectangle {
        id: rectPassw
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectLogin.right

        Text {
            anchors.fill: parent
            text:  i_passw
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(timerPassw.running)
                {
                    loaderPassw.visible = true
                    loaderPassw.item.forceActiveFocus()

                    timerPassw.stop()
                }
                else
                {
                    timerPassw.restart()
                }
            }
            Timer {
                id: timerPassw
                interval: 200
                onTriggered: clickSignal(i_id)
            }
        }
    } // rectPassw

    Rectangle {
        id: rectMail
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectPassw.right

        Text {
            anchors.fill: parent
            text:  i_mail
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(timerMail.running)
                {
                    loaderMail.visible = true
                    loaderMail.item.forceActiveFocus()

                    timerMail.stop()
                }
                else
                {
                    timerMail.restart()
                }
            }
            Timer {
                id: timerMail
                interval: 200
                onTriggered: clickSignal(i_id)
            }
        }
    } // rectMail

    Loader {
        id: loaderName
        height: rectName.height
        width:  rectName.width
        anchors { verticalCenter: rectName.verticalCenter; left: rectName.left}
        visible: false
        sourceComponent: visible ? inputName : undefined

        Component {
            id: inputName
            TextField {
                anchors { fill: rectName }
                text: ""
                onAccepted:{
                    // DO STUFF
                    if (text != "") {
                        if  (i_id == -1)
                            Provider_.updateNewField("employees.name", text);
                        else
                            Provider_.updateField(i_id, "employees.name", text);
                    }

                    loaderName.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        loaderName.visible = false
                        if (text != "" && i_id == -1) {
                           Provider_.updateNewField("employees.name", text);
                        }
                    }
                }
            }
        }
    } // loaderName

    Loader {
        id: loaderSurname
        anchors.fill: rectSurname
        visible: false
        sourceComponent: visible ? inputSurname: undefined

        Component {
            id: inputSurname
            TextField {
                anchors.fill: rectSurname
                text: ""
                onAccepted:{
                    // DO STUFF
                    if (text != "") {
                        if  (i_id == -1)
                            Provider_.updateNewField("employees.surname", text);
                        else
                            Provider_.updateField(i_id, "employees.surname", text);
                    }

                    loaderSurname.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        loaderSurname.visible = false
                        if (text != "" && i_id == -1) {
                           Provider_.updateNewField("employees.surname", text);
                        }
                    }
                }
            }
        }
    } // loaderSurname

    Loader {
        id: loaderLogin
        anchors.fill: rectLogin
        visible: false
        sourceComponent: visible ? inputLogin: undefined

        Component {
            id: inputLogin
            TextField {
                anchors.fill: rectLogin
                text: ""
                onAccepted:{
                    // DO STUFF
                    if (text != "") {
                        if  (i_id == -1)
                            Provider_.updateNewField("employees.login", text);
                        else
                            Provider_.updateField(i_id, "employees.login", text);
                    }

                    loaderLogin.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        loaderLogin.visible = false
                        if (text != "" && i_id == -1) {
                           Provider_.updateNewField("employees.login", text);
                        }
                    }
                }
            }
        }
    } // loaderLogin

    Loader {
        id: loaderPassw
        anchors.fill: rectPassw
        visible: false
        sourceComponent: visible ? inputPassw: undefined

        Component {
            id: inputPassw
            TextField {
                anchors.fill: rectPassw
                text: ""
                onAccepted:{
                    // DO STUFF
                    if (text != "") {
                        if  (i_id == -1) {
//                            Provider_.updateNewField("employees.mail", text);
                        }
                        else
                            Provider_.updateField(i_id, "employees.password", text);
                    }

                    loaderPassw.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        loaderPassw.visible = false
                    }
                }
            }
        }
    } // loaderPassw

    Loader {
        id: loaderMail
        anchors.fill: rectMail
        visible: false
        sourceComponent: visible ? inputMail: undefined

        Component {
            id: inputMail
            TextField {
                anchors.fill: rectMail
                text: ""
                onAccepted:{
                    // DO STUFF
                    if (text != "") {
                        if  (i_id == -1)
                            Provider_.updateNewField("employees.mail", text);
                        else
                            Provider_.updateField(i_id, "employees.mail", text);
                    }

                    loaderMail.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        if (text != "" && i_id == -1) {
                           Provider_.updateNewField("employees.mail", text);
                        }

                        loaderMail.visible = false
                    }
                }
            }
        }
    } // loaderMail

} // Item
