import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    property int    i_id   : -1
    property string i_tel  : ""
    property string i_name : ""
    property string i_mail : ""

    property int    numWidth: 50
    property int    fieWidth: 150
    property int    sepWidth : 1

    Rectangle {
        id: rectId
        width:  numWidth
        height: parent.height
        color:  "transparent"

        Text {
            anchors.fill: parent
            text:  i_id
            color: Style_.colorText
        }
    } // rectId

    Rectangle {
        id: rectName
        width:  parent.width - rectId.width - rectMail.width - rectTel.width
        height: parent.height
        color:  "transparent"
        anchors.left: rectId.right

        Text {
            id: textName
            anchors.fill: parent
            text:  i_name
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                loaderName.visible = true
                loaderName.item.forceActiveFocus()
            }
        }
    } // rectName

    Rectangle {
        id: rectMail
        width:  fieWidth * 2
        height: parent.height
        color:  "transparent"
        anchors.left: rectName.right

        Text {
            id: textMail
            anchors.fill: parent
            text:  i_mail
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                loaderMail.visible = true
                loaderMail.item.forceActiveFocus()
            }
        }
    } // rectMail

    Rectangle {
        id: rectTel
        width:  fieWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectMail.right

        Text {
            id: textTel
            anchors.fill: parent
            text:  i_tel
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                loaderTel.visible = true
                loaderTel.item.forceActiveFocus()
            }
        }
    } // rectTel

    Loader {
        id: loaderName
        height: rectName.height
        width:  rectName.width
        anchors { verticalCenter: rectName.verticalCenter; left: rectName.left}
        visible: false
        sourceComponent: visible ? inputName : undefined

        Component {
            id: inputName
            TextField {
                anchors { fill: rectName }
                text: ""
                onAccepted:{
                    // DO STUFF
                    Provider_.updateField(i_id, "suppliers.name", text);

                    loaderName.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        loaderName.visible = false
                        if (text != "" && i_id == -1) {
                           Provider_.updateNewField("suppliers.name", text);
                        }
                    }
                }
            }
        }
    } // loaderName

    Loader {
        id: loaderMail
        height: rectMail.height
        width:  rectMail.width
        anchors { verticalCenter: rectMail.verticalCenter; left: rectMail.left}
        visible: false
        sourceComponent: visible ? inputMail: undefined

        Component {
            id: inputMail
            TextField {
                anchors { fill: rectMail }
                text: ""
                onAccepted:{
                    // DO STUFF
                    Provider_.updateField(i_id, "suppliers.mail", text);

                    loaderMail.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        loaderMail.visible = false
                        if (text != "" && i_id == -1) {
                           Provider_.updateNewField("suppliers.mail", text);
                        }
                    }
                }
            }
        }
    } // loaderMail

    Loader {
        id: loaderTel
        height: rectTel.height
        width:  rectTel.width
        anchors { verticalCenter: rectTel.verticalCenter; left: rectTel.left}
        visible: false
        sourceComponent: visible ? inputTel : undefined

        Component {
            id: inputTel
            TextField {
                anchors { fill: rectTel }
                text: ""
                onAccepted:{
                    // DO STUFF

                    Provider_.updateField(i_id, "suppliers.tel", text);

                    loaderTel.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        loaderTel.visible = false
                        if (text != "" && i_id == -1) {
                           Provider_.updateNewField("suppliers.tel", text);
                        }
                    }
                }
            }
        }
    } // loaderTel

} // Item
