import QtQuick 2.0

Item {

    property string defColor: "#3a3f44"
    property string textColor: "#fff"

    property int borderWidth;
    property string pasiveColor;

    property string defaultText;

    property string text: hostInput.text;

    Rectangle {
        id: mainRect
        anchors.fill: parent
        color: defColor

        border.width: Style_.borderWidth
        border.color: Style_.borderColor
        radius: Style_.radius

        Text {
            id: hostTextHint

            text: defaultText
            color: textColor
            font.family: "Open Sans"
            font.pointSize: parent.height/4

            anchors.left: parent.left
            anchors.leftMargin: parent.height / 2
            anchors.verticalCenter: parent.verticalCenter
        } // hostTextHint

        TextInput {
            id: hostInput
            inputMethodHints: Qt.ImhPreferNumbers

            color: textColor
            font.family: "Open Sans"
            font.pointSize: parent.height/4
            maximumLength: 34

            anchors.left: parent.left
            anchors.leftMargin: parent.height / 2
            anchors.verticalCenter: parent.verticalCenter

            onTextChanged: {
                if (text.length > 3) {
                    hintHost()
                }
                else {
                    hostTextHint.text = ""
                    hintsEnabled = 0
                }
            }
        } // hostInput

//        Rectangle {
//            id: hostLable
//            height: host.height/2.5
//            width:  height * 3
//            visible: false

//            border.width: borderWidth
//            border.color: borderActiveColor
//            radius: borderWidth * 2
//            color: "#36b7eb"

//            anchors.top:  host.top
//            anchors.left: host.left
//            anchors.topMargin:  0 - height/2
//            anchors.leftMargin: height

//            Text {
//                text: "Host"
//                color: "white"
//                font.family: "Open Sans"
//                font.pointSize: parent.height/2
//                font.bold: true
//                anchors.centerIn: parent
//            }
//        } // hostLable

        MouseArea {
            anchors.fill: parent
            z: 5
            onClicked: {
//                deactivatePort()
//                activateHost();
                activate();
            }
            onDoubleClicked: {
                hostInput.selectAll()
            }
        }

        Keys.onPressed: {
            if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter || event.key == Qt.Key_Tab) {
                if (hintsEnabled === 0) {
                    deactivateHost()
                    activatePort()
                }
                else if (hintsEnabled === 1) {
                    hostInput.text = hostTextHint.text
                    hostTextHint.text = ""
                    hintsEnabled = 0
                }

                console.log("host Tab")
            }
        }
    } // MainRect

    function activate() {
        console.log("TextInputRect: Activate.")

        mainRect.border.color = borderActiveColor
//        hostLable.visible = true

        hostTextHint.text = ""

        hostInput.color = "white"
        hostInput.forceActiveFocus()

        if (hostInput.text == hostDefaultValue)
            hostInput.text = ""
    }
}
