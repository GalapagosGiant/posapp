import QtQuick 2.0

Item {

    property bool flag: false

    Rectangle {
        id: mainRect
        anchors.fill: parent
        color: "transparent"
        radius: Style_.radius

        Rectangle {
            width: button.width + 10
            height: width
            radius: width / 2
            color:  Style_.colorBack
            anchors.verticalCenter:   button.verticalCenter
            anchors.horizontalCenter: button.horizontalCenter
        } // button border

        Rectangle {
            id: button
            width:  30
            height: width
            radius: width / 2
            color:  Style_.colorBack

            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin:   width / 2
            anchors.rightMargin: width / 2

            Image {
                anchors.fill: parent
                source: "qrc:/img/gear30x30.png"
            }

            MouseArea {
                anchors.fill: parent
                onClicked:    buttonClicked()
            }

        } // button

        Rectangle {
            width: status.width + 10
            height: width
            radius: width / 2
            color:  Style_.colorBack
            anchors.verticalCenter:   status.verticalCenter
            anchors.horizontalCenter: status.horizontalCenter
        } // status border

        Rectangle {
            id: status
            width:  10
            height: width
            radius: width / 2
            color: "red"

            anchors.left:   button.left
            anchors.bottom: button.bottom
//            anchors.leftMargin:   -width
//            anchors.bottomMargin: -width
        } // status



    }

    function setStatus(val) {
        status.color = val ? "green" : "red";
    }

    function buttonClicked() {
        console.log("ServerInfo: Button clicked")

        flag = !flag

        showInfo(flag)
    }

    function showInfo() {
        mainRect.color = flag ? "#3a3f44" : "transparent";
    }

    function hideInfo() {

    }
}
