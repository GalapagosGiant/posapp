import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.2

Item {
    property int spacing : 15
    property int fontSize: 15

    Rectangle {
        width:  parent.width  - 2 * spacing
        height: parent.height - 2 * spacing
        color: "transparent"

        anchors.verticalCenter:   parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        PageHeader {
            id:     header
            width:  parent.width
            height: parent.height * 2/15
            headerText: "Documents"

            onClickSignal: onBackButtonClicked();
        } // header

        Rectangle {
            id:     docsRect
            width:  parent.width
            height: parent.height - header.height - footer.height
            color:  "transparent"

            anchors.top: header.bottom

            ListView {
                id: docsView
                width:  parent.width * 0.95
                height: parent.height

                anchors.horizontalCenter: parent.horizontalCenter

                clip: true
                currentIndex: 0
                model: modelDocs_

                delegate: DelegateDocs {
                    height: fontSize * 2
                    width:  docsView.width
                    i_id:      model.id
                    i_name:    model.name
                    i_date:    model.date
                    i_path:    model.document
                    i_login:   model.login
                    i_price:   model.price
                }
            } // docsView
        } // docsRect

        PageFooter {
            id:     footer
            width:  parent.width
            height: parent.height * 1/15
            type:   "documents"

//            onSearchChanged: Provider_.filterChanged(text)

            anchors.bottom: parent.bottom
        } // footer

    } // mainRect

} // Item
