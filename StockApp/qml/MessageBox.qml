import QtQuick 2.0
import QtGraphicalEffects 1.12

Item {

    signal clsMsgSignal();

    property string login: "admin"

    Rectangle {
        id: mainRect
        anchors.fill: parent
        color: Style_.colorMain

        Rectangle {
            width:  parent.width  * 0.8
            height: parent.height * 0.8
            color:  "transparent"

            anchors.centerIn: parent

            Rectangle {
                id: header
                width: parent.width
                height: parent.height * 0.3
                color:  "transparent"

                Text {
                    id: label
                    text:  "Message to '" + login + "'"
                    color: Style_.colorText
                    font.pointSize: parent.height / 3
                }

                Rectangle {
                    width:  parent.width
                    height: 1
                    color:  Style_.colorBack

                    anchors.top: label.bottom
                    anchors.topMargin: label.height
                } // Separator

            } // header

            Rectangle {
                width:  parent.width
                height: parent.height - header.height - footer.height
                color:  "#3a3f44"

                anchors.top: header.bottom

                border.width: Style_.borderWidth
                border.color: Style_.borderColorActive
                radius: Style_.radius

                TextInput {
                    id: input
                    width:  parent.width
                    height: parent.height / 2

                    anchors.left: parent.left
                    anchors.leftMargin: height / 2
                    anchors.verticalCenter: parent.verticalCenter

                    color: "white"
                    font.family: "Open Sans"
                    font.pointSize: parent.height/4
                    maximumLength: 30
                } // input
            }

            Rectangle {
                id: footer
                width: parent.width
                height: parent.height * 0.3
                color: "transparent"

                anchors.bottom: parent.bottom

                Rectangle {
                    id: buttonYes
                    width:  parent.height * 0.8;
                    height: width
                    radius: height / 2
                    color:  Style_.buttonColor

                    border.width: Style_.borderWidth
                    border.color: Style_.buttonBorderColor

                    anchors.right:  parent.right
                    anchors.bottom: parent.bottom

                    Text {
                        text: qsTr("✓")
                        color: "white"
                        font.pointSize:   parent.height / 2
                        anchors.centerIn: parent
                    }

                    MouseArea {
                        anchors.fill: parent
                        onPressed: parent.opacity = 0.8
                        onReleased: {
                            parent.opacity = 1;
                            onButtonClicked(1);
                        }
                    }
                } // Button (yes)

                Rectangle {
                    width:  buttonYes.width;
                    height: buttonYes.height
                    radius: buttonYes.radius
                    color:  buttonYes.color

                    border.width: buttonYes.border.width
                    border.color: buttonYes.border.color

                    anchors.verticalCenter: buttonYes.verticalCenter
                    anchors.right: buttonYes.left
                    anchors.rightMargin: width / 2

                    Text {
                        text:  qsTr("x")
                        color: "white"
                        font.pointSize:   parent.height / 2
                        anchors.centerIn: parent
                    }

                    MouseArea {
                        anchors.fill: parent
                        onPressed: parent.opacity = 0.8
                        onReleased: {
                            parent.opacity = 1;
                            onButtonClicked(0);
                        }
                    }
                } // Button (no)
            } // footer
        }

    } // mainRect

    DropShadow {
        anchors.fill: mainRect
//        verticalOffset: 3
        radius: 15
        samples: 17
        color: "#80000000"
        source: mainRect
    }

    function clear() {

    }

    function onButtonClicked(value) {
        {console.log("onButtonClicked")

        if (value === 1 && input.text !== "" && login !== "")
            Provider_.sendMessage(input.text, login);
        }

        login  = ""
        input.text = ""

        clsMsgSignal();
    }
}
