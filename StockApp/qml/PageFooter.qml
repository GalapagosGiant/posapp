import QtQuick 2.0

Item {
    signal searchChanged(string text);

    property int parentId: -1
    property string type: ""

    property int addConfirm: 0

    Rectangle {
        anchors.fill: parent
        color: "transparent"

        Rectangle {
            id: searchRect
            width:  parent.width / 2
            height: parent.height * 0.8
            opacity: 0.3
            radius: Style_.radius

            border.width: Style_.borderWidth
            border.color: Style_.borderColor

            anchors.verticalCenter: parent.verticalCenter

            TextInput {
                id: input
                color: Style_.colorText
                font.pointSize: parent.height * 0.4

                anchors.fill: parent
//                verticalAlignment: parent.verticalCenter
                onActiveFocusChanged: {
                    if (focus === true)
                    {
                        searchRect.border.color = Style_.borderColorActive
                    }
                }

                onTextChanged: {
                    searchChanged(text)
                }
            }

        } // searchRect

        Rectangle {
            id: button
            width:  parent.height;
            height: width
            radius: height / 2
            color:  Style_.buttonColor

            border.width: Style_.borderWidth
            border.color: Style_.buttonBorderColor

            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter

            Text {
                id:   btnIcon
                text: type === "supplies" ? qsTr("+") : qsTr("✓")
                color: "white"
                font.pointSize:   parent.height / 2
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onPressed: parent.opacity = 0.8
                onReleased: {
                    parent.opacity = 1;
                    onButtonPressed();
                }
            }
        } // Button (add)
    } // rectMain

    function clearSearch() {
        input.text = "";
    }

    function clearNew() {
        addConfirm = 0;
        btnIcon.text = "+";
    }

    function onButtonPressed() {
        if (type === "supplies")
        {
            Provider_.sendSupply();
            return;
        }

        if (addConfirm == 0)
        {
            addConfirm = 1;
            btnIcon.text = "✓";
            Provider_.addNew(type, parentId);
        }
        else
        {
            clearNew();
            Provider_.confirmNew(type);
        }
    }
} // Item
