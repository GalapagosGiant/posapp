import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.2


Item {
    property int spacing : 15
    property int fontSize: 15

    Rectangle {
        width:  parent.width  - 2 * spacing
        height: parent.height - 2 * spacing
        color: "transparent"

        anchors.verticalCenter:   parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        PageHeader {
            id:     header
            width:  parent.width
            height: parent.height * 2/15
            headerText: "Suppliers"

            onClickSignal: {
                console.log("TEEEST");
            }
        } // header

        ListView {
            id: listView
            width:  parent.width * 0.95
            height: parent.height - header.height - rectFooter.height

            anchors.top: header.bottom
            anchors.horizontalCenter: parent.horizontalCenter

            clip: true
            model: modelSuppliers_

            delegate: DelegateSuppliers {
                height: fontSize * 2
                width:  parent.width
                i_id:     model.id
                i_name:   model.name
                i_mail:   model.mail
                i_tel:    model.tel
            }
        } // listView

        PageFooter {
            id:     rectFooter
            width:  parent.width
            height: parent.height * 1/15
            type:   "suppliers"

            anchors.top: listView.bottom
        } // rectFooter

    } // mainRect
} // Item
