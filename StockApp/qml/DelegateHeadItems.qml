import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    property string i_name  : "NAME"
    property string i_id    : "ID"
    property string i_price : "PRICE"
    property string i_amount: "AMOUNT"

    property int    numWidth: 50
    property int    sepWidth : 1

    Rectangle {
        id: rectId
        width:  numWidth
        height: parent.height
        color:  "transparent"

        Text {
            anchors.fill: parent
            text:  i_id
            color: Style_.colorText
        }
    } // rectId

    Rectangle {
        id: rectName
        width:  parent.width - rectId.width - rectAmount.width - rectPrice.width - rectTrash.width
        height: parent.height
        color:  "transparent"
        anchors.left: rectId.right

        Text {
            anchors.fill: parent
            text:  i_name
            color: Style_.colorText
        }
    } // rectName

    Rectangle {
        id: rectAmount
        width:  numWidth * 2
        height: parent.height
        color:  "transparent"
        anchors.left: rectName.right

        Text {
            anchors.fill: parent
            text:  i_amount
            color: Style_.colorText
        }
    } // rectAmount

    Rectangle {
        id: rectPrice
        width:  numWidth * 2
        height: parent.height
        color:  "transparent"
        anchors.left: rectAmount.right

        Text {
            anchors.fill: parent
            text:  i_price
            color: Style_.colorText
        }
    } // rectPrice

    Rectangle {
        id: rectTrash
        width:  height
        height: parent.height
        color:  "transparent"
        anchors.left: rectPrice.right
    } // rectTrash

} // Item
