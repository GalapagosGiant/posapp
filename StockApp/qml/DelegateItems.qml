import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    property string i_name: ""
    property int    i_id    : -1
    property int    i_price : -1
    property int    i_amount: -1

    property int    numWidth: 50
    property int    sepWidth : 1

    Rectangle {
        id: rectId
        width:  numWidth
        height: parent.height
        color:  "transparent"

        Text {
            anchors.fill: parent
            text:  i_id
            color: Style_.colorText
        }
    } // rectId

    Rectangle {
        id: rectName
        width:  parent.width - rectId.width - rectAmount.width - rectPrice.width - rectTrash.width
        height: parent.height
        color:  "transparent"
        anchors.left: rectId.right

        Text {
            anchors.fill: parent
            text:  i_name
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                loaderName.visible = true
                loaderName.item.forceActiveFocus()
            }
        }
    } // rectName

    Rectangle {
        id: rectAmount
        width:  numWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectName.right

        Text {
            anchors.fill: parent
            text:  i_amount
            color: Style_.colorText
        }
    } // rectAmount

    Rectangle {
        id: rectPrice
        width:  numWidth
        height: parent.height
        color:  "transparent"
        anchors.left: rectAmount.right

        Text {
            anchors.fill: parent
            text:  i_price
            color: Style_.colorText
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                loaderPrice.visible = true
                loaderPrice.item.forceActiveFocus()
            }
        }
    } // rectPrice

    Rectangle {
        id: rectTrash
        width:  height
        height: parent.height
        color:  "transparent"
        anchors.left: rectPrice.right

        Image {
            width:  parent.width * 0.8
            height: width
            source: "qrc:/img/trash-50.png"
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                Provider_.removeItem("products", i_id)
            }
        }
    } // rectTrash

    Loader {
        id: loaderName
        height: rectName.height
        width:  rectName.width
        anchors { verticalCenter: rectName.verticalCenter; left: rectName.left}
        visible: false
        sourceComponent: visible ? inputName : undefined

        Component {
            id: inputName
            TextField {
                anchors { fill: rectName }
                text: ""
                onAccepted:{
                    if (text != "")
                        if  (i_id == -1)
                            Provider_.updateNewField("products.name", text);
                        else
                            Provider_.updateField(i_id, "products.name", text);

                    loaderName.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        if (text != "" && i_id == -1)
                           Provider_.updateNewField("products.name", text);

                        loaderName.visible = false
                    }
                }
            }
        }
    } // loaderName

    Loader {
        id: loaderPrice
        height: rectPrice.height
        width:  rectPrice.width
        anchors { verticalCenter: rectPrice.verticalCenter; left: rectPrice.left}
        visible: false
        sourceComponent: visible ? inputPrice : undefined

        Component {
            id: inputPrice
            TextField {
                anchors { fill: rectPrice }
                text: ""
                onAccepted:{
                    if (text != "")
                        if  (i_id == -1)
                            Provider_.updateNewField("products.price", text);
                        else
                            Provider_.updateField(i_id, "products.price", text);

                    loaderPrice.visible = false
                }

                onActiveFocusChanged: {
                    if (!activeFocus) {
                        if (text != "" && i_id == -1)
                           Provider_.updateNewField("products.price", text);

                        loaderPrice.visible = false
                    }
                }
            }
        }
    } // loaderPrice

} // Item
