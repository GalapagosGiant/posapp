import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.2

Item {
    property int spacing : 15
    property int fontSize: 15

    property string textCategories: "Categories"
    property string textProducts:   "Products"

    Rectangle {
        width:  parent.width  - 2 * spacing
        height: parent.height - 2 * spacing
        color: "transparent"

        anchors.verticalCenter:   parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        PageHeader {
            id:     header
            width:  parent.width
            height: parent.height * 2/15
            headerText: textCategories

            onClickSignal: onBackButtonClicked();
        } // header

        Rectangle {
            id:     stackRect
            width:  parent.width
            height: parent.height - header.height - footer.height
            color:  "transparent"

            anchors.top: header.bottom

            SwipeView {
                id: swipeView
                anchors.fill: parent
                interactive: false
                currentIndex: 0
                clip: true

                Page {
                    background: {
                        color: "transparent"
                    }

                    ListView {
                        id: categoriesView
                        width:  parent.width * 0.95
                        height: parent.height
                        currentIndex: 0

                        anchors.horizontalCenter: parent.horizontalCenter

                        clip: true
                        model: modelCategories_

                        delegate: DelegateCategories {
                            id: categoriesDelegate
                            height: fontSize * 2
                            width:  parent.width
                            i_id:    model.id
                            i_name:  model.name
                            i_color: model.color

                            onClickSignal: {
                                categorySelected(id)
                            }
                        }
                    } // categoriesView
                }

                Page {
                    background: {
                        color: "transparent"
                    }

                    ListView {
                        id: productsView
                        width:  parent.width * 0.95
                        height: parent.height

                        anchors.horizontalCenter: parent.horizontalCenter

                        header: DelegateItems {
                            height:  fontSize * 2
                            width:   productsView.width
                            i_id:     0
                            i_name:   "name"
                            i_amount: 3
                            i_price:  3
                        }

                        clip: true
                        model: modelProducts_

                        delegate: DelegateItems {
                            id: productsDelegate
                            height:  fontSize * 2
                            width:   productsView.width
                            i_id:     model.id
                            i_name:   model.name
                            i_amount: model.amount
                            i_price:  model.price
                        }
                    } // productsView
                }

            } // swipeView
        } // stackRect

        PageFooter {
            id:     footer
            width:  parent.width
            height: parent.height * 1/15
            type:   "categories"

            anchors.bottom: parent.bottom

            onSearchChanged: Provider_.filterChanged(text)
        } // rectFooter

    } // mainRect

    function categorySelected(id)
    {
        console.log("Cat selected " + id)
        header.headerText = textProducts;
        footer.parentId = id;
        footer.type = textProducts.toLowerCase()

        swipeView.setCurrentIndex(1);

        Provider_.setCategory(id);
    }

    function onBackButtonClicked()
    {
        header.headerText = textCategories

        swipeView.setCurrentIndex(0)

        footer.clearSearch();
    }

} // Item
