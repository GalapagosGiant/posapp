import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.2


Item {
    property int spacing : 15
    property int fontSize: 15

    Rectangle {
        width:  parent.width  - 2 * spacing
        height: parent.height - 2 * spacing
        color: "transparent"

        anchors.verticalCenter:   parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        TableView {
            id: tableView
            //                id: listView
            width:  parent.width
            height: parent.height * 14/15
            objectName: "tableView"
            horizontalScrollBarPolicy: -1
            selectionMode: SelectionMode.SingleSelection

            TableViewColumn {
                id: nameColumn
                title: "Name"
                role: "name"
                movable: false
                resizable: false
                width: 300 /*tableView.viewport.width - amountColumn.width*/
            }

            TableViewColumn {
                id: amountColumn
                title: "Amount"
                role: "amount"
                movable: false
                resizable: false
                width: 100 //tableView.viewport.width / 3
            }

            TableViewColumn {
                id: priceColumn
                title: "Price"
                role: "price"
                movable: false
                resizable: false
                width: 100
            }

            /*style:*/ TableViewStyle {
                headerDelegate: Rectangle {
                    height: fontSize * 2
                    width:  textItem.implicitWidth
                    color:  Style_.colorBack
                    Text {
                        id: textItem
                        text:  styleData.value
                        color: "white"
                        font.pixelSize: fontSize

                        anchors.fill: parent
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: styleData.textAlignment
                        anchors.leftMargin: fontSize
                    }
                    Rectangle {
                        anchors.right: parent.right
                        anchors.top:   parent.top
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 1
                        anchors.topMargin: 1
                        width: 1
                        color: Style_.colorMain
                    }
                } // headerDelegate
                rowDelegate: Rectangle {
                    height: fontSize * 2
                    width:  textItem.implicitWidth
                    color:  Style_.colorBack
                    Text {
//                        id: textItem
                        text:  styleData.value
                        color: "white"
                        font.pixelSize: fontSize

                        anchors.fill: parent
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: styleData.textAlignment
                        anchors.leftMargin: fontSize
                    }
                    Rectangle {
                        anchors.right: parent.right
                        anchors.top:   parent.top
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 1
                        anchors.topMargin: 1
                        width: 1
                        color: Style_.colorMain
                    }
                } // itemDelegate
            } // TableViewStyle

            model: ListModel {
                id: libraryModel
                ListElement {
                    title: "A Masterpiece"
                    author: "Gabriel"
                }
                ListElement {
                    title: "Brilliance"
                    author: "Jens"
                }
                ListElement {
                    title: "Outstanding"
                    author: "Frederik"
                }
            }

            itemDelegate: Rectangle {
                Text {
                    anchors { verticalCenter: parent.verticalCenter; left: parent.left }
                    color: "black"
                    text: styleData.value + "(value)"
                }

                MouseArea {
                    id: cellMouseArea
                    anchors.fill: parent
                    onClicked: {
                        // Column index are zero based
                        if(styleData.column === 1){
                            loader.visible = true
                            loader.item.forceActiveFocus()
                        }
                    }
                }

                Loader {
                    id: loader
                    height: parent.height
                    width:  parent.width
                    anchors { verticalCenter: parent.verticalCenter; left: parent.left}
                    visible: false
                    sourceComponent: visible ? input : undefined

                    Component {
                        id: input
                        TextField {
                            anchors { fill: parent }
                            text: ""
                            onAccepted:{
                                // DO STUFF
                                loader.visible = false
                            }

                            onActiveFocusChanged: {
                                if (!activeFocus) {
                                    loader.visible = false
                                }
                            }
                        }
                    }
                }
            }
        }


        //        ListView {
        //            id: listView
        //            width:  parent.width
        //            height: parent.height * 14/15
        //            clip: true

        //            model: modelCustomers_

        //            delegate: Rectangle {
        //                implicitWidth: 100
        //                implicitHeight: 50
        //                Text {
        //                    text: model.name
        //                }
        //            }
        //        } // listView

        Rectangle {
            width:  parent.width
            height: parent.height - tableView.height
            color:  "transparent"

            anchors.top: tableView.bottom

            Rectangle {
                width:  parent.width / 2
                height: parent.height * 0.8
                radius: Style_.radius

                anchors.verticalCenter: parent.verticalCenter
            } // Search

            Rectangle {
                width:  parent.height;
                height: width
                radius: height / 2
                color:  Style_.buttonColor

                border.width: Style_.borderWidth
                border.color: Style_.buttonBorderColor

                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter

                Text {
                    text: qsTr("+")
                    color: "white"
                    font.pointSize:   parent.height / 2
                    anchors.centerIn: parent
                }
            } // Button (add)
        } // footer

    } // mainRect
} // Item
