import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.2

Item {
    property int spacing : 15
    property int fontSize: 15

    property string textEmployee: "Employee"
    property string textSessions: "Sessions"

    Rectangle {
        width:  parent.width  - 2 * spacing
        height: parent.height - 2 * spacing
        color: "transparent"

        anchors.verticalCenter:   parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        PageHeader {
            id:     header
            width:  parent.width
            height: parent.height * 2/15
            headerText: textEmployee

            onClickSignal: onBackButtonClicked();
        } // header

        Rectangle {
            id:     stackRect
            width:  parent.width
            height: parent.height - header.height - footer.height
            color:  "transparent"

            anchors.top: header.bottom

            SwipeView {
                id: swipeView
                anchors.fill: parent
                interactive: false
                currentIndex: 0
                clip: true

                Page {
                    background: {
                        color: "transparent"
                    }

                    ListView {
                        id: employeeView
                        width:  parent.width * 0.95
                        height: parent.height

                        anchors.horizontalCenter: parent.horizontalCenter

                        clip: true
                        currentIndex: 0
                        model: modelEmployees_

                        delegate: DelegateEmployee {
                            id: categoriesDelegate
                            height: fontSize * 2
                            width:  employeeView.width
                            i_id:      model.id
                            i_name:    model.name
                            i_surname: model.surname
                            i_mail:    model.mail
                            i_login:   model.login
                            i_position: model.position

                            onClickSignal: {
                                employeeSelected(id)
                            }
                        }
                    } // employeeView
                }

                Page {
                    background: {
                        color: "transparent"
                    }

                    ListView {
                        id: sessionView
                        width:  parent.width * 0.95
                        height: parent.height

                        anchors.horizontalCenter: parent.horizontalCenter

                        clip: true
                        model: modelSessions_

                        delegate: DelegateSession {
                            height: fontSize * 2
                            width:  sessionView.width
                            i_id:     model.id
                            i_date:   model.date
                            i_start:  model.start
                            i_end:    model.end
                        }
                    } // sessionView
                }

            } // swipeView
        } // stackRect

        PageFooter {
            id:     footer
            width:  parent.width
            height: parent.height * 1/15
            type:   "employees"

            onSearchChanged: Provider_.filterChanged(text)

            anchors.bottom: parent.bottom
        } // footer

    } // mainRect

    function employeeSelected(id)
    {
        console.log("Cat selected " + id)
        header.headerText = textSessions;

        swipeView.setCurrentIndex(1);

        Provider_.setCategory(id);
    }

    function onBackButtonClicked()
    {
        header.headerText = textEmployee

        swipeView.setCurrentIndex(0)

        footer.clearSearch();
    }

} // Item
