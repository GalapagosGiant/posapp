import QtQuick 2.0

Item {

    property string img :  ""
    property string nm  :  ""

    Rectangle {
        width:  parent.width * 0.9
        height: width
        color:  Style_.colorMain
        radius: Style_.radius

        anchors.verticalCenter:   parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        Image {
            id: image
            width:  parent.width * 0.6
            height: width
            source: img

            anchors.verticalCenter:   parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
            text:  nm.toUpperCase()
            color: Style_.colorText

            anchors.top: image.bottom
//            anchors.topMargin: (parent.height - image.height) / 8
            anchors.horizontalCenter: parent.horizontalCenter
        }
    } // mainRect
} // Item
