import QtQuick 2.0

Item {

    signal clickSignal;

    property string headerText: ""

    Rectangle {
        anchors.fill: parent
        color:  "transparent"

        Rectangle {
            id:     button
            width:  height
            height: parent.height / 2
            radius: Style_.radius
            color:  Style_.colorText

            border.width: Style_.borderWidth
            border.color: Style_.borderColor

            Text {
                text:  "<"
                color: Style_.colorMain
                font.pointSize: parent.height / 2
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onPressed: {
                    button.opacity = 0.9
                }
                onReleased: {
                    button.opacity = 1;
                    clickSignal();
                }
            }
        } // button

        Text {
            text:  headerText
            color: Style_.colorText
            font.pointSize: parent.height / 5

            anchors.left: button.right
            anchors.leftMargin: button.width
            anchors.verticalCenter: button.verticalCenter
        }

        Rectangle {
            width:  parent.width
            height: 1
            color:  Style_.colorBack

            anchors.bottom:       parent.bottom
            anchors.bottomMargin: parent.height / 4
        } // Separator
    } // rectMain
} // Item
