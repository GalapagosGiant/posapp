import QtQuick 2.0
import QtQuick.Controls 2.12

import "qrc:/qml/scripts/functions.js" as Functions

Pane {
    anchors.fill: parent

    property int borderWidthTmp: parent.width/150
    property int blockWidth:  parent.width/12

    property string borderActiveColor:  "#2f9dca"
    property string pasiveColorTmp:  "#eaeaea"
    property string textDefaultColor:  "black"

    property string messageDefaultValue:  "Please fill out the form below to get started"

    property var hintHosts: ['127.0.0.1', '89.87.45.12']
    property var hintPorts: ['1487', '5555']
    property int hintsEnabled: 0

    Connections {
        target: Client_
        onQmlGotError: showError(error)
        onQmlSignalNewHints: setHints(hosts, ports)
    }

    Rectangle {
        anchors.fill: parent
        color: Style_.colorBack

        Rectangle {
            width:  parent.width  / 4
            height: parent.height / 4

            color: "transparent"

            anchors.verticalCenter:   parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            Rectangle {
                id: messageArea
                color: "transparent"

                width:  parent.width
                height: (parent.height - host.height - port.height) /1.5

                Text {
                    id: signInText

                    text: "Sign in"
                    color: pasiveColorTmp
                    font.family: "Open Sans"
                    font.pointSize: parent.height/3
                    font.bold: true

                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Text {
                    id: messageText

                    text: messageDefaultValue
                    color: pasiveColorTmp
                    font.family: "Open Sans"
                    font.pointSize: parent.height/7
                    font.bold: true

                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }

            TextInputRect {
                id: host
                width:  parent.width
                height: parent.height / 5

                defaultText: "Login"
                borderWidth: borderWidthTmp
                pasiveColor: pasiveColorTmp

                anchors.top:  messageArea.bottom
                anchors.topMargin: height / 2
            }

            TextInputRect {
                id: port
                width:  host.width
                height: host.height

                defaultText: "Password"
                borderWidth: host.borderWidth
                pasiveColor: host.pasiveColor

                anchors.top:  host.bottom
                anchors.topMargin: height / 2
            }

            ButtonRect {
                id: button
                width:  parent.width
                height: host.height

                anchors.top:   port.bottom
                anchors.topMargin: height / 2
            }

            MouseArea {
                anchors.fill: parent
                z: -10

                onClicked: {
                    deactivateHost();
                    deactivatePort();

                    console.log("background")
                }
            }


        }

    } // background

    function activateHost() {
        host.border.color = borderActiveColor
        hostLable.visible = true

        hostTextHint.text = ""

        hostInput.color = textDefaultColor
        hostInput.forceActiveFocus()

        if (hostInput.text == hostDefaultValue)
            hostInput.text = ""
    }

    function activatePort() {
        port.border.color = borderActiveColor
        portLable.visible = true

        portTextHint.text = ""

        portInput.color = textDefaultColor
        portInput.forceActiveFocus()

        if (portInput.text == portDefaultValue)
            portInput.text = ""
    }

    function deactivateHost() {
        host.border.color = pasiveColorTmp
        hostLable.visible = false

        hostInput.focus = false

        if (hostInput.text == "" )
            hostTextHint.text = hostDefaultValue
    }

    function deactivatePort() {
        port.border.color = pasiveColorTmp
        portLable.visible = false

        portInput.focus = false

        if (portInput.text == "" )
            portTextHint.text = portDefaultValue
    }

    function connect() {
//        if (!validatePort(portInput.text)) {
//            showError("Port is not valid")
//            portTextHint.text = portDefaultValue
//            portInput.text = ""
//            deactivatePort()

//            return
//        }

//        deactivateHost()
//        deactivatePort()

        var result = Client_.connectToHost(host.text, port.text)

        if (result === 1)
            stack.popExit()
    }

    function showError(error) {
        messageText.text = error
        messageText.color = "#FF5632"
    }

    function validatePort(port) {
        if (isNaN(port))
            return false

        return port >= 1 && port <= 65535
    }

    function hintHost() {
        var result = 1

        for (var i = 0; i < hintHosts.length; i++) {
            var res = 1;

            for (var j = 0; j < hostInput.text.length; j++) {
                if (hostInput.text.charAt(j) !== hintHosts[i].charAt(j)) {
                    res = -1
                }
            }

            if (res === 1) {
                result = i
                break
            }

            result = -1
        }

        if (result !== -1) {
            hostTextHint.text = hintHosts[result]

            hintsEnabled = 1
        } else {
            hostTextHint.text = ""
        }
    }

    function hintPort() {
        var result = 1

        for (var i = 0; i < hintPorts.length; i++) {
            var res = 1;

            for (var j = 0; j < portInput.text.length; j++) {
                if (portInput.text.charAt(j) !== hintPorts[i].charAt(j)) {
                    res = -1
                }
            }

            if (res === 1) {
                result = i
                break
            }

            result = -1
        }

        if (result !== -1) {
            portTextHint.text = hintPorts[result]

            hintsEnabled = 1
        } else {
            portTextHint.text = ""
        }
    }

    function setHints(hosts, ports) {
        hintHosts = hosts
        hintPorts = ports

        for (var i = 0; i < hintHosts.length; i++) {
            console.log("HINTS " + hintHosts[i])
        }
    }
} // Item
