#include <QQmlContext>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QStandardPaths>
#include <QDir>

#include "options.h"
#include "logger.h"
#include "style.h"
#include "clientmanager.h"

#include "cpp/stockprovider.h"

void setOptions(Options *options);

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

  Options options;

  setOptions(&options);

  Logger logger(QCoreApplication::applicationPid(), "stock");

  logger.startMessage();

  // Data provider

  StockProvider provider(&logger, &options);

  // Create client

  ClientManager client("stock", &logger, &options, &provider);

  provider.setClient(&client.client_);

  // Style

  Style style("dark");

  QApplication app(argc, argv);

  QQmlApplicationEngine engine;

  // Set context properties

  QQmlContext *context = engine.rootContext();

  context -> setContextProperty("Style_", &style);
//  context -> setContextProperty("modelCustomers_", &provider.modelCustomers_);
  context -> setContextProperty("Provider_", &provider);
  context -> setContextProperty("modelSuppliers_", &provider.modelSuppliers_);
  context -> setContextProperty("modelEmployees_", &provider.modelEmployees_);
  context -> setContextProperty("modelSessions_",  &provider.modelSessions_);
  context -> setContextProperty("modelCategories_", &provider.modelGroups_);
  context -> setContextProperty("modelProducts_", &provider.proxyProducts_);
  context -> setContextProperty("modelDocs_", &provider.modelDocs_);
  context -> setContextProperty("modelSupply_", &provider.modelSupply_);
  context -> setContextProperty("graph_", &provider.modelGraph_);

  // Load qml files

  const QUrl url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                   &app, [url](QObject *obj, const QUrl &objUrl) {
    if (!obj && url == objUrl)
      QCoreApplication::exit(-1);
  }, Qt::QueuedConnection);
  engine.load(url);

  client.client_.run();

  return app.exec();
}

void setOptions(Options *options)
{
  QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);

  options -> HomePath = path + "/PosApp";
  options -> DocsPath = options -> HomePath + "/docs";
  options -> ImgPath  = options -> HomePath + "/img";

  bool res = false;

  if (!QDir(options -> HomePath).exists())
  {
    res = QDir(path).mkdir("PosApp");

    qDebug() << "Created home directory " << options -> HomePath << res;
  }

  if (!QDir(options -> ImgPath).exists())
  {
    res = QDir(options -> HomePath).mkdir("img");

    qDebug() << "Created image directory " << options -> ImgPath << res;
  }

  if (!QDir(options -> DocsPath).exists())
  {
    res = QDir(options -> HomePath).mkdir("docs");

    qDebug() << "Created docs directory " << options -> DocsPath << res;
  }
}
