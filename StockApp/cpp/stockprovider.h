#ifndef STOCKPROVIDER_H
#define STOCKPROVIDER_H

#include <QObject>

#include "models/documentmodel.h"
#include "models/customermodel.h"
#include "models/employeemodel.h"
#include "models/sessionmodel.h"
#include "models/graphmodel.h"
#include "lists/documentlist.h"
#include "lists/customerlist.h"
#include "lists/employeelist.h"
#include "lists/sessionlist.h"
#include "provider.h"

class StockProvider : public Provider
{
  Q_OBJECT
public:
  StockProvider(Logger *logger, Options *options, QObject *parent = nullptr);

  QString getName() override
  {
    return "StockProvider";
  }

  DocumentModel modelDocs_;
  CustomerModel modelSuppliers_;
  EmployeeModel modelEmployees_;
  SessionModel  modelSessions_;
  ItemListModel modelSupply_;
  GraphModel    modelGraph_;

  void updateDocuments(QVector<DocumentItem> &vec) override;
  void updateSuppliers(QVector<CustomerItem> &vec) override;
  void updateEmployees(QVector<EmployeeItem> &vec) override;
  void updateSessions(QVector<SessionItem> &vec) override;
  void del(const QString &type, const int &id) override;

public slots:

  void addNew(QString type, int parent = -1);
  void confirmNew(QString type);
  void cancelNew(QString type);
  void updateField(int id, QString type, QString value);
  void updateNewField(QString type, QString value);
  void removeItem(QString type, int id);
  void openDoc(QString file);
  void addToSupply(int id);
  void removeFromSupply(int id);
  void sendSupply();

private:
  DocumentList listDocs_;
  CustomerList listSuppliers_;
  EmployeeList listEmployees_;
  SessionList  listSessions_;
  ItemList     listSupply_;
};

#endif // STOCKPROVIDER_H
