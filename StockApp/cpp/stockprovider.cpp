#include "stockprovider.h"
#include "client.h"
#include "options.h"

#include <QFile>
#include <QDesktopServices>

StockProvider::StockProvider(Logger *logger, Options *options, QObject *parent)
  : Provider(logger, options, parent)
{
  modelSuppliers_.setList(&listSuppliers_);
  modelEmployees_.setList(&listEmployees_);
  modelSessions_ .setList(&listSessions_);
  modelDocs_     .setList(&listDocs_);
  modelSupply_   .setList(&listSupply_);

  modelGraph_.setList(&listDocs_); // ?

  int flag = 1;

  for (int i = 0; i < 9; i++)
  {
    QDateTime date(QDate(2020, 1 + i, 1), QTime(8, 30, 0));

    modelGraph_.addData(date, 10 * i * flag);

    flag *= -1;
  }
}

void StockProvider::updateDocuments(QVector<DocumentItem> &vec)
{
  log("Update " + QString::number(vec.size()) + " documents");

  listDocs_.updateList(vec);
}

void StockProvider::updateSuppliers(QVector<CustomerItem> &vec)
{
  log("Update " + QString::number(vec.size()) + " suppliers");

  listSuppliers_.updateList(vec);
}

void StockProvider::updateEmployees(QVector<EmployeeItem> &vec)
{
  log("Update " + QString::number(vec.size()) + " employees");

  listEmployees_.updateList(vec);
}

void StockProvider::updateSessions(QVector<SessionItem> &vec)
{
  log("Update " + QString::number(vec.size()) + " sessions");

  listSessions_.updateList(vec);
}

void StockProvider::addNew(QString type, int parent)
{
  log("Add new element type '" + type + "' with parent id '" + parent + "'");

  if (QString::compare(type, "suppliers") == 0)
  {
    listSuppliers_.appendNewItem();
  }
  else if (QString::compare(type, "employees") == 0)
  {
    listEmployees_.appendNewItem();
  }
  else if (QString::compare(type, "categories") == 0)
  {
    listGroups_.appendNewItem();
  }
  else if (QString::compare(type, "products") == 0)
  {
    listProducts_.appendNewItem(parent);
  }
  else
  {
    logErr("Unknown element type '" + type + "'");
  }
}

void StockProvider::confirmNew(QString type)
{
  log("Confirm new elements type '" + type + "'");

  if (QString::compare(type, "suppliers") == 0)
  {
    client_ -> add(type, listSuppliers_.newItems());

    listSuppliers_.removeNewItem();
  }
  else if (QString::compare(type, "employees") == 0)
  {
    client_ -> add(type, listEmployees_.newItems());

    listEmployees_.removeNewItem();
  }
  else if (QString::compare(type, "categories") == 0)
  {
    client_ -> add(type, listGroups_.newItems());

    listGroups_.removeNewItem();
  }
  else if (QString::compare(type, "products") == 0)
  {
    client_ -> add(type, listProducts_.newItems());

    listProducts_.removeNewItem();
  }
  else
  {
    logErr("Unknown elements type '" + type + "'");
  }
}

void StockProvider::cancelNew(QString type)
{
  log("Cancel new item element type '" + type + "'");

  if (QString::compare(type, "suppliers") == 0)
  {
    listSuppliers_.removeNewItem();
  }
  else if (QString::compare(type, "employees") == 0)
  {
    listEmployees_.removeNewItem();
  }
  else if (QString::compare(type, "categories") == 0)
  {
    listGroups_.removeNewItem();
  }
  else if (QString::compare(type, "products") == 0)
  {
    listProducts_.removeNewItem();
  }
  else
  {
    logErr("Unknown element type '" + type + "'");
  }
}

void StockProvider::updateNewField(QString type, QString value)
{
  QStringList list = type.split(QLatin1Char('.'), Qt::SkipEmptyParts);

  if (QString::compare(list.at(0), "suppliers") == 0)
  {
    listSuppliers_.updateNewItem(list.at(1), value);
  }
  else if (QString::compare(list.at(0), "employees") == 0)
  {
    listEmployees_.updateNewItem(list.at(1), value);
  }
  else if (QString::compare(list.at(0), "categories") == 0)
  {
    listGroups_.updateNewItem(list.at(1), value);
  }
  else if (QString::compare(list.at(0), "products") == 0)
  {
    listProducts_.updateNewItem(list.at(1), value);
  }
  else
  {
    logErr("Unknown elements type '" + type + "'");
  }
}

void StockProvider::removeItem(QString type, int id)
{
  log("Remove item '" + type + "' id '" + QString::number(id));

  if (client_)
  {
    client_ -> del(QString::number(id), type);
  }
}

void StockProvider::updateField(int id, QString type, QString value)
{
  log("Update '" + type + "' id '" + QString::number(id) + "' value '" + value + "'");

  if (client_)
  {
    QString strId = QString::number(id);

    client_ -> set(strId, type, value);
  }
}

void StockProvider::del(const QString &type, const int &id)
{
  log("Delete item type '" + type + "' wid id '" + id + "'");

  if (QString::compare(type, "suppliers") == 0)
  {
    listSuppliers_.removeItem(id);
  }
  else if (QString::compare(type, "employees") == 0)
  {
    listEmployees_.removeItem(id);
  }
  else if (QString::compare(type, "sessions") == 0)
  {
    listSessions_.removeItem(id);
  }
  else
  {
    Provider::del(type, id);
  }
}

void StockProvider::openDoc(QString file)
{
  log("Open document '" + file + "'");

  QString path = options_ -> DocsPath + "/" + file;

  if(!path.isEmpty() && QFile::exists(path))
  {
    QDesktopServices::openUrl(QUrl::fromLocalFile(path));

    return;
  }

  logErr("File '" + path + "' does not exist");
}

void StockProvider::addToSupply(int id)
{
  log("Add to supply index '" + QString::number(id) + "'");

  QVector<Item> vec = listProducts_.items();

  for (int i = 0; i < vec.size(); i++)
  {
    if (vec.at(i).id() == id)
    {
      Item item = vec.at(i);

      item.setAmount(1);

      listSupply_.appendItem(item);
    }
  }
}

void StockProvider::removeFromSupply(int id)
{
  log("Remove from supply index '" + QString::number(id) + "'");

  listSupply_.removeOrReduce(id);
}

void StockProvider::sendSupply()
{
  double total = 0, tax = 0;

  if (client_)
  {
    client_ -> sendCheck("supply", listSupply_.items(), QString::number(total, 'f', 2), QString::number(tax, 'f', 2));

    listSupply_.clear();
  }
}
