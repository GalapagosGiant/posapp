/****************************************************************************
** Meta object code from reading C++ file 'client.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../CoreLib/client.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'client.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Client_t {
    QByteArrayData data[34];
    char stringdata0[378];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Client_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Client_t qt_meta_stringdata_Client = {
    {
QT_MOC_LITERAL(0, 0, 6), // "Client"
QT_MOC_LITERAL(1, 7, 22), // "qmlSignalStatusChanged"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 6), // "status"
QT_MOC_LITERAL(4, 38, 17), // "qmlSignalNewHints"
QT_MOC_LITERAL(5, 56, 16), // "QVector<QString>"
QT_MOC_LITERAL(6, 73, 5), // "hosts"
QT_MOC_LITERAL(7, 79, 5), // "ports"
QT_MOC_LITERAL(8, 85, 11), // "qmlGotError"
QT_MOC_LITERAL(9, 97, 5), // "error"
QT_MOC_LITERAL(10, 103, 13), // "statusChanged"
QT_MOC_LITERAL(11, 117, 15), // "clientConnected"
QT_MOC_LITERAL(12, 133, 12), // "clientLogged"
QT_MOC_LITERAL(13, 146, 8), // "QString*"
QT_MOC_LITERAL(14, 155, 4), // "time"
QT_MOC_LITERAL(15, 160, 5), // "login"
QT_MOC_LITERAL(16, 166, 8), // "password"
QT_MOC_LITERAL(17, 175, 15), // "connectToServer"
QT_MOC_LITERAL(18, 191, 4), // "host"
QT_MOC_LITERAL(19, 196, 4), // "port"
QT_MOC_LITERAL(20, 201, 12), // "disconnected"
QT_MOC_LITERAL(21, 214, 8), // "gotError"
QT_MOC_LITERAL(22, 223, 28), // "QAbstractSocket::SocketError"
QT_MOC_LITERAL(23, 252, 9), // "reconnect"
QT_MOC_LITERAL(24, 262, 7), // "timeout"
QT_MOC_LITERAL(25, 270, 4), // "send"
QT_MOC_LITERAL(26, 275, 7), // "message"
QT_MOC_LITERAL(27, 283, 8), // "sendPing"
QT_MOC_LITERAL(28, 292, 14), // "timeoutLoading"
QT_MOC_LITERAL(29, 307, 6), // "qmlLog"
QT_MOC_LITERAL(30, 314, 11), // "onConnected"
QT_MOC_LITERAL(31, 326, 21), // "onTextMessageReceived"
QT_MOC_LITERAL(32, 348, 23), // "onBinaryMessageReceived"
QT_MOC_LITERAL(33, 372, 5) // "array"

    },
    "Client\0qmlSignalStatusChanged\0\0status\0"
    "qmlSignalNewHints\0QVector<QString>\0"
    "hosts\0ports\0qmlGotError\0error\0"
    "statusChanged\0clientConnected\0"
    "clientLogged\0QString*\0time\0login\0"
    "password\0connectToServer\0host\0port\0"
    "disconnected\0gotError\0"
    "QAbstractSocket::SocketError\0reconnect\0"
    "timeout\0send\0message\0sendPing\0"
    "timeoutLoading\0qmlLog\0onConnected\0"
    "onTextMessageReceived\0onBinaryMessageReceived\0"
    "array"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Client[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       1,  156, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x06 /* Public */,
       4,    2,  112,    2, 0x06 /* Public */,
       8,    1,  117,    2, 0x06 /* Public */,
      10,    0,  120,    2, 0x06 /* Public */,
      11,    0,  121,    2, 0x06 /* Public */,
      12,    1,  122,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      15,    2,  125,    2, 0x0a /* Public */,
      17,    2,  130,    2, 0x0a /* Public */,
      20,    0,  135,    2, 0x0a /* Public */,
      21,    1,  136,    2, 0x0a /* Public */,
      23,    0,  139,    2, 0x0a /* Public */,
      24,    0,  140,    2, 0x0a /* Public */,
      25,    1,  141,    2, 0x0a /* Public */,
      27,    0,  144,    2, 0x0a /* Public */,
      28,    0,  145,    2, 0x0a /* Public */,
      29,    1,  146,    2, 0x0a /* Public */,
      30,    0,  149,    2, 0x08 /* Private */,
      31,    1,  150,    2, 0x08 /* Private */,
      32,    1,  153,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, 0x80000000 | 5, 0x80000000 | 5,    6,    7,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   15,   16,
    QMetaType::Void, QMetaType::QUrl, QMetaType::Int,   18,   19,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 22,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::LongLong, QMetaType::QString,   26,
    QMetaType::LongLong,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   26,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   26,
    QMetaType::Void, QMetaType::QByteArray,   33,

 // properties: name, type, flags
       3, QMetaType::QString, 0x00495103,

 // properties: notify_signal_id
       3,

       0        // eod
};

void Client::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Client *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->qmlSignalStatusChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->qmlSignalNewHints((*reinterpret_cast< QVector<QString>(*)>(_a[1])),(*reinterpret_cast< QVector<QString>(*)>(_a[2]))); break;
        case 2: _t->qmlGotError((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->statusChanged(); break;
        case 4: _t->clientConnected(); break;
        case 5: _t->clientLogged((*reinterpret_cast< QString*(*)>(_a[1]))); break;
        case 6: _t->login((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 7: _t->connectToServer((*reinterpret_cast< const QUrl(*)>(_a[1])),(*reinterpret_cast< const int(*)>(_a[2]))); break;
        case 8: _t->disconnected(); break;
        case 9: _t->gotError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 10: _t->reconnect(); break;
        case 11: _t->timeout(); break;
        case 12: { qint64 _r = _t->send((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< qint64*>(_a[0]) = std::move(_r); }  break;
        case 13: { qint64 _r = _t->sendPing();
            if (_a[0]) *reinterpret_cast< qint64*>(_a[0]) = std::move(_r); }  break;
        case 14: _t->timeoutLoading(); break;
        case 15: _t->qmlLog((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 16: _t->onConnected(); break;
        case 17: _t->onTextMessageReceived((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 18: _t->onBinaryMessageReceived((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<QString> >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketError >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Client::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::qmlSignalStatusChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Client::*)(QVector<QString> , QVector<QString> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::qmlSignalNewHints)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Client::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::qmlGotError)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Client::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::statusChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Client::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::clientConnected)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Client::*)(QString * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::clientLogged)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<Client *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->status(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<Client *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setStatus(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Client::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_Client.data,
    qt_meta_data_Client,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Client::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Client::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Client.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Object"))
        return static_cast< Object*>(this);
    return QObject::qt_metacast(_clname);
}

int Client::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Client::qmlSignalStatusChanged(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Client::qmlSignalNewHints(QVector<QString> _t1, QVector<QString> _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Client::qmlGotError(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Client::statusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void Client::clientConnected()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void Client::clientLogged(QString * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
