/****************************************************************************
** Meta object code from reading C++ file 'style.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../CoreLib/style.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'style.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Style_t {
    QByteArrayData data[20];
    char stringdata0[288];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Style_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Style_t qt_meta_stringdata_Style = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Style"
QT_MOC_LITERAL(1, 6, 16), // "colorBackChanged"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 16), // "colorMainChanged"
QT_MOC_LITERAL(4, 41, 16), // "colorTextChanged"
QT_MOC_LITERAL(5, 58, 18), // "buttonColorChanged"
QT_MOC_LITERAL(6, 77, 24), // "buttonBorderColorChanged"
QT_MOC_LITERAL(7, 102, 24), // "borderColorActiveChanged"
QT_MOC_LITERAL(8, 127, 18), // "borderColorChanged"
QT_MOC_LITERAL(9, 146, 18), // "borderWidthChanged"
QT_MOC_LITERAL(10, 165, 13), // "radiusChanged"
QT_MOC_LITERAL(11, 179, 9), // "colorBack"
QT_MOC_LITERAL(12, 189, 9), // "colorMain"
QT_MOC_LITERAL(13, 199, 9), // "colorText"
QT_MOC_LITERAL(14, 209, 11), // "buttonColor"
QT_MOC_LITERAL(15, 221, 17), // "buttonBorderColor"
QT_MOC_LITERAL(16, 239, 17), // "borderColorActive"
QT_MOC_LITERAL(17, 257, 11), // "borderColor"
QT_MOC_LITERAL(18, 269, 11), // "borderWidth"
QT_MOC_LITERAL(19, 281, 6) // "radius"

    },
    "Style\0colorBackChanged\0\0colorMainChanged\0"
    "colorTextChanged\0buttonColorChanged\0"
    "buttonBorderColorChanged\0"
    "borderColorActiveChanged\0borderColorChanged\0"
    "borderWidthChanged\0radiusChanged\0"
    "colorBack\0colorMain\0colorText\0buttonColor\0"
    "buttonBorderColor\0borderColorActive\0"
    "borderColor\0borderWidth\0radius"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Style[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       9,   68, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x06 /* Public */,
       3,    0,   60,    2, 0x06 /* Public */,
       4,    0,   61,    2, 0x06 /* Public */,
       5,    0,   62,    2, 0x06 /* Public */,
       6,    0,   63,    2, 0x06 /* Public */,
       7,    0,   64,    2, 0x06 /* Public */,
       8,    0,   65,    2, 0x06 /* Public */,
       9,    0,   66,    2, 0x06 /* Public */,
      10,    0,   67,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      11, QMetaType::QString, 0x00495103,
      12, QMetaType::QString, 0x00495103,
      13, QMetaType::QString, 0x00495103,
      14, QMetaType::QString, 0x00495103,
      15, QMetaType::QString, 0x00495103,
      16, QMetaType::QString, 0x00495103,
      17, QMetaType::QString, 0x00495103,
      18, QMetaType::Int, 0x00495103,
      19, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,

       0        // eod
};

void Style::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Style *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->colorBackChanged(); break;
        case 1: _t->colorMainChanged(); break;
        case 2: _t->colorTextChanged(); break;
        case 3: _t->buttonColorChanged(); break;
        case 4: _t->buttonBorderColorChanged(); break;
        case 5: _t->borderColorActiveChanged(); break;
        case 6: _t->borderColorChanged(); break;
        case 7: _t->borderWidthChanged(); break;
        case 8: _t->radiusChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Style::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Style::colorBackChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Style::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Style::colorMainChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Style::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Style::colorTextChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Style::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Style::buttonColorChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Style::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Style::buttonBorderColorChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Style::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Style::borderColorActiveChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (Style::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Style::borderColorChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (Style::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Style::borderWidthChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (Style::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Style::radiusChanged)) {
                *result = 8;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<Style *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->colorBack(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->colorMain(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->colorText(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->buttonColor(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->buttonBorderColor(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->borderColorActive(); break;
        case 6: *reinterpret_cast< QString*>(_v) = _t->borderColor(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->borderWidth(); break;
        case 8: *reinterpret_cast< int*>(_v) = _t->radius(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<Style *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setColorBack(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setColorMain(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setColorText(*reinterpret_cast< QString*>(_v)); break;
        case 3: _t->setButtonColor(*reinterpret_cast< QString*>(_v)); break;
        case 4: _t->setButtonBorderColor(*reinterpret_cast< QString*>(_v)); break;
        case 5: _t->setBorderColorActive(*reinterpret_cast< QString*>(_v)); break;
        case 6: _t->setBorderColor(*reinterpret_cast< QString*>(_v)); break;
        case 7: _t->setBorderWidth(*reinterpret_cast< int*>(_v)); break;
        case 8: _t->setRadius(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Style::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_Style.data,
    qt_meta_data_Style,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Style::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Style::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Style.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Style::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Style::colorBackChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Style::colorMainChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void Style::colorTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void Style::buttonColorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void Style::buttonBorderColorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void Style::borderColorActiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void Style::borderColorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void Style::borderWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void Style::radiusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
