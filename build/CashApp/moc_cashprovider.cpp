/****************************************************************************
** Meta object code from reading C++ file 'cashprovider.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../CashApp/cpp/cashprovider.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cashprovider.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CashProvider_t {
    QByteArrayData data[13];
    char stringdata0[122];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CashProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CashProvider_t qt_meta_stringdata_CashProvider = {
    {
QT_MOC_LITERAL(0, 0, 12), // "CashProvider"
QT_MOC_LITERAL(1, 13, 10), // "taxChanged"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 12), // "totalChanged"
QT_MOC_LITERAL(4, 38, 13), // "amountChanged"
QT_MOC_LITERAL(5, 52, 9), // "addToCart"
QT_MOC_LITERAL(6, 62, 2), // "id"
QT_MOC_LITERAL(7, 65, 14), // "removeFromCart"
QT_MOC_LITERAL(8, 80, 14), // "onCheckClicked"
QT_MOC_LITERAL(9, 95, 9), // "clearCart"
QT_MOC_LITERAL(10, 105, 3), // "tax"
QT_MOC_LITERAL(11, 109, 5), // "total"
QT_MOC_LITERAL(12, 115, 6) // "amount"

    },
    "CashProvider\0taxChanged\0\0totalChanged\0"
    "amountChanged\0addToCart\0id\0removeFromCart\0"
    "onCheckClicked\0clearCart\0tax\0total\0"
    "amount"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CashProvider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       3,   60, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    0,   50,    2, 0x06 /* Public */,
       4,    0,   51,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   52,    2, 0x0a /* Public */,
       7,    1,   55,    2, 0x0a /* Public */,
       8,    0,   58,    2, 0x0a /* Public */,
       9,    0,   59,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      10, QMetaType::Double, 0x00495103,
      11, QMetaType::Double, 0x00495103,
      12, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,

       0        // eod
};

void CashProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CashProvider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->taxChanged(); break;
        case 1: _t->totalChanged(); break;
        case 2: _t->amountChanged(); break;
        case 3: _t->addToCart((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->removeFromCart((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->onCheckClicked(); break;
        case 6: _t->clearCart(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (CashProvider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CashProvider::taxChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (CashProvider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CashProvider::totalChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (CashProvider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CashProvider::amountChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<CashProvider *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< double*>(_v) = _t->tax(); break;
        case 1: *reinterpret_cast< double*>(_v) = _t->total(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->amount(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<CashProvider *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setTax(*reinterpret_cast< double*>(_v)); break;
        case 1: _t->setTotal(*reinterpret_cast< double*>(_v)); break;
        case 2: _t->setAmount(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject CashProvider::staticMetaObject = { {
    QMetaObject::SuperData::link<Provider::staticMetaObject>(),
    qt_meta_stringdata_CashProvider.data,
    qt_meta_data_CashProvider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CashProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CashProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CashProvider.stringdata0))
        return static_cast<void*>(this);
    return Provider::qt_metacast(_clname);
}

int CashProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Provider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void CashProvider::taxChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void CashProvider::totalChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void CashProvider::amountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
